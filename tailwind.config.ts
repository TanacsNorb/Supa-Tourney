import type { Config } from "tailwindcss";
import colors from "tailwindcss/colors";
import kobalteTCSS from "@kobalte/tailwindcss";

const config: Config = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    colors: {
      primary: colors.red,
      secondary: "#f6f5f4",
      accent: colors.cyan,
      // The lighter yellows
      warning: colors.yellow,
      // The darker yellows
      error: colors.yellow,
      colortext: "#f6f5f4",
      inversetext: "#111",
      black: colors.black,
      background: "#160630",
    },
    extend: {
      keyframes: {
        toastenter: {
          "0%": { opacity: "0.6", transform: "translateX(100%)" },
          "100%": { opacity: "1", transform: "translateX(0)" },
        },
        toastexit: {
          "100%": { opacity: "0", transform: "scale(0.75)" },
        },
        modaloverlayenter: {
          "0%": { "background-color": "rgb(0 0 0 / 0.0)", opacity: "0.6" },
          "100%": { "background-color": "rgb(0 0 0 / 0.25)", opacity: "1" },
        },
        modaloverlayexit: {
          "100%": { opacity: "0" },
        },
        confirmenter: {
          "0%": {
            "background-color": "rgb(0 0 0 / 0.0)",
            opacity: "0.6",
            transform: "rotateX(10deg)",
          },
          "100%": {
            "background-color": "rgb(0 0 0 / 0.05)",
            opacity: "1",
            transform: "rotateX(0)",
          },
        },
        collapsibleOpen: {
          "0%": {
            height: "0",
          },
          "100%": {
            height: "var(--kb-collapsible-content-height)",
          },
        },
        collapsibleClose: {
          "0%": {
            height: "var(--kb-collapsible-content-height)",
          },
          "100%": {
            height: "0",
          },
        },
        breath: {
          "50%": {
            transform: "scale(1.15)"
          },
          "100%": {
            transform: "scale(1)"
          }
        }
      },
      animation: {
        toastenter: "toastenter 200ms ease-out",
        toastexit: "toastexit 200ms ease-in",
        modaloverlayenter: "modaloverlayenter 350ms ease-out forwards",
        modaloverlayexit: "modaloverlayexit 350ms ease-in",
        confirmenter: "confirmenter 350ms ease-out forwards",
        collapsibleOpen: "collapsibleOpen 250ms ease",
        collapsibleClose: "collapsibleClose 250ms ease",
        popoverenter: "modaloverlayenter 150ms ease-out forwards",
        popoverexit: "modaloverlayexit 150ms ease-in",
        breath: "breath 1150ms linear infinite"
      },
      backgroundColor: {
        custom: "var(--custom-bg)",
      },
      textColor: {
        custom: "var(--custom-text)",
      },
      borderColor: {
        custom: "var(--custom-border)",
      },
      outlineColor: {
        custom: "var(--custom-outline)",
      },
      screens: {
        fine: {raw: "(pointer:fine)"}
      }
    },
  },
  plugins: [kobalteTCSS],
  future: {
    hoverOnlyWhenSupported: true,
  },
};

export default config;
