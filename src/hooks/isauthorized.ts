import { createSignal } from "solid-js";
import PocketBase from "pocketbase";

const pocketbase = new PocketBase(import.meta.env.VITE_PB_URL);
function determineIsAuthorized() {
  return (
    pocketbase.authStore.isValid &&
    Boolean(pocketbase.authStore.token) &&
    pocketbase.authStore.model?.collectionName === "users"
  );
}
const [localIsAuthorized, setIsAuthorized] = createSignal(
  determineIsAuthorized(),
);
export const isAuthorized = () => localIsAuthorized();

export default async function refreshIsAuthorized() {
  if (determineIsAuthorized()) setIsAuthorized(true);
  else setIsAuthorized(false);
}

