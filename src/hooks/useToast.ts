import { useContext } from "solid-js";
import { ToasterContext } from "../app";

export default function useToast(){
  return useContext(ToasterContext);
}
