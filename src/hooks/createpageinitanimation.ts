import { BeforeLeaveEventArgs, useBeforeLeave } from "@solidjs/router";
import anime from "animejs";
import { onMount } from "solid-js";

export default function createPageInitAnimation(ref: HTMLElement) {
  onMount(() => {
    const mediaQuery = window.matchMedia("(prefers-reduced-motion: reduce)");
    // Check if the media query matches or is not available.
    if (!mediaQuery || mediaQuery.matches) return;

    anime({
      targets: ref,
      scale: [0.75, 1],
      opacity: [0.5, 1],
      easing: "easeInCubic",
      duration: 650,
    });

  })

  useBeforeLeave((e: BeforeLeaveEventArgs) => {
    e.preventDefault();

    const mediaQuery = window.matchMedia("(prefers-reduced-motion: reduce)");
    // Check if the media query matches or is not available.
    if (!mediaQuery || mediaQuery.matches) {
      e.retry(true);
      return;
    }

    anime({
      targets: ref,
      scale: [1, 0.85],
      translateX: [0, "-25%"],
      opacity: [1, 0],
      duration: 500,
      easing: "easeInCubic",
    });

    setTimeout(() => {
      e.retry(true);
    }, 525);
  });
}
