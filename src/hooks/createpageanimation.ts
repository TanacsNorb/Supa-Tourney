import { onMount } from "solid-js";
import anime from "animejs";
import { useBeforeLeave, BeforeLeaveEventArgs } from "@solidjs/router";

export default function createPageAnimation(ref: HTMLElement) {
  onMount(() => {
    const mediaQuery = window.matchMedia("(prefers-reduced-motion: reduce)");
    // Check if the media query matches or is not available.
    if (!mediaQuery || mediaQuery.matches) return;

    anime({
      targets: ref,
      scale: [0.85, 1],
      translateX: ["25%", 0],
      opacity: [0.8, 1],
      easing: "easeOutCubic",
      duration: 500,
    });
  });

  useBeforeLeave((e: BeforeLeaveEventArgs) => {
    e.preventDefault();

    const mediaQuery = window.matchMedia("(prefers-reduced-motion: reduce)");
    // Check if the media query matches or is not available.
    if (!mediaQuery || mediaQuery.matches) {
      e.retry(true);
      return;
    }

    anime({
      targets: ref,
      scale: [1, 0.85],
      translateX: [0, "-25%"],
      opacity: [1, 0],
      duration: 500,
      easing: "easeInCubic",
    });

    setTimeout(() => {
      e.retry(true);
    }, 525);
  });
}
