import { createSignal } from "solid-js";

const PHONESIZE = 640;
const SUB1K = 1000;
const [isPhone, setIsPhone] = createSignal(window.innerWidth < PHONESIZE);
const [isSub1k, setIsSub1k] = createSignal(window.innerWidth < SUB1K);

function listen() {
  setIsPhone(window.innerWidth < PHONESIZE);
  setIsSub1k(window.innerWidth < SUB1K);
}
window.addEventListener("resize", listen);

export default function useIsPhone() {
  return isPhone;
}

export function useIsSub1000(){
  return isSub1k;
}
