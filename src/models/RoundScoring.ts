import { z } from "zod";

const RoundScoringTypeValues = [
  "SUMOFDIFFERENCES",
  "ROUNDSWON",
  "FIX",
] as const;
export const RoundScoringTypes = z.enum(RoundScoringTypeValues);

export type TWinnerScoringTypes = z.infer<typeof RoundScoringTypes>;
export type TLoserScorintTypes = Exclude<
  TWinnerScoringTypes,
  "SUMOFDIFFERENCES"
>;

export const MultiplierSchema = z.coerce
  .number()
  .nonnegative({ message: "Cannot be less than 0!" })
  .max(99, { message: "Maximum is 99" });

export const AmountSchema = z.coerce
  .number()
  .min(1, { message: "Minimum is 1!" })
  .max(999, { message: "Maximum is 999!" });
export const LoserAmountSchema = z.coerce
  .number()
  .min(0, { message: "Minimum is 0!" })
  .max(999, { message: "Maximum is 999!" });
const FixScoreSchema = z.object({
  type: z.literal(RoundScoringTypes.Values.FIX),
  amount: AmountSchema,
});
const LoserFixScoreSchema = z.object({
  type: z.literal(RoundScoringTypes.Values.FIX),
  amount: LoserAmountSchema,
});

const WinnerVariableScoreSchema = z.object({
  type: z.enum([
    RoundScoringTypes.Values.ROUNDSWON,
    RoundScoringTypes.Values.SUMOFDIFFERENCES,
  ]),
  multiplier: MultiplierSchema.optional(),
});

const LoserVariableScoreSchema = z.object({
  type: z.literal(RoundScoringTypes.Values.ROUNDSWON),
  multiplier: MultiplierSchema.optional(),
});

export const RoundScoringNameSchema = z
  .string()
  .min(3, { message: "Min length is 3!" })
  .max(50, { message: "Max length is 50!" });

export const RoundScoringDBSchema = z.object({
  name: RoundScoringNameSchema,
  winner: z.discriminatedUnion("type", [
    FixScoreSchema,
    WinnerVariableScoreSchema,
  ]),
  loser: z.discriminatedUnion("type", [
    LoserFixScoreSchema,
    LoserVariableScoreSchema,
  ]),
  customColor: z.string().optional(),
});
export type RoundScoringDB = z.infer<typeof RoundScoringDBSchema>;

export const RoundScoringSchema = RoundScoringDBSchema.extend({
  hash: z.string(),
});
// TODO: refactor
export type RoundScoring = z.infer<typeof RoundScoringSchema>;

export type RoundScoringDBInstance = RoundScoringDB & {
  roundNumber: number;
};

export type RoundScoringDBRecord = {
  id: string;
  updated: string;
  created: string;
  name: string;
  winnertype: TWinnerScoringTypes;
  winneramount: number;
  losertype: TLoserScorintTypes;
  loseramount: number;
  customcolor?: string;
  roundnumber: number;
  tournament: string;
};

export function RoundScoringRecordToModel(
  record?: RoundScoringDBRecord,
): RoundScoringDBInstance | null {
  if (!record) return null;
  return {
    name: record.name,
    winner: {
      ...(record.winnertype === "FIX"
        ? { type: record.winnertype, amount: record.winneramount }
        : {
            type: record.winnertype,
            multiplier: record.winneramount,
          }),
    },
    loser: {
      ...(record.losertype === "FIX"
        ? { type: record.losertype, amount: record.loseramount }
        : {
            type: record.losertype,
            multiplier: record.loseramount,
          }),
    },
    customColor: record.customcolor,
    roundNumber: record.roundnumber,
  };
}

/*
const test: RoundScoring = {
  name: "valami",
  winner: {
    type: "SUMOFDIFFERENCES",
    multiplier: 69
  },
  loser: {
    type: "FIX",
    amount: 1
  }
}
*/
