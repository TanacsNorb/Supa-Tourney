import { z } from "zod";

const ScoringRuleValues = [
  "Once",
  "Multiple",
  "Simultaneous",
  "Undetermined",
] as const;
export const ScoringRule = z.enum(ScoringRuleValues);
export type TScoringRule = z.infer<typeof ScoringRule>;

const DefaultSportValues = [
  "Darts",
  "Beerpong",
  "Snooker",
  "Tablefootball",
] as const;
export const DefaultSport = z.enum(DefaultSportValues);
export type TDefaultSport = z.infer<typeof DefaultSport>;

export const SportSchemaName = z
  .string()
  .min(3, { message: "Min length is 3!" })
  .max(50, { message: "Max length is 50!" });
export const SportSchemaRounds = z.coerce
  .number({
    invalid_type_error: "Input must be a number!",
    required_error: "Required",
  })
  .min(1, { message: "Minimum is 1" })
  .max(16, { message: "Max length is 16!" });
export const SportSchemaScoreStart = z.coerce.number({
  invalid_type_error: "Input must be a number!",
  required_error: "Required",
});
export const SportSchemaScoreGoal = z.coerce.number();
export const SportSchemaScoresPerTurn = z.coerce
  .number({
    invalid_type_error: "Input must be a number!",
    required_error: "Required",
  })
  .min(2, { message: "Minimum is 2" })
  .max(32, { message: "Maximum is 32!" });

export const SportSchema = z.object({
  name: SportSchemaName,
  rounds: SportSchemaRounds,
  scoreStart: SportSchemaScoreStart,
  scoreGoal: SportSchemaScoreGoal.optional(),
  fixedRounds: z
    .string()
    .transform((val) => val === "true")
    .or(z.boolean()),
  scoringRule: ScoringRule,
  scoresPerTurn: SportSchemaScoresPerTurn.optional(),
  commonInputs: z.array(z.number()).optional(),
  customColor: z.string().optional(),
  defaultSport: DefaultSport.optional(),
});

export const UniqueSportSchema = SportSchema.extend({
  hash: z.string().optional(),
});

export type Sport = z.infer<typeof SportSchema>;

export type UniqueSport = z.infer<typeof UniqueSportSchema>;

export type SportInstance = Sport & { roundNumber: number };

export type SportRecord = {
  id: string;
  updated: string;
  created: string;
  name: string;
  rounds: number;
  scorestart: number;
  scoregoal: number;
  roundsplayed: "best of" | "fixed";
  scoringrule: TScoringRule;
  scoresperturn?: number;
  commoninputs?: string;
  customcolor?: string;
  defaultsport?: TDefaultSport;
  roundnumber: number;
  tournament: string;
};

export function SportRecordToModel(
  record: SportRecord,
): SportInstance {
  return {
    name: record.name,
    rounds: record.rounds,
    scoreStart: record.scorestart,
    scoreGoal: record.scoregoal,
    fixedRounds: record.roundsplayed === "fixed" ? true : false,
    scoringRule: record.scoringrule,
    scoresPerTurn: record.scoresperturn,
    commonInputs: record.commoninputs
      ? record.commoninputs.split(",").map((num) => parseInt(num))
      : undefined,
    customColor: record.customcolor,
    defaultSport: record.defaultsport,
    roundNumber: record.roundnumber,
  };
}

