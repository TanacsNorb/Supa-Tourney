const PBENDPOINTS = {
  users: "users",
  roundScorings: "roundscorings",
  sports: "sports",
  temporaryUsers: "temporaryusers",
  tournaments: "tournaments",
  lobbyUsers: "lobbyusers",
  joinwithurl: "joinwithurl",
  setupGuest: "setupguest",
  leavetournament: "leavetournament",
  startTournament: "starttournament",
  tournamentsInGame: "tournamentsingame",
  getOpponents: "opponents",
  tournamentMatches: "tournamentmatches",
  startNextRound: "startnextround",
  scoring: "scoring",
  undeterminedFinished: "undeterminedfinished",
  getTournament: "gettournament"

} as const;

export default PBENDPOINTS;
