import request from "./request";

export async function devautologin() {
  if (import.meta.env.PROD) return;
  await request(async (pb) => {
    console.log(pb.authStore.token);
    if(pb.authStore.isValid) return;
    const authData = await pb
      .collection("users")
      .authWithPassword("tesztelek", import.meta.env.VITE_DEV_PASSWORD ?? "");
  });
}
