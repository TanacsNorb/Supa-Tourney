import { useNavigate } from "@solidjs/router";
import { newPocketbase } from "./request";

export default function userIsAuthenticated() {
  const navigate = useNavigate();
  const pb = newPocketbase();

  if (!pb.authStore.isValid) navigate("/", { replace: true });
  if (pb.authStore.model?.collectionName === "temporaryusers") {
    pb.collection("temporaryusers")
      .getOne(pb.authStore.model.id)
      .catch((_) => {
        window.localStorage.removeItem("pocketbase_auth");
        navigate("/", { replace: true });
      });
  }
}
