import PocketBase from "pocketbase";

const pb = new PocketBase(import.meta.env.VITE_PB_URL);
export default async function request(
  callback: (p: PocketBase) => Promise<any>,
) {
  const result = await callback(pb);
  return result;
}

export async function retryRequest(
  callback: (p: PocketBase) => Promise<any>,
  count: number,
) {
  let tries = 0;
  while (true) {
    try {
      if (tries !== 0) await new Promise((r) => setTimeout(r, tries * 350));
      const result = await callback(pb);
      return result;
    } catch (error) {
      tries++;
      if (tries >= count) throw new Error("Maximum request tries reached!");
    }
  }
}

export function newPocketbase() {
  return pb;
}
