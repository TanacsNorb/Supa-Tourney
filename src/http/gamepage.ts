import PBENDPOINTS from "./endpoints";
import { newPocketbase } from "./request";

const pb = newPocketbase();
export async function getOpponents() {
  const arr = await pb.send(PBENDPOINTS.getOpponents, { method: "GET" });
  return arr.reverse();
}
