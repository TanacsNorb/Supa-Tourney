import { Record } from "pocketbase";
import request, { retryRequest } from "./request";
import PBENDPOINTS from "./endpoints";
import {
  SportRecordToModel,
  SportRecord,
  SportInstance,
} from "../models/Sport";
import {
  RoundScoringDBInstance,
  RoundScoringDBRecord,
  RoundScoringRecordToModel,
} from "../models/RoundScoring";

export function getTournamentInfo(lobbyid: string) {
  return async () => {
    return await request(async (pb) => {
      const record = await pb
        .collection(PBENDPOINTS.tournaments)
        .getOne(lobbyid, {});
      return record as Record;
    });
  };
}

export function getLobbyPlayers(lobbyid: string) {
  return async () => {
    return await request(async (pb) => {
      const records = await pb
        .collection(PBENDPOINTS.lobbyUsers)
        .getFullList({ sort: "updated", filter: `tournament="${lobbyid}"` });
      return records as Record[];
    });
  };
}

export function getSportsPairs(): () => Promise<
  Array<[SportInstance, RoundScoringDBInstance | null]>
> {
  return async () => {
    return await request(async (pb) => {
      const sports = (await pb.collection("sports").getFullList({
        sort: "roundnumber",
      })) as SportRecord[];
      const roundscorings = (await pb.collection("roundscorings").getFullList({
        sort: "roundnumber",
      })) as RoundScoringDBRecord[];

      const result = [];
      for (let i = 0; i < sports.length; i++) {
        result.push([
          SportRecordToModel(sports[i]),
          RoundScoringRecordToModel(roundscorings[i]),
        ]);
      }

      return result as Array<[SportInstance, RoundScoringDBInstance | null]>;
    });
  };
}
