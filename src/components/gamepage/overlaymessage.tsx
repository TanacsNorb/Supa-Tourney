import { Dialog } from "@kobalte/core";
import anime from "animejs";
import { Accessor, Setter, Show, createEffect, createSignal } from "solid-js";
import { Portal } from "solid-js/web";

export type Message = {
  tint: "you" | "opponent";
  message: string;
};
type OverlayMessageProps = {
  open: Accessor<boolean>;
  setOpen: Setter<boolean>;
  message: Accessor<Message>;
};
export function OverlayMessage(props: OverlayMessageProps) {
  const [open, setOpen] = createSignal(false);
  let ref: HTMLDivElement;

  createEffect(() => {
    if (props.open()) {
      setOpen(true);
      setTimeout(() => {
        anime({
          targets: ref,
          opacity: [0.25, 1],
          translateY: ["-3rem", 0],
          easing: "easeOutCubic",
          duration: 500,
        });
      }, 1);

      setTimeout(() => {
        anime({
          targets: ref,
          opacity: [1, 0.5],
          scale: [1, 0],
          translateY: "-2rem",
          easing: "easeOutSine",
          duration: 250,
          complete() {
            props.setOpen(false);
            setOpen(false);
          },
        });
      }, 2250);
    }
  });

  return (
    <Show when={open()}>
      <Portal mount={document.body}>
        <div
          class="pointer-events-none fixed left-0 right-0 top-0 z-50 flex items-start justify-center overflow-auto pt-12 text-2xl font-bold leading-tight !outline-none md:pt-24 md:text-3xl lg:text-4xl"
          classList={{
            "text-accent-500": props.message().tint === "you",
            "text-primary-600": props.message().tint === "opponent",
          }}
        >
          <p
            ref={ref!}
            class="pointer-events-auto rounded-md p-2 text-center !outline-none backdrop-blur selection:bg-accent-500/30"
            classList={{
              "bg-accent-500/20 border-2 border-accent-500/30":
                props.message().tint === "you",
              "bg-primary-500/10 border-2 border-primary-500/25":
                props.message().tint === "opponent",
            }}
          >
            {props.message().message}
          </p>
        </div>
      </Portal>
    </Show>
  );
}
