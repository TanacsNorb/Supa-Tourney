import { For, Show, createEffect, createSignal } from "solid-js";
import { DefaultSportInputProps } from "../../pages/subpages/game/matchpage";
import { Button } from "@kobalte/core";
import { newPocketbase } from "../../http/request";
import PBENDPOINTS from "../../http/endpoints";
import Spinner from "../spinner";

export function DefaultDartsInput(props: DefaultSportInputProps) {
  const [multiplier, setMultiplier] = createSignal(1);
  const [buffer, setBuffer] = createSignal<number[]>([], {
    equals: (prev, next) => prev.length != next.length,
  });

  function fillBuffer() {
    const temp = [...buffer()];
    temp.length = 3;
    return temp.fill(-1, buffer().length, 3);
  }

  function handleScore(number: number) {
    setBuffer((prev) => {
      prev.push(number * multiplier());
      return prev;
    });
  }

  function removeLastScore() {
    setBuffer((arr) => {
      if (arr.length > 0) arr.length = arr.length - 1;
      return arr;
    });
  }

  createEffect(() => {
    if (buffer().length === 3) {
      props.sendScore(buffer()).then(() => {
        setBuffer((arr) => {
          arr.length = 0;
          return arr;
        });
      });
    }
  });

  return (
    <div
      class="relative p-2 transition-opacity duration-500 md:p-4"
      classList={{ "opacity-50": !props.isYourTurn() }}
    >
      <Show when={!props.isYourTurn()}>
        <span class="absolute inset-0 z-50 block" />
      </Show>
      <div class="relative mb-6 flex justify-center gap-8 md:mb-8">
        <For each={fillBuffer()}>
          {(num) => (
            <span class="box-content min-w-[3ch] rounded-md border-b-4 border-b-primary-950 bg-primary-800 p-1 text-center md:text-lg">
              <Show when={num !== -1} fallback={"X"}>
                {num}
              </Show>
            </span>
          )}
        </For>
        <Button.Root onClick={removeLastScore}>
          <svg
            fill="currentColor"
            stroke-width="0"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 16 16"
            height="1.5em"
            width="1.5em"
            class="absolute right-0 top-0 transition-colors duration-100 hover:text-primary-200 md:right-12 lg:right-48"
          >
            <path d="M5.83 5.146a.5.5 0 0 0 0 .708L7.975 8l-2.147 2.146a.5.5 0 0 0 .707.708l2.147-2.147 2.146 2.147a.5.5 0 0 0 .707-.708L9.39 8l2.146-2.146a.5.5 0 0 0-.707-.708L8.683 7.293 6.536 5.146a.5.5 0 0 0-.707 0z"></path>
            <path d="M13.683 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-7.08a2 2 0 0 1-1.519-.698L.241 8.65a1 1 0 0 1 0-1.302L5.084 1.7A2 2 0 0 1 6.603 1h7.08zm-7.08 1a1 1 0 0 0-.76.35L1 8l4.844 5.65a1 1 0 0 0 .759.35h7.08a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1h-7.08z"></path>
          </svg>
        </Button.Root>
      </div>
      <div class="mb-4 flex justify-center gap-2 md:gap-8">
        <Button.Root
          onClick={() => setMultiplier(1)}
          class="rounded-md px-2 py-1 text-lg outline outline-2 outline-offset-2 outline-primary-200/0 transition-[outline-color,_outline-offset] duration-100 hover:outline-primary-200 active:outline-offset-0"
          classList={{
            "!outline-offset-0 !outline-primary-200": multiplier() === 1,
          }}
        >
          Single
        </Button.Root>
        <Button.Root
          onClick={() => setMultiplier(2)}
          class="rounded-md px-2 py-1 text-lg outline outline-2 outline-offset-2 outline-primary-200/0 transition-[outline-color,_outline-offset] duration-100 hover:outline-primary-200 active:outline-offset-0"
          classList={{
            "!outline-offset-0 !outline-primary-200": multiplier() === 2,
          }}
        >
          Double
        </Button.Root>
        <Button.Root
          onClick={() => setMultiplier(3)}
          class="rounded-md px-2 py-1 text-lg outline outline-2 outline-offset-2 outline-primary-200/0 transition-[outline-color,_outline-offset] duration-100 hover:outline-primary-200 active:outline-offset-0"
          classList={{
            "!outline-offset-0 !outline-primary-200": multiplier() === 3,
          }}
        >
          Triple
        </Button.Root>
      </div>
      <div class="flex flex-wrap justify-center gap-4">
        <For each={Array(21)}>
          {(_, index) => (
            <Button.Root
              class="box-content min-w-[3ch] rounded-sm border p-2 transition-colors duration-100 hover:border-primary-200 hover:bg-primary-200/25 fine:p-1"
              onClick={[handleScore, index()]}
            >
              {index()}
            </Button.Root>
          )}
        </For>
      </div>
    </div>
  );
}

export function DefaultBeerpongInput(props: DefaultSportInputProps) {
  const [buffer, setBuffer] = createSignal<number[]>([], {
    equals: (prev, next) => prev.length != next.length,
  });

  function handleScore(number: number) {
    setBuffer((prev) => {
      prev.push(number);
      return prev;
    });
  }

  function removeLastScore() {
    setBuffer((arr) => {
      if (arr.length > 0) arr.length = arr.length - 1;
      return arr;
    });
  }

  function handleSendScore(scores: number[]) {
    if (buffer().length <= 0) return;
    props.sendScore(scores).then((_) => {
      setBuffer((prev) => {
        prev.length = 0;
        return prev;
      });
    });
  }

  return (
    <div
      class="relative p-2 transition-opacity duration-500 md:p-4"
      classList={{ "opacity-50": !props.isYourTurn() }}
    >
      <Show when={!props.isYourTurn()}>
        <span class="absolute inset-0 z-50 block" />
      </Show>
      <div class="relative mb-6 flex justify-center gap-8 md:mb-8">
        <For
          each={buffer()}
          fallback={
            <span class="box-content min-w-[3ch] rounded-md border-b-4 bg-primary-800 p-1 text-center opacity-0 md:text-lg">
              0
            </span>
          }
        >
          {(num) => (
            <span class="box-content min-w-[3ch] rounded-md border-b-4 border-b-primary-950 bg-primary-800 p-1 text-center md:text-lg">
              <Show when={num !== -1} fallback={"X"}>
                {num}
              </Show>
            </span>
          )}
        </For>
        <Button.Root onClick={removeLastScore}>
          <svg
            fill="currentColor"
            stroke-width="0"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 16 16"
            height="1.5em"
            width="1.5em"
            class="absolute right-0 top-0 transition-colors duration-100 hover:text-primary-200 md:right-12 lg:right-48"
          >
            <path d="M5.83 5.146a.5.5 0 0 0 0 .708L7.975 8l-2.147 2.146a.5.5 0 0 0 .707.708l2.147-2.147 2.146 2.147a.5.5 0 0 0 .707-.708L9.39 8l2.146-2.146a.5.5 0 0 0-.707-.708L8.683 7.293 6.536 5.146a.5.5 0 0 0-.707 0z"></path>
            <path d="M13.683 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-7.08a2 2 0 0 1-1.519-.698L.241 8.65a1 1 0 0 1 0-1.302L5.084 1.7A2 2 0 0 1 6.603 1h7.08zm-7.08 1a1 1 0 0 0-.76.35L1 8l4.844 5.65a1 1 0 0 0 .759.35h7.08a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1h-7.08z"></path>
          </svg>
        </Button.Root>
      </div>
      <div class="flex flex-wrap justify-center gap-4">
        <For each={Array(3)}>
          {(_, index) => (
            <Button.Root
              class="box-content min-w-[3ch] rounded-sm border p-2 transition-colors duration-100 hover:border-primary-200 hover:bg-primary-200/25 fine:p-1"
              onClick={[handleScore, index()]}
            >
              {index()}
            </Button.Root>
          )}
        </For>
        <Button.Root
          onClick={[handleSendScore, buffer()]}
          class="ml-2 rounded-md border border-primary-400 bg-primary-400/20 p-2 transition-all hover:bg-primary-400 hover:text-black active:scale-90 md:ml-4 fine:p-1"
        >
          Turn finished
        </Button.Root>
      </div>
    </div>
  );
}

export function DefaultTableFootballInput(props: DefaultSportInputProps) {
  return (
    <div
      class="flex flex-1 self-center p-2 transition-opacity duration-500 md:p-4"
      classList={{ "opacity-50": !props.isYourTurn() }}
    >
      <Button.Root
        class="mx-auto h-full w-full max-w-xs rounded-md border border-primary-400 bg-primary-400/10 px-4 py-4 text-lg font-bold transition-all hover:bg-primary-400 hover:text-black active:scale-90 md:py-2"
        onClick={[props.sendScore, [1]]}
      >
        Goal!
      </Button.Root>
    </div>
  );
}

export type UndeterminedSportProps = {
  isMatchFinishedInitiated: boolean;
};
export function DefaultSnookerInput(
  props: DefaultSportInputProps & UndeterminedSportProps,
) {
  const inputs = [-7, -6, -5, -4, 0, 1, 2, 3, 4, 5, 6, 7];
  const [buffer, setBuffer] = createSignal<number[]>([], {
    equals: (prev, next) => prev.length != next.length,
  });

  function handleScore(number: number) {
    setBuffer((prev) => {
      prev.push(number);
      return prev;
    });
  }

  function removeLastScore() {
    setBuffer((arr) => {
      if (arr.length > 0) arr.length = arr.length - 1;
      return arr;
    });
  }

  function handleSendScore(scores: number[]) {
    if (buffer().length <= 0) return;
    props.sendScore(scores).then((_) => {
      setBuffer((prev) => {
        prev.length = 0;
        return prev;
      });
    });
  }

  return (
    <div class="flex-1 p-2 md:p-4">
      <div
        class="relative transition-opacity duration-500"
        classList={{ "opacity-50": !props.isYourTurn() }}
      >
        <Show when={!props.isYourTurn()}>
          <span class="absolute inset-0 z-50 block" />
        </Show>
        <div class="relative mb-6 flex flex-wrap justify-center gap-8 md:mb-8">
          <For
            each={buffer()}
            fallback={
              <span class="box-content min-w-[3ch] rounded-md border-b-4 bg-primary-800 p-1 text-center opacity-0 md:text-lg">
                0
              </span>
            }
          >
            {(num) => (
              <span class="box-content min-w-[3ch] rounded-md border-b-4 border-b-primary-950 bg-primary-800 p-1 text-center md:text-lg">
                <Show when={num !== -1} fallback={"X"}>
                  {num}
                </Show>
              </span>
            )}
          </For>
          <Button.Root onClick={removeLastScore}>
            <svg
              fill="currentColor"
              stroke-width="0"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 16 16"
              height="1.5em"
              width="1.5em"
              class="absolute right-0 top-0 transition-colors duration-100 hover:text-primary-200 md:right-12 lg:right-48"
            >
              <path d="M5.83 5.146a.5.5 0 0 0 0 .708L7.975 8l-2.147 2.146a.5.5 0 0 0 .707.708l2.147-2.147 2.146 2.147a.5.5 0 0 0 .707-.708L9.39 8l2.146-2.146a.5.5 0 0 0-.707-.708L8.683 7.293 6.536 5.146a.5.5 0 0 0-.707 0z"></path>
              <path d="M13.683 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-7.08a2 2 0 0 1-1.519-.698L.241 8.65a1 1 0 0 1 0-1.302L5.084 1.7A2 2 0 0 1 6.603 1h7.08zm-7.08 1a1 1 0 0 0-.76.35L1 8l4.844 5.65a1 1 0 0 0 .759.35h7.08a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1h-7.08z"></path>
            </svg>
          </Button.Root>
        </div>
        <div class="flex flex-wrap justify-center gap-4">
          <For each={inputs}>
            {(value) => (
              <Button.Root
                class="box-content min-w-[3ch] rounded-sm border p-2 transition-colors duration-100 hover:border-primary-200 hover:bg-primary-200/25 fine:p-1"
                onClick={[handleScore, value]}
              >
                {value}
              </Button.Root>
            )}
          </For>
          <div class="w-full md:hidden" />
          <Button.Root
            onClick={[handleSendScore, buffer()]}
            class="ml-2 rounded-md border border-primary-400 bg-primary-400/20 p-2 transition-all hover:bg-primary-400 hover:text-black active:scale-90 md:ml-4 fine:p-1"
          >
            Turn finished
          </Button.Root>
        </div>
      </div>
      <div class="flex justify-center pt-6">
        <Button.Root
          disabled={props.isMatchFinishedInitiated}
          onClick={() => {
            if (props.isMatchFinishedInitiated) return;

            const pb = newPocketbase();
            pb.send(`${PBENDPOINTS.undeterminedFinished}/init`, {
              method: "POST",
            });
          }}
          class="w-full max-w-xs rounded-md border border-primary-600 p-3 transition-colors fine:p-2"
          classList={{
            "bg-primary-600/0 flex justify-center gap-4":
              props.isMatchFinishedInitiated,
            "bg-primary-600/25 hover:bg-primary-600/50":
              !props.isMatchFinishedInitiated,
          }}
        >
          <Show
            when={!props.isMatchFinishedInitiated}
            fallback={
              <>
                Confirming
                <Spinner show size="textxl" />
              </>
            }
          >
            Round is finished!
          </Show>
        </Button.Root>
      </div>
    </div>
  );
}
