import { A } from "@solidjs/router";

type BackButtonProps = {
  label?: string
}
export default function BackButton(props: BackButtonProps) {
  return (
    <A
      href=".."
      class="group inline-flex items-center gap-1 rounded-br-lg border-b border-r pl-1 pr-1 pt-1 text-sm text-accent-200/50 transition-colors hover:text-accent-200 md:pb-1 md:pl-2 md:pt-2 md:text-base"
    >
      <svg
        fill="currentColor"
        stroke-width="0"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 16 16"
        style="overflow: visible;"
        height="1.25em"
        width="1.25em"
        class="transition-transform group-hover:-translate-x-1"
      >
        <path
          fill-rule="evenodd"
          d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"
        ></path>
      </svg>
      <span class="block">{props.label ?? "Back"}</span>
    </A>
  );
}
