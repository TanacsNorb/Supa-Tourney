import { Show } from "solid-js";

type SpinnerProps = {
  size?: "text" | "textxl" | "big" | "giant";
  show?: boolean;
};
export default function Spinner(props: SpinnerProps) {
  return (
    <Show when={Boolean(props.show)}>
      <div
        class="relative"
        classList={{
          "w-[1em] h-[1em]": props.size === "text",
          "w-[1.5em] h-[1.5em]": props.size === "textxl",
          "w-[3.5em] h-[3.5em]": props.size === "big" || !props.size,
          "w-[6em] h-[6em]": props.size === "giant",
        }}
      >
        <span class="absolute inset-0 block rounded-full [box-shadow:_0_0_10px_3px_rgba(175,175,175,0.4)_inset]" />
        <span class="absolute inset-0 block animate-spin rounded-full [box-shadow:_0_2px_2px_#22D3EE_inset]" />
      </div>
    </Show>
  );
}
