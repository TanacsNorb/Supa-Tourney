import { RadioGroup } from "@kobalte/core";
import { For, onMount } from "solid-js";

export default function ButtonGroup(props: ButtonGroupProps) {
  onMount(() => {
    if (props.defaultValue && props.onChange)
      props.onChange(props.defaultValue);
  });

  return (
    <RadioGroup.Root
      name={props.name}
      required={props.required}
      disabled={props.disabled}
      classList={{ "opacity-75": props.disabled }}
      onChange={props.onChange}
      defaultValue={props.defaultValue}
    >
      <RadioGroup.Label class="pl-2 text-[0.875em] empty:hidden">
        {props.label}
      </RadioGroup.Label>
      <div class="flex flex-row flex-wrap">
        <For each={props.buttons}>
          {(button) => (
            <RadioGroup.Item
              value={button.value}
              class="group relative isolate min-w-max flex-1 overflow-hidden border border-secondary/50 p-2 text-center first-of-type:rounded-l-sm last-of-type:rounded-r-sm hover:cursor-pointer focus-visible:bg-error-50"
              classList={{ "pointer-events-none": props.disabled }}
            >
              <RadioGroup.ItemInput class="peer absolute -bottom-1 left-0" />
              <RadioGroup.ItemControl class="peer absolute inset-0 -z-10 bg-accent-50/0 transition-colors duration-100 group-hover:bg-primary-800/10 peer-focus:bg-primary-800/10 ui-checked:!bg-primary-800/30" />
              <RadioGroup.ItemLabel
                class="pointer-events-none relative block transition-colors duration-100 ui-checked:font-bold"
                classList={{ "line-through": props.disabled }}
              >
                <span aria-hidden="true" class="invisible font-bold">
                  {button.label}
                </span>
                <span class="absolute inset-0">{button.label}</span>
              </RadioGroup.ItemLabel>
            </RadioGroup.Item>
          )}
        </For>
      </div>
      <RadioGroup.Description class="py-[0.25rem] pl-1 text-[0.875em] leading-none opacity-75 empty:hidden">
        {props.description}
      </RadioGroup.Description>
    </RadioGroup.Root>
  );
}

type ButtonGroupProps = {
  label?: string;
  name: string;
  required?: boolean;
  disabled?: boolean;
  buttons: Buttons[];
  onChange?: (value: string) => void;
  defaultValue?: string;
  description?: string;
};

type Buttons = {
  label: string;
  value: string;
};
