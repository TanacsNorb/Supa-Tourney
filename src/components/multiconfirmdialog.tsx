import { Dialog } from "@kobalte/core";
import { For, createSignal } from "solid-js";

type DialogButton = {
  label: string;
  value: string;
};
type MultiDialogProps = {
  callback: (confirm: string) => void;
  title?: string;
  main: DialogButton;
  additionalButtons?: DialogButton[];
};
const [multiDialogProps, setMultiDialogProps] = createSignal<MultiDialogProps>({
  callback: () => {},
  main: { value: "", label: "" },
});
const [open, setOpen] = createSignal(false);

export default function ToMultipleConfirm(props: MultiDialogProps) {
  setOpen(true);
  setMultiDialogProps(props);
}

export function MultiDialog() {
  return (
    <Dialog.Root open={open()}>
      <Dialog.Portal>
        <Dialog.Overlay class="fixed inset-0 z-50 flex items-baseline justify-center overflow-auto px-4 py-8 backdrop-blur-[1px] ui-expanded:animate-confirmenter ui-closed:animate-modaloverlayexit">
          <Dialog.Content
            onClick={(e: MouseEvent) => e.stopPropagation()}
            class="relative w-full max-w-[550px] rounded-sm border border-primary-200 bg-background p-2 shadow-xl shadow-black md:p-4"
          >
            <Dialog.Title class="flex-1 break-words text-center text-lg font-bold leading-none md:text-xl">
              {multiDialogProps().title ?? "Are you sure?"}
            </Dialog.Title>
            <div class="mt-6 flex justify-end gap-6 md:mt-8 md:gap-8">
              <For each={multiDialogProps().additionalButtons}>
                {(button) => (
                  <Dialog.CloseButton
                    onClick={() => {
                      setOpen(false);
                      multiDialogProps().callback(button.value);
                    }}
                    class="transition-color rounded-md px-2 text-base leading-none outline-none duration-100 hover:bg-secondary/25 focus-visible:bg-secondary/25 md:text-lg"
                  >
                    {button.label}
                  </Dialog.CloseButton>
                )}
              </For>
              <Dialog.CloseButton
                onClick={() => {
                  setOpen(false);
                  multiDialogProps().callback(multiDialogProps().main.value);
                }}
                class="rounded-md px-2 text-base leading-none outline outline-2 outline-error-400 transition-[outline-offset,color] duration-100 hover:text-error-400 hover:outline-offset-2 focus-visible:text-error-400 focus-visible:outline-offset-2 md:text-lg"
              >
                {multiDialogProps().main.label}
              </Dialog.CloseButton>
            </div>
          </Dialog.Content>
        </Dialog.Overlay>
      </Dialog.Portal>
    </Dialog.Root>
  );
}
