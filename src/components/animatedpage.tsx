import { JSX, ParentProps } from "solid-js";
import { createEffect } from "solid-js";
import createPageAnimation from "../hooks/createpageanimation";
import createPageInitAnimation from "../hooks/createpageinitanimation";

type AnimatedPageProps = {
  onScroll?: JSX.EventHandlerUnion<HTMLDivElement, Event> | undefined
};
export default function AnimatedPage(props: ParentProps<AnimatedPageProps>) {
  let ref: HTMLDivElement;
  let sessionStarted = window.sessionStorage.getItem("sessionStarted");

  createEffect(() => {
    if (sessionStarted !== "true") {
      window.sessionStorage.setItem("sessionStarted", "true");
      createPageInitAnimation(ref);
    }
    else createPageAnimation(ref);
  });

  return (
    <div
      class="fixed inset-0 overflow-auto rounded-xl outline outline-4 outline-offset-[6px] outline-accent-200/50"
      onScroll={props.onScroll}
      ref={ref!}
    >
      {props.children}
    </div>
  );
}
