import { Button } from "@kobalte/core";
import { Show } from "solid-js";
import Spinner from "./spinner";

type LoadingButtonProps = {
  label?: string;
  loading: boolean;
  disabled?: boolean;
  onClick: () => void;
};
export default function LoadingButton(props: LoadingButtonProps) {
  return (
    <Button.Root
      type="button"
      disabled={props.disabled}
      class="mx-auto grid w-full max-w-[450px] place-items-center rounded-md border border-primary-400 p-2 outline-none transition-[color,background-color,transform] hover:bg-primary-400 hover:text-black focus-visible:bg-primary-400 focus-visible:text-black active:scale-95"
      classList={{
        "pointer-events-none !bg-primary-400/0": props.loading,
        "opacity-80 pointer-events-none border-primary-400/50 disabled:hover:!bg-primary-400/0 disabled:hover:!text-colortext":
          Boolean(props.disabled),
      }}
      onClick={props.onClick}
    >
      <Show when={!props.loading} fallback={<Spinner show size="textxl" />}>
        {props.label}
      </Show>
    </Button.Root>
  );
}
