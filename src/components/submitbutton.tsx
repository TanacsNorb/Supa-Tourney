import { Button } from "@kobalte/core";
import { Show } from "solid-js";
import Spinner from "./spinner";

type SubmitButtonProps = {
  label?: string;
  loading: boolean;
  disabled?: boolean;
};
export default function SubmitButton(props: SubmitButtonProps) {
  return (
    <Button.Root
      type="submit"
      disabled={props.disabled}
      class="mx-auto mt-4 grid w-full max-w-[450px] place-items-center rounded-md border border-primary-400 p-2 outline-none transition-[color,background-color,transform] hover:bg-primary-400 hover:text-black focus-visible:bg-primary-400 focus-visible:text-black active:scale-95"
      classList={{
        "pointer-events-none !bg-primary-400/0": props.loading,
        "opacity-80 pointer-events-none border-primary-400/50": Boolean(
          props.disabled,
        ),
      }}
    >
      <Show when={!props.loading} fallback={<Spinner show size="textxl" />}>
        {props.label}
      </Show>
    </Button.Root>
  );
}
