import { TextField, ToggleButton } from "@kobalte/core";
import { TextFieldInputProps } from "@kobalte/core/dist/types/text-field";
import { For, Show, createSignal, splitProps } from "solid-js";
import { ZodNumber, ZodString } from "zod";

export default function TextFieldInput(
  props: TextFieldInputProps & MyTextFieldInputProps,
) {
  const [local, inputProps] = splitProps(props, [
    "class",
    "classList",
    "type",
    "onBlur",
    "ref",
    "label",
    "description",
    "schema",
    "inputref",
  ]);
  const [isValid, setIsValid] = createSignal<"valid" | "invalid">("valid");
  const [errorMessages, setErrorMessages] = createSignal<string[]>([]);
  const [showPassword, setShowPassword] = createSignal(false);

  function handleBlur(event: FocusEvent & { currentTarget: HTMLInputElement }) {
    if (!local.schema) return;
    const input = event.currentTarget;

    if (input.disabled) return;
    const result = local.schema.safeParse(input.value);
    if (!result.success) {
      setIsValid("invalid");
      setErrorMessages(result.error.format()._errors);
    } else {
      setIsValid("valid");
      setErrorMessages([]);
    }
  }

  return (
    <TextField.Root
      class="flex w-full flex-col transition-[border-color,_opacity] duration-75"
      classList={{
        "opacity-50 !border-l-error-500/0": inputProps.disabled,
      }}
      validationState={isValid()}
      required={inputProps.required}
    >
      <TextField.Label
        class="border-l border-l-primary-400 pl-2 text-[0.875em] empty:hidden"
        classList={{
          "!border-l-error-500": isValid() === "invalid",
          "!border-l-primary-400/0": inputProps.disabled,
        }}
      >
        {local.label}
      </TextField.Label>
      <div class="group relative">
        <TextField.Input
          ref={local.inputref}
          onBlur={handleBlur}
          class="w-full border-b border-l border-b-secondary/0 border-l-primary-400 bg-secondary/10 px-[0.75em] py-[0.375em] outline-none transition-[border-color,_opacity] duration-75 read-only:!cursor-default read-only:!border-b-secondary/0 read-only:!bg-secondary/5 read-only:opacity-80 hover:bg-secondary/20 focus:border-b-primary-400 disabled:!border-l-primary-400/0 disabled:bg-secondary/5 group-hover:bg-secondary/20 ui-invalid:focus:border-b-error-500"
          classList={{
            "!border-error-500": isValid() === "invalid",
            "pr-[2em]": local.type === "password",
          }}
          type={
            local.type === "password"
              ? showPassword()
                ? "text"
                : "password"
              : "text"
          }
          {...inputProps}
        />
        <PasswordShowButton
          isPassword={local.type === "password"}
          onChange={setShowPassword}
        />
      </div>
      <TextField.Description class="py-[0.125rem] pl-1 text-[0.875em] leading-none opacity-75 empty:hidden">
        {local.description}
      </TextField.Description>
      <TextField.ErrorMessage class="flex flex-wrap gap-2 pb-[0.125rem] pl-2 pt-1 text-[0.875em] empty:hidden md:gap-4">
        <For each={errorMessages()}>{(item) => <ErrorChip text={item} />}</For>
      </TextField.ErrorMessage>
    </TextField.Root>
  );
}

function ErrorChip(props: ErrorChipProps) {
  return (
    <p class="rounded-2xl border border-error-400 bg-error-400/25 px-1 md:px-2">
      {props.text}
    </p>
  );
}

type PasswordShowButtonProps = {
  isPassword: boolean;
  onChange: (state: boolean) => void;
};
function PasswordShowButton(props: PasswordShowButtonProps) {
  const [pressed, setPressed] = createSignal(false);

  if (!props.isPassword) return <></>;
  return (
    <ToggleButton.Root
      class="absolute right-1 top-[0.25em] px-1 py-[0.25em] transition-colors hover:text-primary-400"
      aria-label="Show/Hide password"
      title={pressed() ? "Hide password" : "Show password"}
      pressed={pressed()}
      onChange={(bool) => {
        props.onChange(bool);
        setPressed(bool);
      }}
    >
      {(state) => (
        <Show
          when={state.pressed()}
          fallback={
            <svg
              fill="currentColor"
              stroke-width="0"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 16 16"
              height="1.25em"
              width="1.25em"
            >
              <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"></path>
              <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"></path>
            </svg>
          }
        >
          <svg
            fill="currentColor"
            stroke-width="0"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 16 16"
            height="1.25em"
            width="1.25em"
          >
            <path d="M13.359 11.238C15.06 9.72 16 8 16 8s-3-5.5-8-5.5a7.028 7.028 0 0 0-2.79.588l.77.771A5.944 5.944 0 0 1 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.134 13.134 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755-.165.165-.337.328-.517.486l.708.709z"></path>
            <path d="M11.297 9.176a3.5 3.5 0 0 0-4.474-4.474l.823.823a2.5 2.5 0 0 1 2.829 2.829l.822.822zm-2.943 1.299.822.822a3.5 3.5 0 0 1-4.474-4.474l.823.823a2.5 2.5 0 0 0 2.829 2.829z"></path>
            <path d="M3.35 5.47c-.18.16-.353.322-.518.487A13.134 13.134 0 0 0 1.172 8l.195.288c.335.48.83 1.12 1.465 1.755C4.121 11.332 5.881 12.5 8 12.5c.716 0 1.39-.133 2.02-.36l.77.772A7.029 7.029 0 0 1 8 13.5C3 13.5 0 8 0 8s.939-1.721 2.641-3.238l.708.709zm10.296 8.884-12-12 .708-.708 12 12-.708.708z"></path>
          </svg>
        </Show>
      )}
    </ToggleButton.Root>
  );
}

type MyTextFieldInputProps = {
  label?: string;
  description?: string;
  schema?: ZodString | ZodNumber;
  inputref?: (ref: HTMLInputElement) => any;
};

type ErrorChipProps = {
  text: string;
};
