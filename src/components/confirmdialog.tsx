import { Dialog } from "@kobalte/core";
import { createSignal } from "solid-js";

type ConfirmDialogProps = {
  callback: (confirm: boolean) => void;
  title?: string;
  confirmLabel?: string;
  cancelLabel?: string;
};
const [confirmDialogProps, setConfirmDialogProps] =
  createSignal<ConfirmDialogProps>({ callback: () => {} });
const [open, setOpen] = createSignal(false);

export default function ToConfirm(props: ConfirmDialogProps) {
  setOpen(true);
  setConfirmDialogProps(props);
}

export function ConfirmDialog() {
  return (
    <Dialog.Root open={open()}>
      <Dialog.Portal>
        <Dialog.Overlay class="fixed inset-0 z-50 flex items-baseline justify-center overflow-auto p-4 backdrop-blur-[1px] ui-expanded:animate-confirmenter ui-closed:animate-modaloverlayexit">
          <Dialog.Content
            onClick={(e: MouseEvent) => e.stopPropagation()}
            class="relative w-full max-w-[400px] rounded-sm border border-primary-200 bg-background p-2 shadow-xl shadow-black md:p-4"
          >
            <Dialog.Title class="flex-1 break-words py-2 text-center text-lg font-bold leading-none md:text-xl">
              {confirmDialogProps().title ?? "Are you sure?"}
            </Dialog.Title>
            <div class="mt-6 flex justify-end gap-6 md:gap-10">
              <Dialog.CloseButton
                onClick={() => {
                  setOpen(false);
                  confirmDialogProps().callback(false);
                }}
                class="transition-color rounded-md p-2 text-base leading-none outline-none duration-100 hover:bg-secondary/25 focus-visible:bg-secondary/25 md:text-lg fine:!py-1"
              >
                {confirmDialogProps().cancelLabel ?? "Cancel"}
              </Dialog.CloseButton>
              <Dialog.CloseButton
                onClick={() => {
                  setOpen(false);
                  confirmDialogProps().callback(true);
                }}
                class="rounded-md p-2 text-base leading-none outline outline-2 outline-error-400 transition-[outline-offset,color] duration-100 hover:text-error-400 hover:outline-offset-2 focus-visible:text-error-400 focus-visible:outline-offset-2 md:text-lg fine:!py-1"
              >
                {confirmDialogProps().confirmLabel ?? "Yes"}
              </Dialog.CloseButton>
            </div>
          </Dialog.Content>
        </Dialog.Overlay>
      </Dialog.Portal>
    </Dialog.Root>
  );
}
