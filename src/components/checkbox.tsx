import { Checkbox as KobalteCheckbox } from "@kobalte/core";

export default function Checkbox(props: CheckboxProps) {
  return (
    <KobalteCheckbox.Root class="group">
      <KobalteCheckbox.Input class="peer" name={props.name} />
      <div class="inline-flex gap-4 items-center">
        <KobalteCheckbox.Control class="h-[1.5rem] w-[1.5rem] rounded-sm border border-secondary/50 bg-secondary/20 group-hover:border-secondary transition-colors duration-100">
          <KobalteCheckbox.Indicator>
            <svg
              fill="currentColor"
              stroke-width="0"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 16 16"
              height="1.25rem"
              width="1.25rem"
              class="scale-110"
            >
              <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z"></path>
            </svg>
          </KobalteCheckbox.Indicator>
        </KobalteCheckbox.Control>
        <KobalteCheckbox.Label class="empty:hidden">
          {props.label}
        </KobalteCheckbox.Label>
      </div>
      <KobalteCheckbox.Description class="pl-2 text-[0.875em] leading-none opacity-75 empty:hidden">
        {props.description}
      </KobalteCheckbox.Description>
    </KobalteCheckbox.Root>
  );
}

type CheckboxProps = {
  label?: string;
  name: string;
  required?: boolean;
  disabled?: boolean;
  defaultChecked?: boolean;
  description?: string;
};
