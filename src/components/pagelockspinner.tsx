import { Dialog } from "@kobalte/core";
import Spinner from "./spinner";

type PageLockSpinnerProps = {
  title?: string;
  show?: boolean;
};
export default function PageLockSpinner(props: PageLockSpinnerProps) {
  return (
    <Dialog.Root open={Boolean(props.show)}>
      <Dialog.Portal>
        <Dialog.Overlay class="fixed inset-0 z-50 grid place-items-center overflow-auto bg-black/20 p-2 backdrop-blur-sm">
          <Dialog.Content>
            <Dialog.Title class="mb-4 text-center text-2xl font-bold leading-none empty:hidden md:text-3xl">
              {props.title}
            </Dialog.Title>
            <div class="grid place-items-center">
              <Spinner show={true} size="giant" />
            </div>
          </Dialog.Content>
        </Dialog.Overlay>
      </Dialog.Portal>
    </Dialog.Root>
  );
}
