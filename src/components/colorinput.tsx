import { createSignal } from "solid-js";
import { isDarkTextBetterContrast, rgbaToRgbHex } from "../utils/luminance";

export default function ColorInput() {
  const [color, setColor] = createSignal("#555555");
  const textColor1 = () =>
    isDarkTextBetterContrast(color()) ? "#111" : "#eee";
  const textColor2 = () =>
    isDarkTextBetterContrast(rgbaToRgbHex(color() + "90", "#160630"))
      ? "#111"
      : "#eee";

  function handleColor(e: InputEvent & { currentTarget: HTMLInputElement }) {
    setColor(e.currentTarget.value);
  }

  return (
    <div class="grid grid-cols-2 gap-2 rounded-sm border border-secondary/50 p-1 md:flex md:items-center md:justify-center md:gap-4">
      <label for="customcolorinput" class="ml-auto text-center text-[0.875em]">
        Custom accent color
      </label>
      <input
        id="customcolorinput"
        name="customColor"
        type="color"
        onInput={handleColor}
        value={"#555555"}
      />
      <label class="ml-auto text-center text-[0.875em]">
        How it would look:
      </label>
      <div
        class="flex w-[6ch] flex-col gap-1 self-stretch overflow-hidden rounded-t-md border"
        style={{ "border-color": color() }}
      >
        <span
          class="grid h-4 w-full place-items-center"
          style={{ "background-color": color() }}
        >
          <span
            class="mx-auto block h-2 w-3/4 rounded-md"
            style={{ "background-color": textColor1() }}
          />
        </span>
        <span
          class="grid h-3 w-full place-items-center"
          style={{ "background-color": color() + "90" }}
        >
          <span
            class="mx-auto block h-1 w-3/4 rounded-md"
            style={{ "background-color": textColor2() }}
          />
        </span>
        <span
          class="grid h-3 w-full place-items-center"
          style={{ "background-color": color() + "90" }}
        >
          <span
            class="mx-auto block h-1 w-3/4 rounded-md"
            style={{ "background-color": textColor2() }}
          />
        </span>
      </div>
    </div>
  );
}
