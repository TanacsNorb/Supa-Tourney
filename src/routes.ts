import { lazy } from "solid-js";
import type { RouteDefinition } from "@solidjs/router";

import Home from "./pages/home";
import ROUTES from "./static/constants/routes";
import CreateTournament from "./pages/createtournament";
import JoinGame from "./pages/joingame";
import JoinUrl from "./pages/joinurl";

export const routes: RouteDefinition[] = [
  {
    path: "/",
    component: Home,
  },
  {
    path: ROUTES.dependencies,
    component: lazy(() => import("./pages/dependencies")),
  },
  {
    path: ROUTES.createTournament,
    component: CreateTournament,
  },
  {
    path: ROUTES.joinGame,
    component: JoinGame,
  },
  {
    path: ROUTES.lobby.route,
    component: lazy(() => import("./pages/lobby")),
  },
  {
    path: ROUTES.joinurl,
    component: JoinUrl,
  },
  {
    path: ROUTES.setupGuest,
    component: lazy(() => import("./pages/setupguest")),
  },
  {
    path: ROUTES.game,
    component: lazy(() => import("./pages/game")),
  },
  {
    path: ROUTES.tournamentEnded,
    component: lazy(() => import("./pages/tournamentended")),
  },
  {
    path: "**",
    component: lazy(() => import("./errors/404")),
  },
];
