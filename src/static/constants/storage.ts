const STORAGE = {
  userid: "userid",
  customSports: "customsports",
  customRoundScorings: "customroundscorings",
  initMessageRound: "initmessageround"
}

export default STORAGE;
