const ROUTES = {
  home: "/",
  dependencies: "/dependencies",
  joinGame: "/joingame",
  createTournament: "/createtournament",
  lobby: {
    route: "/lobby/:id",
    navigate: (id: string) => `/lobby/${id}`,
  },
  joinurl: "/joinurl",
  setupGuest: "/setupguest",
  game: "/game",
  tournamentEnded: "/tournamentended"
};

export default ROUTES;
