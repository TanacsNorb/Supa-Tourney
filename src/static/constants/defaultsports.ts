import { DefaultSport, ScoringRule, Sport } from "../../models/Sport";

const BASESPORTS = {
  darts: {
    name: "Default Darts",
    rounds: 11,
    scoreStart: 501,
    scoreGoal: 0,
    fixedRounds: false,
    scoringRule: ScoringRule.Enum.Multiple,
    scoresPerTurn: 3,
    defaultSport: DefaultSport.Enum.Darts,
  },
  beerpong: {
    name: "Default Beerpong",
    rounds: 1,
    scoreStart: 10,
    scoreGoal: 0,
    fixedRounds: true,
    scoringRule: ScoringRule.Enum.Undetermined,
    defaultSport: DefaultSport.Enum.Beerpong,
  },
  snooker: {
    name: "Default Snooker",
    rounds: 5,
    scoreStart: 0,
    scoreGoal: undefined,
    fixedRounds: false,
    scoringRule: ScoringRule.Enum.Undetermined,
    defaultSport: DefaultSport.Enum.Snooker,
  },
  tablefootball: {
    name: "Default Table football",
    rounds: 3,
    scoreStart: 0,
    scoreGoal: 10,
    fixedRounds: true,
    scoringRule: ScoringRule.Enum.Simultaneous,
    defaultSport: DefaultSport.Enum.Tablefootball,
  },
} as const;

export function BaseSportFromName(name: string){
  return Object.values(BASESPORTS).find(sport => sport.defaultSport === name);
}

export default BASESPORTS;
