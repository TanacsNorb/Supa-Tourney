const TAILWINDCUSTOM = Object.freeze({
  BG: "--custom-bg",
  TEXT: "--custom-text",
  BORDER: "--custom-border",
  OUTLINE: "--custom-outline",
});
export default TAILWINDCUSTOM;
