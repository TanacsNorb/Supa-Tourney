import { RoundScoring } from "../../models/RoundScoring";

type BaseScoringListType = {
  [scoring: string]: RoundScoring;
};

// TODO: Unify this schema and backend
const BASESCORINGS: BaseScoringListType = {
  dartsScoring: {
    name: "Scoring for base darts",
    winner: {
      type: "ROUNDSWON",
    },
    loser: {
      type: "FIX",
      amount: 0,
    },
    hash: "2700518194031985"
  },
  beerpongScoring: {
    name: "Scoring for base beerpong",
    winner: {
      type: "FIX",
      amount: 10,
    },
    loser: {
      type: "FIX",
      amount: 0,
    },
    hash: "2973334727106112"
  },
  tableFootballScoring: {
    name: "Scoring for base table football",
    winner: {
      type: "ROUNDSWON",
      multiplier: 1.5,
    },
    loser: {
      type: "ROUNDSWON",
    },
    hash: "681094632888385"
  },
  snookerScoring: {
    name: "Scoring for base snooker",
    winner: {
      type: "SUMOFDIFFERENCES",
    },
    loser: {
      type: "FIX",
      amount: 0,
    },
    hash: "8012370174818705"
  },
};

export function BaseRoundScoringFromHash(hash: string){
  return Object.values(BASESCORINGS).find(scoring => scoring.hash === hash);
}

export default BASESCORINGS;
