export const TOURNAMENTTYPES = {
  koth: "koth",
  roundrobin: "roundrobin"
} as const;

type ValueOf<T> = T[keyof T];
export type TournamentTypes = ValueOf<typeof TOURNAMENTTYPES>;

export const TournamentTypeButtons = [
  {
    label: "King of the Hill",
    value: TOURNAMENTTYPES.koth,
  },
  {
    label: "Round Robin",
    value: TOURNAMENTTYPES.roundrobin,
  },
];
