export default function NotFound() {
  return (
    <section class="p-8 text-colortext">
      <h1 class="text-center text-2xl font-bold md:text-3xl">404: Not Found</h1>
      <p class="mt-4 text-center text-lg">It's gone 😞</p>
    </section>
  );
}
