import { test, expect } from "vitest";

function pocketbaseKothScores(playersArray) {
  const count = playersArray.length;
  const exponent = Math.ceil(Math.log2(count));
  const startingScore = Math.pow(2, exponent - 1);
  const scoreArray = [];
  for (let i = 0; i < count; i++) {
    scoreArray.push(startingScore - Math.floor(i / 4));
  }

  return scoreArray;
}

test("pocketbase score with 2 players", () => {
  expect(pocketbaseKothScores(["a", "b"])).toStrictEqual([1, 1]);
});

test("pocketbase score with 3 players", () => {
  expect(pocketbaseKothScores(["a", "b", "c"])).toStrictEqual([2, 2, 2]);
});

test("pocketbase score with 4 players", () => {
  expect(pocketbaseKothScores(["a", "b", "c", "d"])).toStrictEqual([
    2, 2, 2, 2,
  ]);
});
test("pocketbase score with 5 players", () => {
  expect(pocketbaseKothScores(["a", "b", "c", "d", "e"])).toStrictEqual([
    4, 4, 4, 4, 3,
  ]);
});

test("pocketbase score with 8 players", () => {
  expect(
    pocketbaseKothScores(["a", "b", "c", "d", "e", "f", "g", "h"]),
  ).toStrictEqual([4, 4, 4, 4, 3, 3, 3, 3]);
});

test("pocketbase score with 14 players", () => {
  expect(
    pocketbaseKothScores([
      "a",
      "b",
      "c",
      "d",
      "e",
      "f",
      "g",
      "h",
      "i",
      "j",
      "k",
      "l",
      "m",
      "n",
    ]),
  ).toStrictEqual([8, 8, 8, 8, 7, 7, 7, 7, 6, 6, 6, 6, 5, 5]);
});
