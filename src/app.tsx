import { createContext, type Component } from "solid-js";
import { useRoutes } from "@solidjs/router";
import { routes } from "./routes";
import { Portal } from "solid-js/web";
import { Toast, toaster } from "@kobalte/core";
import { ConfirmDialog } from "./components/confirmdialog";
import { MultiDialog } from "./components/multiconfirmdialog";

type ToastType = {
  description?: string;
  title: string;
};
export const ToasterContext = createContext({
  default: ({ description, title }: ToastType) => -1 as number,
  error: ({ description, title }: ToastType) => -1 as number,
});

const App: Component = () => {
  const Route = useRoutes(routes);
  const commonTwClasses =
    "relative rounded-bl-md rounded-tr-md border p-2 text-colortext backdrop-blur-md ui-opened:animate-toastenter ui-closed:animate-toastexit";

  function showDefault({ description, title }: ToastType) {
    const id = toaster.show((props) => (
      <Toast.Root
        toastId={props.toastId}
        class={
          "border-primary-400 bg-gradient-to-br from-primary-400/40 to-primary-400/10 " +
          commonTwClasses
        }
      >
        <div>
          <div>
            <Toast.Title class="pr-[1em] text-sm md:text-base">
              {title}
            </Toast.Title>
            <Toast.Description class="text-xs empty:hidden md:text-sm">
              {description}
            </Toast.Description>
          </div>
          <Toast.CloseButton
            onClick={() => console.log(props.toastId)}
            class="!pointer-events-auto absolute right-0 top-0 -translate-x-1 text-[1.5em] leading-none transition-transform duration-100 hover:scale-125"
          >
            {"\u26cc"}
          </Toast.CloseButton>
        </div>
      </Toast.Root>
    ));
    return id;
  }

  function showError({ description, title }: ToastType) {
    const id = toaster.show((props) => (
      <Toast.Root
        toastId={props.toastId}
        class={
          "border-error-400 bg-gradient-to-br from-error-400/40 to-error-400/10 " +
          commonTwClasses
        }
      >
        <div>
          <div>
            <Toast.Title class="pr-[1em] text-sm md:text-base">
              {title}
            </Toast.Title>
            <Toast.Description class="text-xs empty:hidden md:text-sm">
              {description}
            </Toast.Description>
          </div>
          <Toast.CloseButton class="!pointer-events-auto absolute right-0 top-0 -translate-x-1 text-[1.5em] leading-none transition-transform duration-100 hover:scale-125">
            {"\u26cc"}
          </Toast.CloseButton>
        </div>
      </Toast.Root>
    ));
    return id;
  }
  const toasts = {
    default: showDefault,
    error: showError,
  };

  return (
    <ToasterContext.Provider value={toasts}>
      <main>
        <Route />
      </main>
      <ConfirmDialog />
      <MultiDialog />
      <Portal>
        <Toast.Region
          class="fixed right-0 top-0 z-[99] w-[25ch] text-sm md:bottom-0 md:text-base"
          limit={3}
          swipeDirection="right"
          duration={4000}
          pauseOnPageIdle={false}
          style={{ "pointer-events": "none" }}
        >
          <Toast.List class="flex flex-col gap-4 p-2 md:gap-6 md:p-4" />
        </Toast.Region>
      </Portal>
    </ToasterContext.Provider>
  );
};

export default App;
