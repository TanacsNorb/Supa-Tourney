import { RadioGroup } from "@kobalte/core";
import AnimatedPage from "../components/animatedpage";
import BackButton from "../components/backbutton";
import SubmitButton from "../components/submitbutton";
import TextFieldInput from "../components/textfield";
import { FormSubmitEvent } from "../models/types";
import {
  For,
  Show,
  createSignal,
  onCleanup,
  onMount,
  useContext,
} from "solid-js";
import request, { newPocketbase } from "../http/request";
import { useNavigate } from "@solidjs/router";
import { z } from "zod";
import PBENDPOINTS from "../http/endpoints";
import { ToasterContext } from "../app";

export default function SetupGuest() {
  const navigate = useNavigate();
  const pocketbase = newPocketbase();
  const toast = useContext(ToasterContext);
  const [loading, setLoading] = createSignal(false);

  onMount(() => {
    if (
      pocketbase.authStore.isValid &&
      pocketbase.authStore.model?.collectionName !== "temporaryusers"
    )
      navigate("/", { replace: true });
  });

  function handleSubmit(e: FormSubmitEvent) {
    e.preventDefault();
    setLoading(true);
    const formData = new FormData(e.currentTarget);
    const jwt = window.localStorage.getItem("tmp_pocketbase");
    pocketbase
      .send(PBENDPOINTS.setupGuest, {
        method: "POST",
        body: {
          username: formData.get("username"),
          avatar: formData.get("avatar"),
          jwt,
        },
      })
      .then(async (response) => {
        const temporaryUserData = {
          username: response.username,
          password: response.genPassword,
          passwordConfirm: response.genPassword,
          selectedavatar: response.avatar,
          tournament: response.tournament,
          registered: false,
        };

        // Failed to authenticate when it was created on the backend
        await pocketbase
          .collection(PBENDPOINTS.temporaryUsers)
          .create(temporaryUserData);

        await pocketbase
          .collection(PBENDPOINTS.temporaryUsers)
          .authWithPassword(response.username, response.genPassword);
        window.localStorage.removeItem("tmp_pocketbase");
        window.localStorage.removeItem("tmp_avatar");
        navigate("../lobby/" + response.tournament, { replace: true });
      })
      .catch(() => {
        toast.error({
          title: "Bad Request!",
        });
      })
      .finally(() => setLoading(false));
  }

  return (
    <AnimatedPage>
      <BackButton />
      <section class="mx-2 my-4 max-w-[750px] overflow-hidden rounded-2xl bg-secondary/5 p-6 md:mx-auto md:mb-12 md:mt-[15vh] md:p-10 lg:p-12">
        <h1 class="mb-[max(8vh,_2rem)] border-b-2 border-primary-700 py-2 text-center text-[clamp(1.5rem,_4vw,_4.5rem)] font-bold leading-none md:-mt-10 md:py-4">
          Join a tournament
        </h1>
        <form onSubmit={handleSubmit} class="flex flex-col gap-12">
          <TextFieldInput
            name="username"
            label="Your name / Username"
            schema={z
              .string()
              .min(3, { message: "Min length is 3!" })
              .max(32, { message: "Max length is 32!" })
              .regex(new RegExp("^[a-zA-Z0-9]+$"), {
                message: "Letters and numbers only!",
              })}
            required
          />
          <AvatarSelection />
          <SubmitButton loading={loading()} label="Join" />
          <Timer />
        </form>
      </section>
    </AnimatedPage>
  );
}

function generateDicebearIds() {
  const jwt = window.localStorage.getItem("tmp_pocketbase")?.split("}")[1];
  if (!jwt) return ["random", "random2", "random3", "random4"];

  const length = Math.floor(jwt.length / 4);
  let res = [];
  for (let i = 0; i < 4; i++) {
    res.push(jwt.slice(i * length, (i + 1) * length));
  }
  return res;
}

function AvatarSelection() {
  const avatar = window.localStorage.getItem("tmp_avatar");
  const ids = generateDicebearIds();

  return (
    <RadioGroup.Root name="avatar" required>
      <RadioGroup.Label>Select your avatar!</RadioGroup.Label>
      <div class="mt-6 grid grid-cols-2 gap-12 md:grid-cols-4">
        <For each={ids}>
          {(id, index) => (
            <RadioGroup.Item value={id}>
              <RadioGroup.ItemInput />
              <RadioGroup.ItemControl class="flex cursor-pointer justify-center transition-transform ui-checked:motion-safe:animate-breath">
                <div class="relative isolate ">
                  <RadioGroup.ItemIndicator class="absolute -inset-1 -z-10 rounded-md bg-primary-600" />
                  <img
                    src={`https://api.dicebear.com/7.x/${avatar}/png?seed=${id}`}
                    alt={`#${index()} avatar choice`}
                    class="max-w-[4rem] rounded-md shadow-secondary/40 outline outline-2 outline-offset-4 outline-secondary/50 [box-shadow:_0_0_8px_8px_var(--tw-shadow-color)]"
                  />
                </div>
              </RadioGroup.ItemControl>
            </RadioGroup.Item>
          )}
        </For>
      </div>
    </RadioGroup.Root>
  );
}

function getExpirationTime() {
  try {
    const jwtSplit = window.localStorage.getItem("tmp_pocketbase")?.split("}");
    if (!jwtSplit) return null;

    const now = Date.now();
    const initTime = parseInt(JSON.parse(jwtSplit[0] + "}").exp);
    return initTime < now ? null : Math.round((initTime - now) / 1000);
  } catch (error) {
    return null;
  }
}

function formatTime(time: number) {
  const seconds = time % 60;

  if (time > 61 && seconds > 30) return `${Math.ceil(time / 60)} minutes left to join`;
  if (time > 60 && seconds <= 30)
    return `${Math.floor(time / 60)}.5 minutes left to join`;

  return `${seconds} seconds left join`;
}

function Timer() {
  const [timeLeft, setTimeLeft] = createSignal(getExpirationTime());
  let interval: number;
  if (timeLeft() !== null) {
    // @ts-ignore
    interval = setInterval(() => {
      // @ts-ignore
      setTimeLeft(timeLeft() - 1);
    }, 1000);
  }

  onCleanup(() => {
    clearInterval(interval);
  });

  return (
    <p class="-mt-10 text-center opacity-75">
      <Show
        when={typeof timeLeft() === "number" && timeLeft()! > 0}
        fallback="Time is up, login expired!"
      >
        {formatTime(timeLeft()!)}
      </Show>
    </p>
  );
}
