import { Button } from "@kobalte/core";
import AnimatedPage from "../components/animatedpage";
import BackButton from "../components/backbutton";
import TextFieldInput from "../components/textfield";
import userIsAuthenticated from "../http/authenticatedroute";
import { FormSubmitEvent } from "../models/types";
import { Show, createEffect, createSignal, useContext } from "solid-js";
import Spinner from "../components/spinner";
import request from "../http/request";
import { ToasterContext } from "../app";
import anime from "animejs";
import SubmitButton from "../components/submitbutton";
import { useNavigate } from "@solidjs/router";
import PBENDPOINTS from "../http/endpoints";

function JoinGame() {
  userIsAuthenticated();
  const toast = useContext(ToasterContext);
  const navigate = useNavigate();
  const [tournamentFound, setTournamentFound] = createSignal(false);
  const [isLoading, setIsLoading] = createSignal(false);

  async function handleTournamentId(e: FormSubmitEvent) {
    setIsLoading(true);

    const value = (e.currentTarget.elements[0] as HTMLInputElement).value;

    await new Promise((r) => setTimeout(r, 250));
    await request(async (pb) => {
      try {
        const result = await pb.send(`${PBENDPOINTS.getTournament}/${value}`, {
          method: "GET",
        });

        if (!result.found) return;
        setTournamentFound(true);
        toast.default({
          title: "Tournament found!",
        });
      } catch (error) {
        toast.error({
          title: "Wrong id!",
        });
      }
    });

    setIsLoading(false);
  }

  async function handleJoinTournament(e: FormSubmitEvent) {
    setIsLoading(true);

    const tournamentid = (e.currentTarget.elements[0] as HTMLInputElement)
      .value;
    const password = (e.currentTarget.elements[1] as HTMLInputElement).value;
    await request(async (pb) => {
      try {
        await pb.send(`jointournament/${tournamentid}`, {
          method: "POST",
          body: {
            password,
          },
        });
        navigate(`/lobby/${tournamentid}`, { replace: true });
      } catch (error) {
        toast.error({
          title: "Wrong password!",
        });
      }
    });

    setIsLoading(false);
  }

  function handleSubmit(e: FormSubmitEvent) {
    e.preventDefault();
    if (!tournamentFound()) handleTournamentId(e);
    else handleJoinTournament(e);
  }

  return (
    <AnimatedPage>
      <BackButton />
      <section class="mx-2 my-4 max-w-[750px] overflow-hidden rounded-2xl bg-secondary/5 p-6 md:mx-auto md:mb-12 md:mt-[15vh] md:p-10 lg:p-12">
        <h1 class="mb-[max(8vh,_2rem)] border-b-2 border-primary-700 py-2 text-center text-[clamp(1.5rem,_4vw,_4.5rem)] font-bold leading-none md:-mt-10 md:py-4">
          Join a tournament
        </h1>
        <form onSubmit={handleSubmit} class="flex flex-col gap-12">
          <div class="flex items-end gap-8">
            <div class="flex-1">
              <TextFieldInput
                name="id"
                label="Tournament id"
                required
                readonly={tournamentFound()}
              />
            </div>
            <TournamentSubmit
              found={tournamentFound()}
              isLoading={isLoading()}
            />
          </div>
          <TournamentPasswordSection found={tournamentFound()} />
        </form>
      </section>
    </AnimatedPage>
  );
}

type TournamentSubmitProps = {
  isLoading: boolean;
  found: boolean;
};
function TournamentSubmit(props: TournamentSubmitProps) {
  return (
    <Show when={!props.found}>
      <Button.Root
        type="submit"
        class="rounded-md border border-primary-400/75 p-2 text-xl leading-none transition-colors duration-100"
        classList={{
          "hover:bg-primary-400/75 hover:text-black": !props.isLoading,
        }}
      >
        <Show when={!props.isLoading} fallback={<Spinner show size="text" />}>
          <svg
            fill="currentColor"
            stroke-width="0"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 16 16"
            height="1em"
            width="1em"
          >
            <path d="M15.854.146a.5.5 0 0 1 .11.54l-5.819 14.547a.75.75 0 0 1-1.329.124l-3.178-4.995L.643 7.184a.75.75 0 0 1 .124-1.33L15.314.037a.5.5 0 0 1 .54.11ZM6.636 10.07l2.761 4.338L14.13 2.576 6.636 10.07Zm6.787-8.201L1.591 6.602l4.339 2.76 7.494-7.493Z"></path>
          </svg>
        </Show>
      </Button.Root>
    </Show>
  );
}

type TournamentPasswordInputProps = {
  found: boolean;
};
function TournamentPasswordSection(props: TournamentPasswordInputProps) {
  let ref: HTMLDivElement;

  createEffect(() => {
    if (props.found)
      anime({
        targets: ref,
        opacity: [0, 1],
        translateY: [32, 0],
        duration: 750,
        easing: "easeOutQuad",
        begin() {
          ref.style.display = "block";
        },
        complete() {
          ref.querySelector("input")?.focus();
        },
      });
  });

  return (
    <div ref={ref!} class="hidden opacity-0">
      <TextFieldInput
        name="password"
        type="password"
        label="Tournament password"
        required={props.found}
      />
      <br />
      <SubmitButton loading={false} label="Join" />
    </div>
  );
}

export default JoinGame;
