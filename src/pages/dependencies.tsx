import { For, Show } from "solid-js";
import AnimatedPage from "../components/animatedpage";
import BackButton from "../components/backbutton";

function Dependencies() {
  const dependencies = [
    {
      display: "SolidJS",
      link: "https://www.solidjs.com",
    },
    {
      display: "TailwindCSS",
      link: "https://www.tailwindcss.com",
    },
    {
      display: "TypeScript",
      link: "https://www.typescriptlang.org",
    },
    {
      display: "Pocketbase",
      link: "https://pocketbase.io",
    },
    {
      display: "Vite",
      link: "https://vitejs.dev/",
    },
    {
      display: "Kobalte",
      link: "https://kobalte.dev/",
    },
    {
      display: "Prettier",
      link: "https://prettier.io/",
    },
    {
      display: "PostCSS",
      link: "https://postcss.org/",
    },
    {
      break: true,
    },
    {
      display: "Versus Icon",
      link: "https://www.flaticon.com/free-icon/vs_12129584?term=versus&page=1&position=15&origin=tag&related_id=12129584",
    },
    {
      display: "Crown Icon",
      link: "https://www.flaticon.com/free-icon/crown_5076417?term=crown&related_id=5076417",
    },
    {
      display: "DiceBear Avatars",
      link: "https://www.dicebear.com/licenses/",
    },
  ];

  return (
    <AnimatedPage>
      <BackButton />
      <div class="mx-auto mt-4 max-w-3xl">
        <h1 class="mb-6 text-center text-2xl font-bold leading-[0.85] text-colortext md:mb-10 md:text-4xl md:leading-none">
          Dependencies
        </h1>
        <ul class="grid grid-cols-2 gap-4 md:grid-cols-3">
          <For each={dependencies}>
            {(item) => (
              <Show
                when={item.link && item.display}
                fallback={
                  <hr class="col-span-2 my-2 border-primary-200 opacity-50 md:col-span-3" />
                }
              >
                <li class="text-center">
                  <a
                    class="group inline-flex items-center rounded-md border border-primary-200/0 p-4 text-lg opacity-90 grayscale-[85%] transition-all duration-300 hover:border-primary-200 hover:opacity-100 hover:grayscale-0 md:text-xl"
                    href={item.link}
                    rel="noreferrer"
                    target="_blank"
                  >
                    <img
                      class="mr-2 inline-block h-[0.75em] w-[0.75em]"
                      src={`https://www.google.com/s2/favicons?domain=${item.link}`}
                    />
                    <span class="inline-block">{item.display}</span>
                  </a>
                </li>
              </Show>
            )}
          </For>
        </ul>
      </div>
    </AnimatedPage>
  );
}

export default Dependencies;
