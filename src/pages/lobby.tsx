// @ts-ignore
import QRCode from "qrcode";
import { A, useLocation, useNavigate } from "@solidjs/router";
import AnimatedPage from "../components/animatedpage";
import {
  For,
  Match,
  Show,
  Switch,
  createEffect,
  createResource,
  createSignal,
  onCleanup,
  onMount,
} from "solid-js";
import request, { newPocketbase } from "../http/request";
import {
  getTournamentInfo,
  getLobbyPlayers,
  getSportsPairs,
} from "../http/lobbypage";
import PageLockSpinner from "../components/pagelockspinner";
import PBENDPOINTS from "../http/endpoints";
import { Record } from "pocketbase";
import Spinner from "../components/spinner";
import { Button, Collapsible, Popover } from "@kobalte/core";
import userIsAuthenticated from "../http/authenticatedroute";
import ToConfirm from "../components/confirmdialog";
import anime from "animejs";
import { SportInstance } from "../models/Sport";
import { isDarkTextBetterContrast } from "../utils/luminance";
import TAILWINDCUSTOM from "../static/constants/tailwindcustom";
import { SportCardInfos } from "./subpages/createtournament/sportssetup";
import { RoundScoringDBInstance } from "../models/RoundScoring";
import { RoundScoringInfos } from "./subpages/createtournament/roundscoringssetup";
import ROUTES from "../static/constants/routes";

export default function Lobby() {
  userIsAuthenticated();
  const pocketbase = newPocketbase();
  const user = pocketbase.authStore.model;
  const navigate = useNavigate();
  const locationArray = useLocation()
    .pathname.split("/")
    .filter((str) => str !== "");
  const tournamentid = locationArray[locationArray.length - 1];
  const [isJoined, setIsJoined] = createSignal(false);
  const [tournamentData] = createResource(
    isJoined,
    getTournamentInfo(tournamentid),
  );
  const [playersData, { refetch }] = createResource(
    isJoined,
    getLobbyPlayers(tournamentid),
  );
  const [sportPairs] = createResource(isJoined, getSportsPairs());
  const [adminLoading, setAdminLoading] = createSignal<
    "no" | "pending" | "finished"
  >("no");
  const [password, setPassword] = createSignal("");

  function refetchLobby() {
    if (pocketbase.authStore.model?.id === tournamentData().owner) {
      pocketbase
        .send("tournamentpassword", { method: "GET" })
        .then((result) => setPassword(result.password))
        .catch((reason) => console.log(reason));
    }
    refetch();
  }

  createEffect(() => {
    if (!tournamentData()) return;
    if (pocketbase.authStore.model?.id !== tournamentData().owner) return;
    setAdminLoading("pending");

    pocketbase
      .send("tournamentpassword", { method: "GET" })
      .then((result) => setPassword(result.password))
      .catch((reason) => console.log(reason));
    setAdminLoading("finished");
  });

  onMount(() => {
    request(async (pb) => {
      if (!pb.authStore.model?.id) navigate("/", { replace: true });
      const collection = pb.authStore.model?.collectionName;
      await pb.collection(collection).authRefresh();
      if (
        !pb.authStore.model?.tournament ||
        pb.authStore.model.tournament !== tournamentid
      ) {
        navigate("/", { replace: true });
        throw new Error("Invalid tournament");
      }
    })
      .then((_) => setIsJoined(true))
      .then((_) => {
        pocketbase.collection(PBENDPOINTS.users).subscribe("*", function (e) {
          const playerIds = (playersData() as Record[]).map(
            (player) => player.id,
          );
          if (
            e.action === "update" &&
            (e.record.tournament === tournamentid ||
              playerIds.includes(e.record.id))
          )
            refetchLobby();
        });
      })
      .then((_) => {
        pocketbase
          .collection(PBENDPOINTS.temporaryUsers)
          .subscribe("*", function (e) {
            if (
              (e.action === "create" || e.action === "delete") &&
              e.record.tournament === tournamentid
            )
              refetchLobby();
          });
      })
      .then((_) => {
        pocketbase
          .collection(PBENDPOINTS.tournaments)
          .subscribe(tournamentid, function (e) {
            if (e.action === "delete") {
              if (
                pocketbase.authStore.model?.collectionName === "temporaryusers"
              ) {
                window.localStorage.removeItem("pocketbase_auth");
              }
              navigate("/", { replace: true });
            } else {
              if (e.record.status === "ingame")
                navigate(ROUTES.game, { replace: true });
            }
          });
      });
  });

  onCleanup(() => {
    pocketbase.collection(PBENDPOINTS.tournaments).unsubscribe();
    pocketbase.collection(PBENDPOINTS.users).unsubscribe();
    pocketbase.collection(PBENDPOINTS.temporaryUsers).unsubscribe();
  });

  function startTournament() {
    if (
      playersData()?.length === 1 ||
      (tournamentData()?.type === "koth" &&
        playersData()?.length !== tournamentData().players)
    )
      return;
    ToConfirm({
      title:
        playersData().length !== tournamentData().players
          ? "The lobby is not full! Start tournament still?"
          : "Start tournament?",
      callback(confirm) {
        if (confirm) {
          const pb = newPocketbase();
          pb.send(PBENDPOINTS.startTournament, { method: "POST" });
        }
      },
    });
  }

  createEffect(() => {
    if (tournamentData()?.status === "ingame")
      navigate(ROUTES.game, { replace: true });
  });

  return (
    <AnimatedPage>
      <Show
        when={playersData() && tournamentData()}
        fallback={<PageLockSpinner show />}
      >
        <LeaveButton isTournamentOwner={adminLoading() === "finished"} />
        <div class="mx-auto mt-4 max-w-[1250px] p-2 sm:p-4 md:mt-6 lg:mt-8">
          <h1 class="mb-2 text-center text-3xl font-bold leading-none text-colortext md:text-4xl">
            {tournamentData().name}
          </h1>
          <h2 class="mb-6 text-center text-xl md:mb-10 md:text-2xl">
            <b class="bg-gradient-to-tr from-primary-500 to-accent-300 bg-clip-text text-colortext/0">
              Lobby
            </b>{" "}
          </h2>
          <div
            class="grid grid-cols-1 gap-2 sm:gap-4 lg:grid-rows-[auto_1fr] lg:gap-8"
            classList={{
              "lg:grid-cols-[auto_1fr]": adminLoading() !== "no",
            }}
          >
            <Switch>
              <Match when={adminLoading() === "pending"}>
                <div>
                  <Spinner show size="giant" />
                </div>
              </Match>
              <Match when={adminLoading() === "finished"}>
                <section class="row-span-2 rounded-md bg-secondary/10 p-4 pt-1">
                  <h3 class="text-center text-xl capitalize md:text-2xl">
                    Join information
                  </h3>
                  <h4 class="mb-4 border-b border-warning-200/75 text-center">
                    Only join one at a time
                  </h4>
                  <div class="flex flex-col justify-around gap-4 sm:flex-row lg:flex-col fine:gap-2">
                    <Collapsible.Root class="sm:w-[calc(30%_-_0.5rem)] lg:w-auto">
                      <Collapsible.Trigger class="group flex w-full items-center justify-center gap-1 md:text-lg [&[data-expanded]>svg]:-rotate-90">
                        <svg
                          fill="currentColor"
                          stroke-width="0"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 16 16"
                          height="1em"
                          width="1em"
                          class="ml-1 transition-[transform,color] duration-[250ms] group-hover:scale-125 group-hover:text-primary-200"
                        >
                          <path d="m3.86 8.753 5.482 4.796c.646.566 1.658.106 1.658-.753V3.204a1 1 0 0 0-1.659-.753l-5.48 4.796a1 1 0 0 0 0 1.506z"></path>
                        </svg>
                        QR Code
                      </Collapsible.Trigger>
                      <Collapsible.Content class="overflow-hidden ui-expanded:animate-collapsibleOpen ui-closed:animate-collapsibleClose">
                        <QRCodeElement
                          password={password()}
                          tournament={tournamentData().id}
                        />
                      </Collapsible.Content>
                    </Collapsible.Root>
                    <div class="text-center sm:w-[calc(30%_-_0.5rem)] lg:w-auto">
                      <Collapsible.Root>
                        <Collapsible.Trigger class="group flex w-full items-center justify-center gap-1 md:text-lg [&[data-expanded]>svg]:-rotate-90">
                          <svg
                            fill="currentColor"
                            stroke-width="0"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 16 16"
                            height="1em"
                            width="1em"
                            class="ml-1 transition-[transform,color] duration-[250ms] group-hover:scale-125 group-hover:text-primary-200"
                          >
                            <path d="m3.86 8.753 5.482 4.796c.646.566 1.658.106 1.658-.753V3.204a1 1 0 0 0-1.659-.753l-5.48 4.796a1 1 0 0 0 0 1.506z"></path>
                          </svg>
                          Link
                        </Collapsible.Trigger>
                        <Collapsible.Content class="overflow-hidden ui-expanded:animate-collapsibleOpen ui-closed:animate-collapsibleClose">
                          <div class="my-2 rounded-md bg-secondary/10 py-2">
                            <CopyLinkButton
                              password={password()}
                              tournament={tournamentData().id}
                            />
                          </div>
                        </Collapsible.Content>
                      </Collapsible.Root>
                    </div>
                    <div class="text-center sm:w-[calc(30%_-_0.5rem)] lg:w-auto">
                      <Collapsible.Root>
                        <Collapsible.Trigger class="group flex w-full items-center justify-center gap-1 md:text-lg [&[data-expanded]>svg]:-rotate-90">
                          <svg
                            fill="currentColor"
                            stroke-width="0"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 16 16"
                            height="1em"
                            width="1em"
                            class="ml-1 transition-[transform,color] duration-[250ms] group-hover:scale-125 group-hover:text-primary-200"
                          >
                            <path d="m3.86 8.753 5.482 4.796c.646.566 1.658.106 1.658-.753V3.204a1 1 0 0 0-1.659-.753l-5.48 4.796a1 1 0 0 0 0 1.506z"></path>
                          </svg>
                          Info
                        </Collapsible.Trigger>
                        <Collapsible.Content class="overflow-hidden ui-expanded:animate-collapsibleOpen ui-closed:animate-collapsibleClose">
                          <div class="my-2 rounded-md bg-secondary/10 py-2">
                            <h4 class="md:text-lg">Tournament ID</h4>
                            <p class="mb-4 text-sm md:text-base">
                              {tournamentData().id}
                            </p>
                            <h4 class="md:text-lg">Password</h4>
                            <p class="text-sm md:text-base">{password()}</p>
                          </div>
                        </Collapsible.Content>
                      </Collapsible.Root>
                    </div>
                  </div>
                </section>
              </Match>
            </Switch>
            <section class="flex-1 rounded-md bg-secondary/10 p-4 pt-1">
              <h3 class="mb-4 text-center text-xl md:mb-8 md:text-2xl">
                Players ({playersData().length} / {tournamentData().players})
              </h3>
              <ul class="grid grid-cols-1 gap-6 md:grid-cols-2 md:gap-8 lg:grid-cols-3">
                <For each={playersData() as unknown as Record[]}>
                  {(player) => (
                    <li
                      class="flex gap-4"
                      classList={{
                        "bg-gradient-to-r from-primary-400/25":
                          player.id === user?.id,
                      }}
                    >
                      <img
                        src={`https://api.dicebear.com/7.x/${
                          tournamentData().avatar
                        }/png?seed=${
                          Boolean(player.selectedavatar)
                            ? player.selectedavatar
                            : player.username
                        }`}
                        alt={`player ${player.username}'s avatar`}
                        class="max-w-[4rem] rounded-md shadow-secondary/40 outline outline-2 outline-offset-4 outline-secondary/50 [box-shadow:_0_0_8px_8px_var(--tw-shadow-color)]"
                      />
                      <div class="flex flex-col self-center">
                        <span class="text-lg">{player.username}</span>
                        <Show when={!player.registered}>
                          <span class="text-sm italic opacity-75">
                            Temporary user
                          </span>
                        </Show>
                        <Show when={player.id === tournamentData().owner}>
                          <span class="text-sm italic text-primary-100 opacity-75">
                            Owner
                          </span>
                        </Show>
                      </div>
                    </li>
                  )}
                </For>
                <For
                  each={[
                    ...Array(tournamentData().players - playersData().length),
                  ]}
                >
                  {(_) => (
                    <li class="flex min-h-[4rem] items-center rounded-sm border-2 border-dashed border-secondary/50" />
                  )}
                </For>
              </ul>
            </section>
            <section class="flex-1 rounded-md bg-secondary/10 p-4 pt-1">
              <h3 class="relative text-center text-xl md:text-2xl">
                {tournamentData().rounds} Rounds
                <span class="absolute bottom-0 left-[50%] block h-[1px] w-[15ch] translate-x-[-50%] bg-primary-400/50" />
              </h3>
              <h4 class="mb-4 text-center text-lg md:mb-8 md:text-xl">
                {tournamentData().type === "koth"
                  ? "King of the Hill"
                  : "Round Robin"}
              </h4>
              <RoundsSection
                sportPairs={sportPairs()}
                isRoundRobin={tournamentData().type === "roundrobin"}
              />
            </section>
            <Show when={adminLoading() === "finished"}>
              <section class="grid place-items-center lg:col-span-2 lg:bg-secondary/10 lg:py-4">
                <Button.Root
                  onClick={startTournament}
                  disabled={
                    playersData()?.length === 1 ||
                    (tournamentData()?.type === "koth" &&
                      playersData()?.length !== tournamentData()?.players)
                  }
                  class="w-full max-w-[50ch] rounded-md bg-primary-400/50 px-1 py-2 text-lg outline outline-2 outline-offset-4 outline-primary-400/0 transition-all duration-200 hover:outline-primary-400/75 focus-visible:outline-primary-400/75 active:bg-primary-400/75 active:outline-offset-0 disabled:!opacity-50"
                >
                  Start Tournament!
                </Button.Root>
              </section>
            </Show>
          </div>
        </div>
      </Show>
    </AnimatedPage>
  );
}

type LeaveButtonProps = {
  isTournamentOwner: boolean;
};
function LeaveButton(props: LeaveButtonProps) {
  const navigate = useNavigate();

  function handleNavigate(e: any) {
    e.preventDefault();

    ToConfirm({
      callback(confirm) {
        if (confirm) {
          const pb = newPocketbase();
          pb.send(PBENDPOINTS.leavetournament, { method: "DELETE" }).then(
            () => {
              if (pb.authStore.model?.collectionName === "temporaryusers")
                window.localStorage.removeItem("pocketbase_auth");
              navigate("/", { replace: true });
            },
          );
        }
      },
    });
  }

  return (
    <A
      href="/"
      class="group inline-flex items-center gap-1 rounded-br-lg border-b border-r pl-1 pr-1 pt-1 text-base text-warning-300/80 transition-colors hover:text-warning-300 md:pb-1 md:pl-2 md:pt-2"
      onClick={handleNavigate}
    >
      <svg
        fill="currentColor"
        stroke-width="0"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 16 16"
        style="overflow: visible;"
        height="1.25em"
        width="1.25em"
        class="transition-transform group-hover:-translate-x-1"
      >
        <path
          fill-rule="evenodd"
          d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"
        ></path>
      </svg>
      <span class="block">
        <Show when={props.isTournamentOwner} fallback="Leave">
          Delete tournament
        </Show>
      </span>
    </A>
  );
}

type QRCodeProps = {
  tournament: string;
  password: string;
};
function QRCodeElement(props: QRCodeProps) {
  let ref: HTMLCanvasElement;
  const [localTournamentPassword, setLocalTournamentPassword] = createSignal(
    props.password,
  );

  function setQRCode() {
    const base = import.meta.env.PROD
      ? import.meta.env.VITE_BASE_URL
      : "http://192.168.50.99:3000";
    QRCode.toCanvas(
      ref,
      `${base}/joinurl?tournament=${props.tournament}&password=${props.password}`,
      function (error: any) {
        if (error) return;
      },
    );
  }

  onMount(() => {
    if (!props.password) return;
    setQRCode();
  });

  createEffect(() => {
    if (props.password !== localTournamentPassword()) {
      anime({
        targets: ref,
        keyframes: [
          { scale: 0.8, easing: "spring(1, 80, 10, 0)", duration: 300 },
          { translateX: "150%", easing: "linear", delay: 150, duration: 300 },
        ],
        complete() {
          setLocalTournamentPassword(props.password);
          setQRCode();
          anime.set(ref, {
            translateX: "-150%",
          });

          anime({
            targets: ref,
            keyframes: [
              { translateX: "0", easing: "linear", duration: 300 },
              {
                scale: 1,
                easing: "spring(1, 80, 10, 0)",
                delay: 150,
                duration: 300,
              },
            ],
            delay: 25,
          });
        },
      });
    }
  });

  return <canvas ref={ref!} class="mx-auto rounded-md opacity-75"></canvas>;
}

function CopyLinkButton(props: QRCodeProps) {
  let ref: HTMLButtonElement;
  const [localTournamentPassword, setLocalTournamentPassword] = createSignal(
    props.password,
  );

  function createUrl() {
    const base = import.meta.env.PROD
      ? import.meta.env.VITE_BASE_URL
      : "http://192.168.50.99:3000";
    return `${base}/joinurl?tournament=${props.tournament}&password=${props.password}`;
  }

  createEffect(() => {
    if (props.password !== localTournamentPassword()) {
      anime({
        targets: ref,
        keyframes: [
          { scale: 0.8, easing: "spring(1, 80, 10, 0)", duration: 300 },
          { opacity: 0, easing: "linear", delay: 150, duration: 200 },
          { opacity: 1, easing: "linear", delay: 225, duration: 200 },
          { scale: 1, easing: "spring(1, 80, 10, 0)", duration: 300 },
        ],
        complete() {
          setLocalTournamentPassword(props.password);
          createUrl();
        },
      });
    }
  });

  return (
    <Button.Root
      ref={ref!}
      class="underline transition-colors duration-100 hover:text-primary-200 focus-visible:text-primary-200 md:text-lg"
      onClick={() => {
        navigator.clipboard.writeText(createUrl());
      }}
    >
      <Show
        when={Boolean(navigator.clipboard?.writeText)}
        fallback={<p class="select-all">{createUrl()}</p>}
      >
        Copy join link
      </Show>
    </Button.Root>
  );
}

type RoundSection = {
  sportPairs?: Array<[SportInstance, RoundScoringDBInstance | null]>;
  isRoundRobin: boolean;
};
function RoundsSection(props: RoundSection) {
  return (
    <div>
      <ul class="grid gap-16 overflow-x-scroll overscroll-x-contain pb-4 [grid-auto-columns:100%] [grid-auto-flow:column] md:overflow-x-auto md:[grid-auto-columns:65%] lg:[grid-auto-columns:45%]">
        <For each={props.sportPairs}>
          {(item, index) => (
            <li class="relative flex flex-col gap-1 rounded-md bg-secondary/10 p-1">
              <strong class="block text-center text-lg !leading-none md:text-xl">
                #{index() + 1}
              </strong>
              <hr class="relative left-[7.5%] mb-2 w-[85%] border-secondary/50" />
              <SportRow sport={item[0]} />
              <Show when={props.isRoundRobin}>
                <RoundScoringRow scoring={item[1]!} />
              </Show>
              <Show when={index() + 1 !== props.sportPairs?.length}>
                <svg
                  fill="currentColor"
                  stroke-width="0"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 16 16"
                  height="4rem"
                  width="4rem"
                  class="absolute -right-16 top-1/2 -translate-y-1/2 text-secondary/75"
                >
                  <path
                    fill-rule="evenodd"
                    d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"
                  ></path>
                </svg>
              </Show>
            </li>
          )}
        </For>
      </ul>
    </div>
  );
}

type SportRowProps = {
  sport: SportInstance;
};
export function SportRow(props: SportRowProps) {
  const pTextColor = props.sport.customColor
    ? isDarkTextBetterContrast(props.sport.customColor)
      ? "#111111"
      : "#eeeeee"
    : "#111111";
  const pColor = Boolean(props.sport.customColor)
    ? props.sport.customColor
    : "#E7CCD1";

  return (
    <div
      class="overflow-hidden rounded-t-md border border-custom"
      style={{
        [TAILWINDCUSTOM.BORDER]: pColor,
      }}
    >
      <div class="flex">
        <h3
          class="relative flex-1 break-words bg-custom py-5 pl-[1em] text-center font-bold !leading-none text-custom md:text-lg"
          style={{
            [TAILWINDCUSTOM.BG]: pColor,
            [TAILWINDCUSTOM.TEXT]: pTextColor,
          }}
        >
          <svg
            fill="currentColor"
            stroke-width="0"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            height="2em"
            width="2em"
            class="absolute left-1 top-3 rotate-90 scale-125 opacity-50"
          >
            <path d="M4.929 19.081c1.895 1.895 4.405 2.938 7.071 2.938s5.177-1.043 7.071-2.938c3.899-3.899 3.899-10.243 0-14.143C17.177 3.044 14.665 2 12 2S6.823 3.044 4.929 4.938c-3.899 3.899-3.899 10.244 0 14.143zm12.728-1.414a7.969 7.969 0 0 1-3.813 2.129c-.009-1.602.586-3.146 1.691-4.251 1.163-1.163 2.732-1.828 4.277-1.851a7.945 7.945 0 0 1-2.155 3.973zm2.325-5.965c-2.124-.021-4.284.853-5.861 2.429-1.532 1.532-2.327 3.68-2.263 5.881a7.946 7.946 0 0 1-5.516-2.345 7.97 7.97 0 0 1-2.332-5.512c.077.002.154.014.231.014 2.115 0 4.16-.804 5.637-2.28 1.58-1.58 2.457-3.739 2.43-5.873a7.948 7.948 0 0 1 5.349 2.337 7.96 7.96 0 0 1 2.325 5.349zM6.343 6.353a7.968 7.968 0 0 1 3.973-2.169c-.018 1.555-.685 3.124-1.851 4.291-1.104 1.103-2.642 1.696-4.238 1.691a7.929 7.929 0 0 1 2.116-3.813z"></path>
          </svg>
          {props.sport.name}
        </h3>
        <Popover.Root placement="top">
          <Popover.Trigger class="group rounded-tr-md px-2">
            <svg
              fill="currentColor"
              stroke-width="0"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 16 16"
              height="1.25em"
              width="1.25em"
              class="group-focus-visible:scale-125"
            >
              <path d="m9.708 6.075-3.024.379-.108.502.595.108c.387.093.464.232.38.619l-.975 4.577c-.255 1.183.14 1.74 1.067 1.74.72 0 1.554-.332 1.933-.789l.116-.549c-.263.232-.65.325-.905.325-.363 0-.494-.255-.402-.704l1.323-6.208Zm.091-2.755a1.32 1.32 0 1 1-2.64 0 1.32 1.32 0 0 1 2.64 0Z"></path>
            </svg>{" "}
          </Popover.Trigger>
          <Popover.Portal>
            <Popover.Content class="relative rounded-md border border-secondary !bg-background shadow-xl shadow-black/20 ui-expanded:animate-popoverenter ui-closed:animate-popoverexit">
              <Popover.Arrow />
              <div>
                <Popover.Title class="px-6 text-center text-lg">
                  {props.sport.name}
                </Popover.Title>
                <Popover.CloseButton class="absolute right-1 top-0 block text-xl leading-none">
                  {"\u26cc"}
                </Popover.CloseButton>
              </div>
              <Popover.Description>
                <SportCardInfos
                  sport={{
                    ...props.sport,
                    customColor: Boolean(props.sport.customColor)
                      ? props.sport.customColor
                      : "#E7CCD1",
                  }}
                />
              </Popover.Description>
            </Popover.Content>
          </Popover.Portal>
        </Popover.Root>
      </div>
    </div>
  );
}

type RoundScoringRowProps = {
  scoring: RoundScoringDBInstance;
};
export function RoundScoringRow(props: RoundScoringRowProps) {
  const pTextColor = props.scoring.customColor
    ? isDarkTextBetterContrast(props.scoring.customColor)
      ? "#111111"
      : "#eeeeee"
    : "#eeeeee";
  const pColor = Boolean(props.scoring.customColor)
    ? props.scoring.customColor
    : "#65132C";

  return (
    <div
      class="overflow-hidden rounded-b-md border border-custom"
      style={{
        [TAILWINDCUSTOM.BORDER]: pColor,
      }}
    >
      <div class="flex">
        <h3
          class="relative flex-1 break-words bg-custom py-5 pl-[1em] text-center text-lg font-bold leading-none text-custom"
          style={{
            [TAILWINDCUSTOM.BG]: pColor,
            [TAILWINDCUSTOM.TEXT]: pTextColor,
          }}
        >
          <svg
            fill="none"
            stroke-width="2"
            xmlns="http://www.w3.org/2000/svg"
            width="2em"
            height="2em"
            viewBox="0 0 24 24"
            stroke="currentColor"
            stroke-linecap="round"
            stroke-linejoin="round"
            class="absolute left-1 top-3 -rotate-45 scale-125 opacity-50"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M3 5m0 2a2 2 0 0 1 2 -2h14a2 2 0 0 1 2 2v10a2 2 0 0 1 -2 2h-14a2 2 0 0 1 -2 -2z"></path>
            <path d="M12 5v2"></path>
            <path d="M12 10v1"></path>
            <path d="M12 14v1"></path>
            <path d="M12 18v1"></path>
            <path d="M7 3v2"></path>
            <path d="M17 3v2"></path>
            <path d="M15 10.5v3a1.5 1.5 0 0 0 3 0v-3a1.5 1.5 0 0 0 -3 0z"></path>
            <path d="M6 9h1.5a1.5 1.5 0 0 1 0 3h-.5h.5a1.5 1.5 0 0 1 0 3h-1.5"></path>
          </svg>
          {props.scoring.name}
        </h3>
        <Popover.Root>
          <Popover.Trigger class="group rounded-tr-md px-2">
            <svg
              fill="currentColor"
              stroke-width="0"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 16 16"
              height="1.25em"
              width="1.25em"
              class="group-focus-visible:scale-125"
            >
              <path d="m9.708 6.075-3.024.379-.108.502.595.108c.387.093.464.232.38.619l-.975 4.577c-.255 1.183.14 1.74 1.067 1.74.72 0 1.554-.332 1.933-.789l.116-.549c-.263.232-.65.325-.905.325-.363 0-.494-.255-.402-.704l1.323-6.208Zm.091-2.755a1.32 1.32 0 1 1-2.64 0 1.32 1.32 0 0 1 2.64 0Z"></path>
            </svg>{" "}
          </Popover.Trigger>
          <Popover.Portal>
            <Popover.Content class="relative rounded-md border border-secondary !bg-background shadow-xl shadow-black/20 ui-expanded:animate-popoverenter ui-closed:animate-popoverexit">
              <Popover.Arrow />
              <div>
                <Popover.Title class="px-6 text-center text-lg">
                  {props.scoring.name}
                </Popover.Title>
                <Popover.CloseButton class="absolute right-1 top-0 block text-xl leading-none">
                  {"\u26cc"}
                </Popover.CloseButton>
              </div>
              <Popover.Description>
                <RoundScoringInfos
                  scoring={{
                    ...props.scoring,
                    customColor: Boolean(props.scoring.customColor)
                      ? props.scoring.customColor
                      : "#65132C",
                    hash: "",
                  }}
                />
              </Popover.Description>
            </Popover.Content>
          </Popover.Portal>
        </Popover.Root>
      </div>
    </div>
  );
}
