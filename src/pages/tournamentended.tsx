import { useLocation, useNavigate } from "@solidjs/router";
import userIsAuthenticated from "../http/authenticatedroute";
import AnimatedPage from "../components/animatedpage";
import { newPocketbase } from "../http/request";
import { Record } from "pocketbase";
import { For, Match, Show, Switch, onMount } from "solid-js";
import { UsernamesType } from "./game";
import BackButton from "../components/backbutton";
import refreshIsAuthorized from "../hooks/isauthorized";
import { TournamentTypes } from "../static/constants/tournamenttypes";

type TournamentState = {
  leaderboard?: Record[];
  avatarType?: string;
  usernames?: UsernamesType;
  tournamentType: TournamentTypes;
};
export default function TournamentEnded() {
  userIsAuthenticated();
  const navigate = useNavigate();
  const pb = newPocketbase();
  const data = useLocation().state as TournamentState;
  const playerId = pb.authStore.model?.id;
  if (!data || !data?.leaderboard) navigate("/", { replace: true });

  const yourPosition = data.leaderboard?.findIndex(
    (rec) => rec.playerid === playerId,
  );

  if (typeof yourPosition !== "number") navigate("/", { replace: true });

  function positionAsText() {
    const pos = yourPosition! + 1;
    switch (pos) {
      case 1:
        return pos + "st";
      case 2:
        return pos + "nd";
      case 3:
        return pos + "rd";
      default:
        return pos + "th";
    }
  }

  onMount(() => {
    if (pb.authStore.model?.collectionName === "users") return;

    pb.authStore.clear();
    new Promise((r) => setTimeout(r, 10)).then(() => refreshIsAuthorized());
  });

  return (
    <AnimatedPage>
      <BackButton label="Leave" />
      <section class="pt-4 text-center md:pt-[10vh]">
        <h1 class="opacity-60 md:text-lg">Tournament has ended!</h1>
        <h2 class="flex flex-col gap-2 overflow-hidden md:gap-4">
          <Switch>
            <Match when={data.tournamentType === "roundrobin"}>
              <span class="block text-2xl md:text-3xl">Your position is</span>
              <span
                class="block animate-breath text-5xl"
                classList={{
                  "bg-gradient-to-tr from-accent-400 to-primary-100 bg-clip-text text-accent-400/0":
                    yourPosition === 0,
                  "text-error-400": yourPosition !== 0,
                }}
              >
                {positionAsText()}
              </span>
              <Show when={yourPosition === 0}>
                <span class="-mt-2 bg-gradient-to-tr from-accent-400 to-primary-100 bg-clip-text text-xl text-accent-400/0 md:-mt-4 md:text-3xl">
                  You won!
                </span>
              </Show>
            </Match>
            <Match when={data.tournamentType === "koth"}>
              <span
                class="block text-5xl"
                classList={{
                  "bg-gradient-to-tr from-accent-400 to-primary-100 bg-clip-text text-accent-400/0":
                    yourPosition === 0,
                  "text-error-400": yourPosition !== 0,
                }}
              >
                <Show
                  when={yourPosition === 0}
                  fallback={<span>Better luck next time!</span>}
                >
                  <span>You won!</span>
                </Show>
              </span>
            </Match>
          </Switch>
        </h2>
      </section>
      <section class="mx-auto mt-6 max-w-3xl p-2 md:mt-10">
        <ul class="flex flex-col gap-4 rounded-md bg-secondary/5 p-4">
          <For each={data.leaderboard}>
            {(player, index) => (
              <li
                class="rounded-md md:flex"
                classList={{
                  "bg-accent-400/10": index() === 0,
                  "bg-secondary/5 mx-4 md:mx-6 lg:mx-8": index() !== 0,
                  "outline-2 outline-primary-200/50 outline":
                    player.playerid === playerId && index() !== 0,
                  "outline-2 outline-accent-200/75 outline":
                    player.playerid === playerId && index() === 0,
                }}
              >
                <span
                  class="relative grid min-w-[5ch] place-items-center rounded-t-md px-2 py-1 text-center text-lg md:!rounded-l-md md:rounded-t-none md:text-xl"
                  classList={{
                    "bg-accent-400/10": index() === 0,
                    "bg-secondary/10": index() !== 0,
                  }}
                >
                  <span class="block">
                    <Show
                      when={data.tournamentType === "roundrobin"}
                      fallback={
                        <Show when={index() === 0} fallback={"Out"}>
                          Winner
                        </Show>
                      }
                    >
                      #{index() + 1}
                    </Show>
                  </span>
                  <Show when={player.playerid === playerId}>
                    <span class="absolute right-4 top-1 opacity-60 md:static">
                      (You)
                    </span>
                  </Show>
                </span>
                <div class="flex flex-grow justify-between gap-4 p-2 md:ml-4 md:p-4">
                  <div class="flex">
                    <img
                      src={`https://api.dicebear.com/7.x/${
                        data.avatarType
                      }/png?seed=${data.usernames?.[player.playerid].avatar}`}
                      alt={`player ${player.username}'s avatar`}
                      class="max-h-[2rem] max-w-[2rem] rounded-md outline outline-2 outline-offset-2 outline-secondary/50 md:max-h-[2.5rem] md:max-w-[2.5rem]"
                    />
                    <span class="grid place-items-center break-all pl-2 md:pl-4">
                      {player.username}
                    </span>
                  </div>
                  <div class="grid place-items-center">
                    <Show when={data.tournamentType === "roundrobin"}>
                      Score: {player.score}
                    </Show>
                  </div>
                </div>
              </li>
            )}
          </For>
        </ul>
      </section>
    </AnimatedPage>
  );
}
