import {
  Accessor,
  For,
  Match,
  ParentProps,
  Show,
  Switch,
  createContext,
  createSignal,
  onMount,
} from "solid-js";
import AnimatedPage from "../components/animatedpage";
import BackButton from "../components/backbutton";
import { Tabs, toaster } from "@kobalte/core";
import useIsPhone from "../hooks/useIsPhone";
import anime from "animejs";
import { createMutable } from "solid-js/store";
import TournamentSetupSubpage from "./subpages/createtournament/tournamentsetup";
import SportsSetupSubpage from "./subpages/createtournament/sportssetup";
import { UniqueSport } from "../models/Sport";
import RoundScoringsSetup from "./subpages/createtournament/roundscoringssetup";
import { RoundScoring } from "../models/RoundScoring";
import RoundScoringsToSportsSubpage from "./subpages/createtournament/roundscoringstosports";
import GeneralInfos from "./subpages/createtournament/generalinfos";
import request from "../http/request";
import {
  TOURNAMENTTYPES,
  TournamentTypes,
} from "../static/constants/tournamenttypes";
import PageLockSpinner from "../components/pagelockspinner";
import { useNavigate } from "@solidjs/router";
import userIsAuthenticated from "../http/authenticatedroute";

function CreateTournament() {
  userIsAuthenticated();
  const [phoneOffset, setPhoneOffset] = createSignal(0);
  function handleScroll(e: any) {
    setPhoneOffset(e.target.scrollTop);
  }

  return (
    <AnimatedPage onScroll={handleScroll}>
      <BackButton />
      <section class="mx-auto mt-4 max-w-[1250px] p-2 md:mt-6 lg:mt-8">
        <h1 class="mb-6 text-center text-2xl font-bold leading-[0.85] text-colortext md:mb-10 md:text-4xl md:leading-none">
          Create your{" "}
          <b class="bg-gradient-to-tr from-primary-500 to-accent-300 bg-clip-text text-colortext/0">
            awesome
          </b>{" "}
          tournament!
        </h1>
        <Stepper offset={phoneOffset} />
      </section>
    </AnimatedPage>
  );
}

export const StepperFormContext = createContext({
  reaction: (f: FormData, s: string) => {},
  values: new Map<string, Map<string, string>>(),
  customSports: createMutable([] as UniqueSport[]),
  selectedSavedSports: createMutable([] as UniqueSport[]),
  customRoundScorings: createMutable([] as RoundScoring[]),
  selectedSavedRoundScorings: createMutable([] as RoundScoring[]),
  assignedRounds: createMutable([] as Array<[UniqueSport, RoundScoring]>),
  roundAssigningReaction: (data: Array<[UniqueSport, RoundScoring]>) => {},
  setTournamentType: (s: TournamentTypes) => {},
  tournamentType: (() =>
    TOURNAMENTTYPES.roundrobin) as Accessor<TournamentTypes>,
});
export const steps = [
  "Tournament Setup",
  "Sports",
  "Round Scorings",
  "Assigning",
  "General",
];
type StepperProps = {
  offset: Accessor<number>;
};
function Stepper(props: StepperProps) {
  const navigate = useNavigate();
  let stepsIndex = createMutable({ value: 0 });
  const [isLoading, setIsLoading] = createSignal(false);
  const [selectedTab, setSelectedTab] = createSignal(steps[0]);
  const [tournamentType, setTournamentType] = createSignal<TournamentTypes>(
    TOURNAMENTTYPES.roundrobin,
  );
  const [acceptedSteps, setAcceptedSteps] = createSignal(
    new Array(steps.length).fill(true, 0, 1).fill(false, 1),
    {
      equals: false,
    },
  );
  const isPhone = useIsPhone();
  const formValues = new Map<string, Map<string, string>>(
    steps.map((str) => [str, new Map()]),
  );
  const customSports = createMutable([]);
  const selectedSavedSports = createMutable([]);
  const customRoundScorings = createMutable([]);
  const selectedSavedRoundScorings = createMutable([]);
  const assignedRounds = createMutable(
    [] as Array<[UniqueSport, RoundScoring]>,
  );

  function roundAssigningReaction(data: Array<[UniqueSport, RoundScoring]>) {
    if (stepsIndex.value < steps.length - 1) {
      stepsIndex.value++;
      setAcceptedSteps((prev) => {
        let tmp = prev;
        tmp[stepsIndex.value] = true;
        return tmp;
      });
    }

    for (let i = 0; i < data.length; i++) {
      assignedRounds[i] = data[i];
    }

    toaster.clear();
    setSelectedTab(steps[stepsIndex.value]);
  }

  function submitTournamentForm() {
    request(async (pb) => {
      if (!pb.authStore.model?.id || isLoading()) return;
      setIsLoading(true);

      try {
        const tournamentData = {
          name: formValues.get(steps[0])!.get("tournamentName"),
          type: formValues.get(steps[0])!.get("tournamentType"),
          players: parseInt(formValues.get(steps[0])!.get("maxPlayers")!),
          rounds: parseInt(formValues.get(steps[0])!.get("roundCount")!),
          avatar: formValues.get(steps[4])!.get("avatar"),
          notification: Boolean(formValues.get(steps[4])!.get("notification")),
          owner: pb.authStore.model.id,
          status: "lobby",
        };
        const tournamentRecord = await pb
          .collection("tournaments")
          .create(tournamentData);

        await pb
          .collection("users")
          .update(pb.authStore.model.id, { tournament: tournamentRecord.id });

        // TODO: error toast
        for (let i = 0; i < assignedRounds.length; i++) {
          const sport = assignedRounds[i][0];
          const roundScoring = assignedRounds[i][1];

          const sportData = {
            name: sport.name,
            rounds: sport.rounds,
            scorestart: sport.scoreStart,
            scoregoal: sport.scoreGoal,
            roundsplayed: sport.fixedRounds ? "fixed" : "best of",
            scoringrule: sport.scoringRule,
            scoresperturn: sport?.scoresPerTurn,
            commoninputs: sport.commonInputs
              ? sport.commonInputs.join(",")
              : undefined,
            customcolor: sport.customColor,
            defaultsport: sport.defaultSport,
            roundnumber: i,
            tournament: tournamentRecord.id,
          };
          await pb.collection("sports").create(sportData);

          const winneramount = () => {
            // @ts-ignore
            if (typeof roundScoring.winner.amount === "number")
              // @ts-ignore
              return roundScoring.winner.amount;
            // @ts-ignore
            if (typeof roundScoring.winner.multiplier === "number")
              // @ts-ignore
              return roundScoring.winner.multiplier;
            return 1;
          };

          const loseramount = () => {
            // @ts-ignore
            if (typeof roundScoring.loser.amount === "number")
              // @ts-ignore
              return roundScoring.loser.amount;
            // @ts-ignore
            if (typeof roundScoring.loser.multiplier === "number")
              // @ts-ignore
              return roundScoring.loser.multiplier;
            return 1;
          };

          if (tournamentType() === "roundrobin") {
            const roundScoringData = {
              name: roundScoring.name,
              winnertype: roundScoring.winner.type,
              winneramount: winneramount(),
              losertype: roundScoring.loser.type,
              loseramount: loseramount(),
              customcolor: roundScoring.customColor,
              roundnumber: i,
              tournament: tournamentRecord.id,
            };

            await pb.collection("roundscorings").create(roundScoringData);
          }
        }

        navigate("/lobby/" + tournamentRecord.id, { replace: true });
      } catch (error) {
      } finally {
        setIsLoading(false);
      }
    });
  }

  function submitReaction(data: FormData, subform: string) {
    if (stepsIndex.value < steps.length) {
      const map = new Map<string, string>();
      for (const [key, value] of data) {
        if (typeof value === "string") map.set(key, value);
      }
      formValues.set(subform, map);

      if (stepsIndex.value === steps.length - 1) {
        submitTournamentForm();
        return;
      }

      stepsIndex.value++;
      setAcceptedSteps((prev) => {
        let tmp = prev;
        tmp[stepsIndex.value] = true;
        return tmp;
      });
      if (stepsIndex.value === 2 && tournamentType() === "koth")
        stepsIndex.value++;

      toaster.clear();
      setSelectedTab(steps[stepsIndex.value]);
    }
  }

  function handleTrigger(
    event: MouseEvent & { currentTarget: HTMLButtonElement },
  ) {
    const tab = event.currentTarget.dataset["key"];
    const index = steps.findIndex((str) => tab === str);
    if (!tab || index === -1) return;

    stepsIndex.value = index;
    setSelectedTab(tab);
  }

  function handlePreviousStep() {
    if (stepsIndex.value > 0) {
      stepsIndex.value--;
      if (stepsIndex.value === 2 && tournamentType() === "koth")
        stepsIndex.value--;
      setSelectedTab(steps[stepsIndex.value]);
    }
  }

  return (
    <>
      <PageLockSpinner
        show={isLoading()}
        title="Tournament creation in progress..."
      />
      <Tabs.Root value={selectedTab()} class="flex flex-col-reverse md:block">
        <Show when={!isPhone()}>
          <Tabs.List class="relative isolate flex items-center border-b border-b-secondary/30 text-lg md:text-xl">
            <For each={acceptedSteps()}>
              {(accepted, index) => (
                <>
                  <Tabs.Trigger
                    value={steps[index()]}
                    class="px-3 py-2 hover:text-primary-100 disabled:opacity-80"
                    classList={{
                      "disabled:opacity-25":
                        index() === 2 && tournamentType() === "koth",
                    }}
                    disabled={
                      !accepted ||
                      (index() === 2 && tournamentType() === "koth")
                    }
                    onClick={handleTrigger}
                  >
                    {steps[index()]}
                  </Tabs.Trigger>
                  <Show when={index() != steps.length - 1}>
                    <Filler />
                  </Show>
                </>
              )}
            </For>
            <Tabs.Indicator class="absolute -top-4 z-10 h-[calc(100%_+_1rem)] border-b-2 border-b-primary-400/75 bg-gradient-to-t from-primary-400/25 transition-[width,_transform] duration-300 ease-[cubic-bezier(0.5,_0,_0.75,_0)]" />
          </Tabs.List>
        </Show>
        <div class="flex flex-col-reverse rounded-b-md border-secondary/0 p-2 text-sm md:flex-col md:border md:border-t-0 md:text-base">
          <div
            class="fixed bottom-0 left-0 z-[99] flex w-full justify-between rounded-none border-t-2 border-secondary/30 bg-[#452D4E] md:static md:mx-0 md:mb-0 md:border-t-0 md:bg-primary-200/0"
            style={{ bottom: `-${props.offset()}px` }}
          >
            <button
              class="group flex items-center gap-1 border-r border-r-secondary/30 p-2 hover:text-primary-100 disabled:opacity-50 md:border-r-0"
              disabled={stepsIndex.value === 0}
              onClick={handlePreviousStep}
            >
              <svg
                fill="currentColor"
                stroke-width="0"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 16 16"
                height="1em"
                width="1em"
                class="block transition-transform group-hover:-translate-x-1 group-disabled:!translate-x-0"
              >
                <path
                  fill-rule="evenodd"
                  d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"
                ></path>
              </svg>
              Back
            </button>
            <Show when={isPhone()}>
              <div class="flex flex-1 items-center pl-2 font-bold">
                {steps[stepsIndex.value]}
              </div>
              <div class="flex items-center pr-2">
                {stepsIndex.value + 1}/{steps.length}
              </div>
            </Show>
            <button
              class="group flex items-center gap-1 border-l border-l-secondary/30 p-2 hover:text-primary-100 md:border-l-0"
              classList={{
                "md:rounded-md fine:hover:scale-110 transition-transform font-bold bg-primary-400/90":
                  stepsIndex.value >= steps.length - 1,
              }}
              type="submit"
              form={steps[stepsIndex.value]}
            >
              <Switch>
                <Match when={stepsIndex.value >= steps.length - 1}>
                  Finish
                </Match>
                <Match when={stepsIndex.value < steps.length - 1}>Next</Match>
              </Switch>
              <svg
                fill="currentColor"
                stroke-width="0"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 16 16"
                height="1em"
                width="1em"
                class="block transition-transform group-hover:translate-x-1"
              >
                <path
                  fill-rule="evenodd"
                  d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"
                ></path>
              </svg>
            </button>
          </div>
          <StepperFormContext.Provider
            value={{
              reaction: submitReaction,
              values: formValues,
              customSports,
              selectedSavedSports,
              selectedSavedRoundScorings,
              customRoundScorings,
              assignedRounds,
              roundAssigningReaction,
              setTournamentType,
              tournamentType,
            }}
          >
            <Tabs.Content value={steps[0]}>
              <TournamentSetupSubpage formId={steps[0]} />
            </Tabs.Content>
            <Tabs.Content value={steps[1]}>
              <SportsSetupSubpage formId={steps[1]} />
            </Tabs.Content>
            <Tabs.Content value={steps[2]}>
              <RoundScoringsSetup formId={steps[2]} />
            </Tabs.Content>
            <Tabs.Content value={steps[3]}>
              <RoundScoringsToSportsSubpage formId={steps[3]} />
            </Tabs.Content>
            <Tabs.Content value={steps[4]}>
              <GeneralInfos formId={steps[4]} />
            </Tabs.Content>
          </StepperFormContext.Provider>
        </div>
      </Tabs.Root>
    </>
  );
}

function Filler() {
  return <div class="h-[1px] flex-grow bg-secondary/30" />;
}

type AnimatedFormProps = {
  handleSubmit: (e: SubmitEvent & { currentTarget: HTMLFormElement }) => void;
  id: string;
  fullSize?: boolean;
};
export function AnimatedForm(props: ParentProps<AnimatedFormProps>) {
  let ref: HTMLDivElement;
  onMount(() => {
    anime({
      targets: ref,
      opacity: [0.4, 1],
      easing: "easeOutQuart",
      duration: 1250,
    });
  });

  return (
    <div class="mb-6 md:mt-10" ref={ref!}>
      <form
        id={props.id}
        onSubmit={props.handleSubmit}
        class="mx-auto flex max-w-[815px] flex-col gap-6 py-4 md:text-lg lg:gap-10"
        classList={{ "max-w-none": Boolean(props.fullSize) }}
      >
        {props.children}
      </form>
    </div>
  );
}

export function validateForm(form: HTMLFormElement) {
  for (const input of form.elements) {
    if (input.getAttribute("data-invalid") !== null) return false;
  }
  return true;
}

export default CreateTournament;
