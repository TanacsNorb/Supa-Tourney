import { Record } from "pocketbase";
import { newPocketbase } from "../../../http/request";
import {
  Accessor,
  For,
  Match,
  Show,
  Switch,
  createEffect,
  createSignal,
  onMount,
  useContext,
} from "solid-js";
import { TournamentContext } from "../../game";
import anime from "animejs";
import { SportCardInfos } from "../createtournament/sportssetup";
import {
  SportRecord,
  SportRecordToModel,
  TDefaultSport,
  TScoringRule,
} from "../../../models/Sport";
import { RoundScoringInfos } from "../createtournament/roundscoringssetup";
import {
  RoundScoring,
  RoundScoringDBRecord,
  RoundScoringRecordToModel,
} from "../../../models/RoundScoring";
import {
  Message,
  OverlayMessage,
} from "../../../components/gamepage/overlaymessage";
import PBENDPOINTS from "../../../http/endpoints";
import { Button, Dialog } from "@kobalte/core";
import {
  DefaultBeerpongInput,
  DefaultDartsInput,
  DefaultSnookerInput,
  DefaultTableFootballInput,
  UndeterminedSportProps,
} from "../../../components/gamepage/custominputs";
import STORAGE from "../../../static/constants/storage";
import ToConfirm from "../../../components/confirmdialog";
import Spinner from "../../../components/spinner";
import TextFieldInput from "../../../components/textfield";
import { FormSubmitEvent } from "../../../models/types";

type MatchPageProps = {
  opponents: string[];
  currentRound: number;
  roundScorings: Record[];
  sports: Record[];
  matches: Record[];
  realtimeMatch: Accessor<Record>;
  yourScore?: Record;
  tournamentType: string;
};

type PlayerHistory = {
  id: string;
  rounds: number;
  history: string | null;
};

export default function MatchPage(props: MatchPageProps) {
  let ref: HTMLDivElement;
  const pb = newPocketbase();
  const usernames = useContext(TournamentContext).usernames;
  const [currentMatch, setCurrentMatch] = createSignal<Record | undefined>(
    undefined,
  );
  const [currentSport, setCurrentSport] = createSignal<Record | undefined>(
    undefined,
  );
  const [currentScoring, setCurrentScoring] = createSignal<Record | undefined>(
    undefined,
  );
  const [messageOpen, setMessageOpen] = createSignal(false);
  let message = { tint: "opponent", message: "Opponents turn!" };

  const hasOpponent = () => {
    return Boolean(
      currentMatch() &&
        currentMatch()?.playeroneid !== "X" &&
        currentMatch()?.playertwoid !== "X",
    );
  };

  const hasMatchEnded = () => {
    if (!hasOpponent()) return false;
    const match = props.realtimeMatch();
    return Boolean(match.winnerid);
  };

  function getCurrentMatch() {
    const playerid = pb.authStore.model?.id;
    const match = props.matches.find(
      (match) =>
        match.tournamentround === props.currentRound &&
        (match.playeroneid === playerid || match.playertwoid === playerid),
    );

    if (match) setCurrentMatch(match);

    if (props.currentRound >= 0) {
      const length = props.sports.length;
      if (props.currentRound > length - 1) {
        setCurrentSport(props.sports[length - 1]);
        setCurrentScoring(props.roundScorings[length - 1]);
      } else {
        setCurrentSport(props.sports[props.currentRound]);
        setCurrentScoring(props.roundScorings[props.currentRound]);
      }
    }
  }

  onMount(() => {
    anime({
      targets: ref,
      translateX: ["100%", 0],
      easing: "linear",
      duration: 400,
    });
    getCurrentMatch();
  });

  createEffect(() => {
    if (props.matches) getCurrentMatch();
  });

  createEffect(() => {
    const matchfinishedInitUser = props.realtimeMatch()?.matchfinished;
    if (
      matchfinishedInitUser &&
      matchfinishedInitUser !== pb.authStore.model?.id
    ) {
      ToConfirm({
        callback(confirmed) {
          if (confirmed) {
            pb.send(`${PBENDPOINTS.undeterminedFinished}/yes`, {
              method: "POST",
            });
          } else {
            pb.send(`${PBENDPOINTS.undeterminedFinished}/no`, {
              method: "POST",
            });
          }
        },
        title: "Is the round actually finished?",
        cancelLabel: "No!",
      });
    }
  });

  createEffect(() => {
    let initMessageRound = parseInt(
      window.sessionStorage.getItem(STORAGE.initMessageRound) ?? "a",
    );
    if (isNaN(initMessageRound)) initMessageRound = -1;

    if (currentMatch() && hasOpponent() && currentSport()) {
      if (initMessageRound >= currentMatch()!.tournamentround) return;
      if (currentMatch()!.winnerid) return;

      if (
        currentMatch()!.currentround <= 0 &&
        currentSport()!.scoringrule !== "Simultaneous"
      ) {
        if (currentMatch()!.currentplayerid === pb.authStore.model?.id) {
          message = {
            tint: "you",
            message: "You start the match!",
          };
        } else {
          message = {
            tint: "opponent",
            message: "The opponent starts the match!",
          };
        }
      } else {
        if (currentMatch()!.currentplayerid === pb.authStore.model?.id) {
          message = {
            tint: "you",
            message: "Your turn!",
          };
        } else {
          message = {
            tint: "opponent",
            message: "Opponent's turn",
          };
        }
      }

      window.sessionStorage.setItem(
        STORAGE.initMessageRound,
        currentMatch()!.tournamentround,
      );
      setMessageOpen(true);
    }
  });

  /*
   * First item of the array is "you"
   * The second item is the opponent
   */
  function determineYou() {
    const arr: PlayerHistory[] = [];
    const userid = pb.authStore.model?.id;
    const playerone = {
      id: props.realtimeMatch().playeroneid,
      rounds: props.realtimeMatch().playeronerounds,
      history: props.realtimeMatch().playeronehistory,
    };

    const playertwo = {
      id: props.realtimeMatch().playertwoid,
      rounds: props.realtimeMatch().playertworounds,
      history: props.realtimeMatch().playertwohistory,
    };
    arr.push(playerone, playertwo);
    if (props.realtimeMatch().playertwoid === userid) arr.reverse();

    return arr;
  }

  return (
    <div
      ref={ref!}
      class="relative mt-8 grid grid-cols-1 gap-2 md:mt-16 md:grid-cols-2 md:gap-4 lg:gap-8"
    >
      <Switch>
        <Match when={props.currentRound === -1}>
          <p class="mx-auto max-w-md rounded-md bg-secondary/10 px-4 py-8 text-center text-xl font-bold md:col-span-2 md:text-2xl">
            The tournament has not yet started!
          </p>
        </Match>
        <Match when={hasOpponent()}>
          <section
            class="flex overflow-hidden rounded-md bg-secondary/10"
            classList={{
              "md:col-span-2": !currentScoring(),
            }}
          >
            <Show when={currentSport()}>
              <h2 class="-rotate-180 bg-primary-200/40 p-2 text-center text-lg leading-none [writing-mode:_tb-rl] md:py-4">
                Sport
              </h2>
              <div class="mx-auto w-full max-w-lg p-2">
                <strong class="block text-center text-lg">
                  {currentSport()!.name}
                </strong>
                <SportCardInfos
                  sport={SportRecordToModel({
                    ...currentSport(),
                    customcolor: Boolean(currentSport()!.customcolor)
                      ? currentSport()!.customcolor
                      : "#E7CCD1",
                  } as unknown as SportRecord)}
                />
              </div>
            </Show>
          </section>
          <Show when={currentScoring()}>
            <section class="flex overflow-hidden rounded-md bg-secondary/10 md:flex-row-reverse">
              <h2 class="-rotate-180 bg-primary-200/40 p-2 text-center text-lg leading-none [writing-mode:_tb-rl] md:py-4">
                Scoring
              </h2>
              <div class="w-full p-2">
                <strong class="block text-center text-lg">
                  {currentScoring()!.name}
                </strong>
                <RoundScoringInfos
                  scoring={
                    {
                      ...RoundScoringRecordToModel({
                        ...currentScoring()!,
                        customcolor: Boolean(currentScoring()!.customcolor)
                          ? currentScoring()!.customcolor
                          : "#65132C",
                      } as unknown as RoundScoringDBRecord),
                    } as RoundScoring
                  }
                />
              </div>
            </section>
          </Show>
          <section class="rounded-md bg-primary-200/25 md:col-span-2">
            <h2 class="p-4 text-center text-xl leading-none md:text-2xl">
              Round {props.realtimeMatch().currentround + 1}
              <Show
                when={
                  props.realtimeMatch().currentround + 1 >
                  currentSport()!.rounds
                }
              >
                <br />
                Overtime
              </Show>
            </h2>
          </section>
          <section class="flex overflow-hidden rounded-md bg-secondary/10 md:flex-row">
            <h2 class="-rotate-180 bg-primary-200/40 p-2 text-center text-lg leading-none [writing-mode:_tb-rl] md:py-4">
              Your history
            </h2>
            <ScoreHistory
              playerHistory={determineYou()[0]}
              isOpponent={false}
            />
          </section>
          <section class="flex overflow-hidden rounded-md bg-secondary/10 md:flex-row-reverse">
            <h2 class="-rotate-180 bg-primary-200/40 p-2 text-center text-lg leading-none [writing-mode:_tb-rl] md:py-4">
              Opponents history
            </h2>
            <ScoreHistory playerHistory={determineYou()[1]} isOpponent={true} />
          </section>
          <section class="flex flex-row overflow-hidden rounded-md bg-secondary/10 md:col-span-2 md:flex-col">
            <h3 class="-rotate-180 bg-primary-200/40 p-2 text-center text-lg leading-none [writing-mode:_tb-rl] md:rotate-0 md:[writing-mode:_horizontal-tb]">
              Input score
            </h3>
            <InputHandler
              defaultSport={currentSport()!.defaultsport}
              scoringRule={currentSport()!.scoringrule}
              scoresPerTurn={currentSport()!.scoresperturn}
              commonInputs={currentSport()!.commoninputs}
              matchFinished={props.realtimeMatch()!.matchfinished}
              currentPlayer={props.realtimeMatch().currentplayerid}
              undeterminedEnding={
                currentSport()!.scorestart === 0 && currentSport()!.scoregoal
              }
            />
          </section>
        </Match>
        <Match when={!hasOpponent()}>
          <p class="mx-auto max-w-md rounded-md bg-secondary/10 px-4 py-8 text-center text-xl font-bold md:col-span-2 md:text-2xl">
            <Show
              when={
                props.tournamentType === "koth" && props.yourScore?.score === -1
              }
              fallback={"You have no opponent in this round!"}
            >
              You are out! <br />
              Better luck next time!
            </Show>
          </p>
        </Match>
      </Switch>
      <OverlayMessage
        setOpen={setMessageOpen}
        open={messageOpen}
        message={() => message as Message}
      />
      <MatchFinishedOverlay
        finished={hasMatchEnded()}
        winner={
          props.realtimeMatch().winnerid
            ? props.realtimeMatch().winnerid === "__tie__"
              ? { name: "__tie__", avatar: null }
              : usernames[props.realtimeMatch().winnerid]
            : undefined
        }
        youWon={pb.authStore.model?.id === props.realtimeMatch().winnerid}
      />
    </div>
  );
}

export type DefaultSportInputProps = {
  sendScore: (scores: number[]) => Promise<void>;
  isYourTurn: Accessor<boolean>;
};
type InputHandlerTypes = {
  defaultSport: TDefaultSport | undefined;
  currentPlayer: string;
  scoringRule: TScoringRule;
  matchFinished: string | undefined;
  scoresPerTurn: number | undefined;
  commonInputs: string | undefined;
  undeterminedEnding: boolean;
};
function InputHandler(props: InputHandlerTypes) {
  const [isYourTurn, setIsYourTurn] = createSignal(false);
  const pb = newPocketbase();
  async function sendScore(scores: number[]) {
    await pb.send(PBENDPOINTS.scoring, {
      method: "POST",
      body: { score: scores.reduce((a, b) => a + b, 0) },
    });
  }

  createEffect(() => {
    const userid = pb.authStore.model?.id;
    if (props.matchFinished) setIsYourTurn(false);
    else if (
      userid === props.currentPlayer ||
      props.scoringRule === "Simultaneous"
    )
      setIsYourTurn(true);
    else setIsYourTurn(false);
  });

  return (
    <div class="flex flex-1 md:block">
      <Switch
        fallback={
          <GeneralInput
            sendScore={sendScore}
            isYourTurn={isYourTurn}
            isMatchFinishedInitiated={Boolean(props.matchFinished)}
            scoresPerTurn={props.scoresPerTurn}
            scoringRule={props.scoringRule}
            commonInputs={props.commonInputs}
            undeterminedEnding={props.undeterminedEnding}
          />
        }
      >
        <Match when={props.defaultSport === "Darts"}>
          <DefaultDartsInput sendScore={sendScore} isYourTurn={isYourTurn} />
        </Match>
        <Match when={props.defaultSport === "Snooker"}>
          <DefaultSnookerInput
            sendScore={sendScore}
            isYourTurn={isYourTurn}
            isMatchFinishedInitiated={Boolean(props.matchFinished)}
          />
        </Match>
        <Match when={props.defaultSport === "Beerpong"}>
          <DefaultBeerpongInput sendScore={sendScore} isYourTurn={isYourTurn} />
        </Match>
        <Match when={props.defaultSport === "Tablefootball"}>
          <DefaultTableFootballInput
            sendScore={sendScore}
            isYourTurn={isYourTurn}
          />
        </Match>
      </Switch>
    </div>
  );
}

type GeneralInputProps = {
  scoresPerTurn: number | undefined;
  scoringRule: TScoringRule;
  commonInputs: string | undefined;
  undeterminedEnding: boolean;
};
function GeneralInput(
  props: DefaultSportInputProps & UndeterminedSportProps & GeneralInputProps,
) {
  const [buffer, setBuffer] = createSignal<number[]>([], {
    equals: (prev, next) => prev.length != next.length,
  });

  function multipleScoringIsAllowed() {
    let scoresPerTurn = props.scoresPerTurn ?? 1;
    if (props.scoringRule === "Multiple" && buffer().length != scoresPerTurn)
      return false;
    return true;
  }

  function removeLastScore() {
    setBuffer((arr) => {
      if (arr.length > 0) arr.length = arr.length - 1;
      return arr;
    });
  }

  function handleScore(value: string) {
    const num = parseInt(value);
    if (isNaN(num)) return;

    if (props.scoringRule !== "Once" && props.scoringRule !== "Simultaneous") {
      let scoresPerTurn = props.scoresPerTurn ?? 1;
      if (props.scoringRule === "Multiple" && buffer().length >= scoresPerTurn)
        return;
      setBuffer((prev) => {
        prev.push(num);
        return prev;
      });
    } else {
      // handles Once & Simultaneous
      props.sendScore([num]);
    }
  }

  function handleSendScore() {
    if (buffer().length <= 0) return;

    if (!multipleScoringIsAllowed()) return;

    props.sendScore(buffer()).then((_) => {
      setBuffer((prev) => {
        prev.length = 0;
        return prev;
      });
    });
  }

  function handleCustomScore(e: FormSubmitEvent) {
    e.preventDefault();
    // @ts-ignore
    const value = e.currentTarget.elements[0]?.value ?? "a";

    handleScore(value);

    e.currentTarget.reset();
    let scoresPerTurn = props.scoresPerTurn ?? 1;
    if (buffer().length < scoresPerTurn)
      (e.currentTarget.elements[0] as HTMLInputElement)?.focus();
  }

  return (
    <div class="flex-1 p-2 md:p-4">
      <div
        class="relative transition-opacity duration-500"
        classList={{ "opacity-50": !props.isYourTurn() }}
      >
        <Show when={!props.isYourTurn()}>
          <span class="absolute inset-0 z-50 block" />
        </Show>
        <Show
          when={
            props.scoringRule !== "Once" && props.scoringRule !== "Simultaneous"
          }
        >
          <div class="relative mb-6 flex justify-center gap-8 md:mb-8">
            <For
              each={buffer()}
              fallback={
                <span class="box-content min-w-[3ch] rounded-md border-b-4 bg-primary-800 p-1 text-center opacity-0 md:text-lg">
                  0
                </span>
              }
            >
              {(num) => (
                <span class="box-content min-w-[3ch] rounded-md border-b-4 border-b-primary-950 bg-primary-800 p-1 text-center md:text-lg">
                  {num}
                </span>
              )}
            </For>
            <Button.Root onClick={removeLastScore}>
              <svg
                fill="currentColor"
                stroke-width="0"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 16 16"
                height="1.5em"
                width="1.5em"
                class="absolute right-0 top-0 transition-colors duration-100 hover:text-primary-200 md:right-12 lg:right-48"
              >
                <path d="M5.83 5.146a.5.5 0 0 0 0 .708L7.975 8l-2.147 2.146a.5.5 0 0 0 .707.708l2.147-2.147 2.146 2.147a.5.5 0 0 0 .707-.708L9.39 8l2.146-2.146a.5.5 0 0 0-.707-.708L8.683 7.293 6.536 5.146a.5.5 0 0 0-.707 0z"></path>
                <path d="M13.683 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-7.08a2 2 0 0 1-1.519-.698L.241 8.65a1 1 0 0 1 0-1.302L5.084 1.7A2 2 0 0 1 6.603 1h7.08zm-7.08 1a1 1 0 0 0-.76.35L1 8l4.844 5.65a1 1 0 0 0 .759.35h7.08a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1h-7.08z"></path>
              </svg>
            </Button.Root>
          </div>
        </Show>
        <form
          class="flex items-end justify-center gap-4 py-3"
          onSubmit={handleCustomScore}
        >
          <div class="max-w-xs">
            <TextFieldInput label="Custom input" inputMode="numeric" />
          </div>
          <Button.Root
            class="box-content min-w-[3ch] rounded-sm border px-2 py-[0.375em] transition-colors duration-100 hover:border-primary-200 hover:bg-primary-200/25"
            type="submit"
          >
            Add score
          </Button.Root>
        </form>
        <div class="flex flex-wrap justify-center gap-4 py-3 empty:hidden">
          <For each={props.commonInputs?.split(",")}>
            {(value) => (
              <Button.Root
                class="box-content min-w-[3ch] rounded-sm border p-2 transition-colors duration-100 empty:hidden hover:border-primary-200 hover:bg-primary-200/25 fine:p-1"
                onClick={[handleScore, value]}
              >
                {value}
              </Button.Root>
            )}
          </For>
        </div>
        <div class="grid place-items-center py-3 empty:hidden">
          <Show
            when={
              props.scoringRule !== "Once" &&
              props.scoringRule !== "Simultaneous"
            }
          >
            <Button.Root
              disabled={!multipleScoringIsAllowed()}
              onClick={handleSendScore}
              class="rounded-md border border-primary-400 bg-primary-400/20 px-4 py-2 text-lg transition-all hover:bg-primary-400 hover:text-black active:scale-90 disabled:!bg-primary-400/0 disabled:!text-colortext disabled:opacity-75 md:ml-4 fine:px-2 fine:py-1"
            >
              Turn finished
            </Button.Root>
          </Show>
        </div>
        <div class="grid place-items-center py-3 empty:hidden">
          <Show when={props.undeterminedEnding}>
            <Button.Root
              disabled={props.isMatchFinishedInitiated}
              onClick={() => {
                if (props.isMatchFinishedInitiated) return;

                const pb = newPocketbase();
                pb.send(`${PBENDPOINTS.undeterminedFinished}/init`, {
                  method: "POST",
                });
              }}
              class="w-full max-w-xs rounded-md border border-primary-600 p-3 transition-colors fine:p-2"
              classList={{
                "bg-primary-600/0 flex justify-center gap-4":
                  props.isMatchFinishedInitiated,
                "bg-primary-600/25 hover:bg-primary-600/50":
                  !props.isMatchFinishedInitiated,
              }}
            >
              <Show
                when={!props.isMatchFinishedInitiated}
                fallback={
                  <>
                    Confirming
                    <Spinner show size="textxl" />
                  </>
                }
              >
                Round is finished!
              </Show>
            </Button.Root>
          </Show>
        </div>
      </div>
    </div>
  );
}

type ScoreHistoryProps = {
  playerHistory: PlayerHistory;
  isOpponent: boolean;
};
function ScoreHistory(props: ScoreHistoryProps) {
  const usernames = useContext(TournamentContext).usernames;
  const avatarType =
    useContext(TournamentContext).tournament().expand.tournament.avatar;
  const history = () => {
    return props.playerHistory.history?.split(",");
  };

  const historyLength = () => {
    return history()?.length ?? 0;
  };

  return (
    <div class="flex w-full flex-col py-2">
      <h3 class="mb-4 grid grid-cols-[1fr_auto] px-2 text-lg md:mb-1 md:px-4 lg:mb-0">
        <div class="flex items-start pt-2 md:items-center md:pt-0">
          <span class="block px-2">Rounds won: </span>
          <span class="inline-block rounded-md bg-primary-900 px-2 md:mx-2">
            {props.playerHistory.rounds}
          </span>
        </div>
        <div class="flex justify-end">
          <img
            src={`https://api.dicebear.com/7.x/${avatarType}/png?seed=${
              usernames[props.playerHistory.id].avatar
            }`}
            alt={`player ${usernames[props.playerHistory.id].name}'s avatar`}
            class="max-w-[2.5rem] rounded-md shadow-secondary/40 outline outline-2 outline-offset-2 outline-secondary/50 [box-shadow:_0_0_8px_8px_var(--tw-shadow-color)]"
          />
        </div>
        <div class="col-span-2 flex justify-end pt-1">
          <Show
            when={props.isOpponent}
            fallback={<span class="pr-1">You</span>}
          >
            <span>{usernames[props.playerHistory.id].name}</span>
          </Show>
        </div>
      </h3>
      <ul class="flex min-h-[8rem] flex-1 flex-col justify-end gap-2">
        <For each={history()}>
          {(score, index) => (
            <li
              // TODO: show difference
              class="text-center md:text-lg"
              classList={{
                "opacity-50": historyLength() - index() === 5,
                "opacity-70": historyLength() - index() === 4,
                "opacity-80": historyLength() - index() === 3,
                "opacity-90": historyLength() - index() === 2,
                "pt-1 pb-0 text-xl border-t-2 border-primary-100/50":
                  historyLength() - index() === 1,
              }}
            >
              {score}
            </li>
          )}
        </For>
      </ul>
    </div>
  );
}

type MatchFinishedOverlayProps = {
  finished: boolean;
  youWon: boolean;
  winner: { name: string; avatar: string | null } | undefined;
};
function MatchFinishedOverlay(props: MatchFinishedOverlayProps) {
  const [show, setShow] = createSignal(props.finished);
  const avatarType =
    useContext(TournamentContext).tournament().expand.tournament.avatar;

  createEffect(() => {
    setShow(props.finished);
  });

  return (
    <Show when={show()}>
      <Dialog.Root open={show()}>
        <Dialog.Portal>
          <Dialog.Overlay class="fixed inset-0 top-12 z-50 flex items-start justify-center overflow-auto bg-black/50 pt-12 text-2xl font-bold leading-tight !outline-none backdrop-blur-sm ui-expanded:animate-modaloverlayenter md:pt-24 md:text-3xl lg:text-4xl fine:top-10">
            <span
              class="absolute inset-0 block rounded-full blur-3xl"
              classList={{
                "bg-error-300/10": !props.youWon,
                "bg-accent-500/5": props.youWon,
              }}
            />
            <div>
              <h1 class="mb-8 text-center text-xl opacity-75">
                Match is over!
              </h1>
              <h2 class="text-center text-2xl md:scale-125 md:text-3xl lg:scale-150">
                <Switch>
                  <Match when={props.winner?.name === "__tie__"}>
                    <span class="block">
                      It is a{" "}
                      <mark class="bg-primary-50/0 text-primary-400">tie!</mark>
                    </span>
                  </Match>
                  <Match
                    when={props.winner?.name !== "__tie__" && props.youWon}
                  >
                    <div class="flex flex-col items-center justify-center gap-4 md:flex-row">
                      <img
                        src={`https://api.dicebear.com/7.x/${avatarType}/png?seed=${props.winner?.avatar}`}
                        alt={`player ${props.winner?.name}'s avatar`}
                        class="max-w-[2.5rem] rounded-md shadow-secondary/40 outline outline-2 outline-offset-2 outline-secondary/50 [box-shadow:_0_0_8px_8px_var(--tw-shadow-color)]"
                      />
                      <span class="block">
                        <mark class="bg-primary-50/0 text-accent-400">You</mark>{" "}
                        won!
                      </span>
                    </div>
                  </Match>
                  <Match
                    when={props.winner?.name !== "__tie__" && !props.youWon}
                  >
                    <div class="flex flex-col items-center gap-2 md:flex-row md:gap-4">
                      <img
                        src={`https://api.dicebear.com/7.x/${avatarType}/png?seed=${props.winner?.avatar}`}
                        alt={`player ${props.winner?.name}'s avatar`}
                        class="max-w-[2.5rem] rounded-md shadow-secondary/40 outline outline-2 outline-offset-2 outline-secondary/50 [box-shadow:_0_0_8px_8px_var(--tw-shadow-color)]"
                      />
                      <span class="block">
                        <mark class="bg-primary-50/0 text-error-300">
                          {props.winner?.name}
                        </mark>{" "}
                        won!
                      </span>
                    </div>
                  </Match>
                </Switch>
              </h2>
            </div>
          </Dialog.Overlay>
        </Dialog.Portal>
      </Dialog.Root>
    </Show>
  );
}
