import { Record } from "pocketbase";
import anime from "animejs";
import VersusIcon from "../../../static/vs.png";
import CrownIcon from "../../../static/crown.png";
import { RoundScoringRow, SportRow } from "../../lobby";
import { SportRecord, SportRecordToModel } from "../../../models/Sport";
import {
  RoundScoringDBRecord,
  RoundScoringRecordToModel,
} from "../../../models/RoundScoring";
import { newPocketbase } from "../../../http/request";
import {
  For,
  Match,
  Show,
  Switch,
  createSignal,
  onCleanup,
  onMount,
  useContext,
} from "solid-js";
import { TournamentContext } from "../../game";
import Spinner from "../../../components/spinner";
import PBENDPOINTS from "../../../http/endpoints";
import LoadingButton from "../../../components/loadingbutton";

export type MatchesRounds = {
  [matchid: string]: Record;
};
type ScoreboardPageProps = {
  leaderboard: null | Record[];
  opponents: string[];
  currentRound: number;
  roundScorings: Record[];
  sports: Record[];
  matches: Record[];
  matchesRounds: MatchesRounds;
  tournamentStatus: string | null;
  everyoneFinished: boolean;
};
export default function ScoreboardPage(props: ScoreboardPageProps) {
  let ref: HTMLDivElement;
  const pb = newPocketbase();
  const you = pb.authStore.model?.id;
  const usernames = useContext(TournamentContext).usernames;
  const tournament = useContext(TournamentContext).tournament;
  const avatarType =
    useContext(TournamentContext).tournament().expand.tournament.avatar;

  onMount(() => {
    anime({
      targets: ref,
      translateX: ["-100%", 0],
      easing: "linear",
      duration: 400,
    });
  });

  return (
    <div
      ref={ref!}
      class="grid grid-cols-1 gap-2 md:grid-cols-2 md:gap-4 lg:gap-8"
    >
      <div class="md:col-span-2 md:-mb-4">
        <h1 class="mt-6 bg-gradient-to-tr from-primary-600 to-accent-300 bg-clip-text text-center text-2xl font-bold leading-none text-colortext/0 md:mt-10 md:text-3xl">
          {tournament()!.expand.tournament.name}
        </h1>
        <h2 class="mx-auto mb-2 w-max rounded-md bg-secondary/10 px-4 py-1 text-center text-lg md:text-xl">
          {props.tournamentStatus ?? "-"}
        </h2>
      </div>
      <Show
        when={props.leaderboard}
        fallback={
          <div>
            <h3>Scoreboard is loading</h3>
            <Spinner size="big" />
          </div>
        }
      >
        <Switch fallback={<h3>something went wrong</h3>}>
          <Match when={tournament()!.expand.tournament.type === "koth"}>
            <div class="flex flex-row overflow-hidden rounded-md bg-secondary/10 md:col-span-2 md:flex-col">
              <h4 class="-rotate-180 bg-primary-200/40 p-2 text-center text-lg leading-none [writing-mode:_tb-rl] md:rotate-0 md:[writing-mode:_horizontal-tb]">
                Opponents
              </h4>
              <ul class="relative flex flex-1 flex-col justify-center gap-2 p-2 md:gap-4 md:p-4 md:text-lg">
                <Show
                  when={
                    props.leaderboard?.find(
                      (player) => player.playerid === pb.authStore.model?.id,
                    )?.score !== -1
                  }
                  fallback={
                    <li>
                      <p class="p-2 text-center md:p-4">
                        <span class="block text-xl font-bold">
                          You are out!
                        </span>
                        <span class="block">Better luck next time!</span>
                      </p>
                    </li>
                  }
                >
                  <For each={props.opponents}>
                    {(id, index) => (
                      <li
                        class="grid grid-cols-[auto_1fr_auto] gap-1 rounded-md border-2 border-primary-50/10 bg-secondary/5 p-2 text-center transition-all duration-500"
                        classList={{
                          "opacity-75 line-through":
                            props.opponents.length - 1 - props.currentRound <
                              index() ||
                            (props.everyoneFinished &&
                              props.opponents.length -
                                1 -
                                props.currentRound ===
                                index()),
                          "!border-accent-400/60":
                            props.opponents.length - 1 - props.currentRound ===
                              index() && !props.everyoneFinished,
                        }}
                      >
                        <span class="flex items-center justify-center">
                          #{props.opponents.length - index()}
                        </span>
                        <span class="flex items-center justify-center break-all">
                          {id !== "X" ? usernames[id].name : "No opponent"}
                        </span>
                        <Show
                          when={id !== "X"}
                          fallback={
                            <span class="block h-[2.5rem] w-[2.5rem]" />
                          }
                        >
                          <img
                            src={`https://api.dicebear.com/7.x/${avatarType}/png?seed=${usernames[id].avatar}`}
                            alt={`player ${usernames[id].name}'s avatar`}
                            class="max-w-[2.5rem] rounded-md shadow-secondary/40 outline outline-2 outline-offset-2 outline-secondary/50 [box-shadow:_0_0_8px_8px_var(--tw-shadow-color)]"
                          />
                        </Show>
                      </li>
                    )}
                  </For>
                </Show>
              </ul>
            </div>
            <MatchesTree
              currentRound={props.currentRound}
              roundScorings={props.roundScorings}
              sports={props.sports}
              matches={props.matches}
              matchesRounds={props.matchesRounds}
              everyoneFinished={props.everyoneFinished}
            />
            <TournamentHandlerSection />
          </Match>
          <Match when={tournament()!.expand.tournament.type === "roundrobin"}>
            <div class="flex overflow-hidden rounded-md bg-secondary/10">
              <h4 class="-rotate-180 bg-primary-200/40 p-2 text-center text-lg leading-none [writing-mode:_tb-rl] md:py-4">
                Leaderboard
              </h4>
              <ul class="flex flex-1 flex-col justify-center gap-2 p-2 md:justify-start md:gap-4 md:p-4 md:text-lg">
                <For each={props.leaderboard as Record[]}>
                  {(playerScore, index) => (
                    <li
                      class="grid grid-cols-[auto_1fr_auto] gap-2 rounded-md bg-secondary/5 pr-2"
                      classList={{
                        "border-b border-accent-400/60 !rounded-br-none":
                          playerScore.playerid === you,
                        "mx-1 lg:mx-2": index() === 1,
                        "mx-2 lg:mx-4": index() === 2,
                        "mx-3 lg:mx-6": index() >= 3,
                      }}
                    >
                      <span
                        class="grid min-w-[4ch] place-items-center rounded-l-md border border-primary-200 px-1 py-2"
                        classList={{
                          "border-b-0 !border-accent-400/60 !rounded-br-none":
                            playerScore.playerid === you,
                          "!bg-accent-400/25":
                            index() === 0 && playerScore.playerid === you,
                          "!bg-primary-200 text-black":
                            index() === 0 && playerScore.playerid !== you,
                        }}
                      >
                        #{index() + 1}
                      </span>
                      <span class="flex flex-col items-center gap-2 py-2 lg:flex-row lg:gap-4 lg:px-2">
                        <img
                          src={`https://api.dicebear.com/7.x/${avatarType}/png?seed=${
                            usernames[playerScore.playerid].avatar
                          }`}
                          alt={`player ${
                            usernames[playerScore.playerid].name
                          }'s avatar`}
                          class="max-w-[2.5rem] rounded-md shadow-secondary/40 outline outline-2 outline-offset-2 outline-secondary/50 [box-shadow:_0_0_8px_8px_var(--tw-shadow-color)]"
                        />
                        <span
                          classList={{
                            "after:content-['(You)'] after:opacity-75 after:min-w-[5ch]":
                              playerScore.playerid === you,
                          }}
                          class="flex items-center justify-center gap-2 break-all"
                        >
                          {playerScore.username}
                        </span>
                      </span>
                      <span class="grid place-items-center">
                        {playerScore.score}
                      </span>
                    </li>
                  )}
                </For>
              </ul>
            </div>
            <div class="flex flex-row overflow-hidden rounded-md bg-secondary/10 md:flex-row-reverse">
              <h4 class="-rotate-180 bg-primary-200/40 p-2 text-center text-lg leading-none [writing-mode:_tb-rl] md:py-4">
                Opponents
              </h4>
              <ul class="flex flex-1 flex-col justify-center gap-2 p-2 md:gap-4 md:p-4 md:text-lg">
                <For each={props.opponents}>
                  {(id, index) => (
                    <li
                      class="grid grid-cols-[auto_1fr_auto] gap-1 rounded-md border-2 border-primary-50/10 bg-secondary/5 p-2 text-center transition-all duration-500"
                      classList={{
                        "opacity-75 line-through":
                          props.opponents.length - 1 - props.currentRound <
                            index() ||
                          (props.everyoneFinished &&
                            props.opponents.length - 1 - props.currentRound ===
                              index()),
                        "!border-accent-400/60":
                          props.opponents.length - 1 - props.currentRound ===
                            index() && !props.everyoneFinished,
                      }}
                    >
                      <span class="flex items-center justify-center">
                        #{props.opponents.length - index()}
                      </span>
                      <span class="flex items-center justify-center break-all">
                        {id !== "X" ? usernames[id].name : "No opponent"}
                      </span>
                      <Show
                        when={id !== "X"}
                        fallback={<span class="block h-[2.5rem] w-[2.5rem]" />}
                      >
                        <img
                          src={`https://api.dicebear.com/7.x/${avatarType}/png?seed=${usernames[id].avatar}`}
                          alt={`player ${usernames[id].name}'s avatar`}
                          class="max-w-[2.5rem] rounded-md shadow-secondary/40 outline outline-2 outline-offset-2 outline-secondary/50 [box-shadow:_0_0_8px_8px_var(--tw-shadow-color)]"
                        />
                      </Show>
                    </li>
                  )}
                </For>
              </ul>
            </div>
            <MatchesSection
              currentRound={props.currentRound}
              roundScorings={props.roundScorings}
              sports={props.sports}
              matches={props.matches}
              matchesRounds={props.matchesRounds}
              everyoneFinished={props.everyoneFinished}
            />
            <TournamentHandlerSection />
          </Match>
        </Switch>
      </Show>
    </div>
  );
}

function TournamentHandlerSection() {
  const pb = newPocketbase();
  const [isOwner, setIsOwner] = createSignal(false);
  const [isLoading, setIsLoading] = createSignal(false);
  const tournamentData = useContext(TournamentContext).tournament;
  const owner =
    useContext(TournamentContext).tournament().expand.tournament.owner;

  const disabled = () =>
    tournamentData().currentround !== -1 && !tournamentData().everyonefinished;
  const label = () => {
    if (tournamentData().currentround === -1) return "Start first round!";
    if (!tournamentData().everyonefinished) return "Matches are in progress!";
    if (
      tournamentData().expand.tournament.rounds ===
      tournamentData().currentround + 1
    )
      return "Finish tournament!";
    return "Start next round!";
  };

  onMount(() => {
    if (pb.authStore.model?.id === owner) setIsOwner(true);
  });

  function start() {
    setIsLoading(true);
    pb.send(PBENDPOINTS.startNextRound, { method: "POST" })
      .then()
      .finally(() => setIsLoading(false));
  }

  return (
    <Show when={isOwner()}>
      <section class="rounded-md bg-secondary/10 p-4 md:col-span-2">
        <LoadingButton
          label={label()}
          loading={isLoading()}
          onClick={start}
          disabled={disabled()}
        />
      </section>
    </Show>
  );
}

type MatchesSectionProps = {
  currentRound: number;
  roundScorings: Record[];
  sports: Record[];
  matches: Record[];
  matchesRounds: MatchesRounds;
  everyoneFinished: boolean;
};
function MatchesSection(props: MatchesSectionProps) {
  let ulref: HTMLUListElement;
  let alreadyKnowsScrolls = false;
  const [showArrows, setShowArrows] = createSignal(true);

  function handleScrollEnd(e: Event) {
    if (alreadyKnowsScrolls) return;

    const target = e.currentTarget as HTMLUListElement;
    if (target.scrollWidth - target.offsetWidth - target.scrollLeft < 100) {
      setShowArrows(false);
      alreadyKnowsScrolls = true;
    } else setShowArrows(true);
  }

  onMount(() => {
    ulref.addEventListener("scroll", handleScrollEnd);
  });

  onCleanup(() => {
    ulref.removeEventListener("scroll", handleScrollEnd);
  });

  const allMatches = Array(props.sports.length)
    .fill(1)
    .map((_) => []) as Record[][];
  for (const match of props.matches) {
    allMatches[match.tournamentround].push(match);
  }

  return (
    <div class="flex flex-row overflow-hidden rounded-md bg-secondary/10 md:col-span-2 md:flex-col">
      <h4 class="-rotate-180 bg-primary-200/40 p-2 text-center text-lg leading-none [writing-mode:_tb-rl] md:rotate-0 md:[writing-mode:_horizontal-tb]">
        Matches
      </h4>
      <ul
        ref={ulref!}
        class="relative grid gap-16 overflow-x-scroll overscroll-x-contain p-4 [grid-auto-columns:100%] [grid-auto-flow:column] md:overflow-x-auto md:[grid-auto-columns:65%] lg:[grid-auto-columns:45%]"
      >
        <For each={allMatches}>
          {(roundMatches, index) => (
            <li
              class="relative overflow-hidden transition-all"
              classList={{
                "outline outline-2 rounded-sm outline-offset-8 outline-accent-400/60":
                  index() === props.currentRound && !props.everyoneFinished,
                "opacity-75": index() < props.currentRound,
              }}
            >
              <strong class="mb-8 block text-center text-lg md:text-xl">
                Round {index() + 1}
              </strong>
              <div class="mb-4 opacity-90">
                <SportRow
                  sport={SportRecordToModel(
                    props.sports[index()] as unknown as SportRecord,
                  )}
                />
              </div>
              <div class="mb-8 opacity-90">
                <RoundScoringRow
                  scoring={
                    RoundScoringRecordToModel(
                      props.roundScorings[
                        index()
                      ] as unknown as RoundScoringDBRecord,
                    )!
                  }
                />
              </div>
              <ul class="flex flex-col gap-4">
                <For each={roundMatches}>
                  {(match) => (
                    <VersusCard
                      currentRound={props.currentRound}
                      match={props.matchesRounds[match.id]}
                    />
                  )}
                </For>
              </ul>
            </li>
          )}
        </For>
        <li
          class="pointer-events-none fixed right-1 z-10 h-6 w-6 translate-y-64 animate-pulse md:hidden"
          classList={{ "!opacity-0": !showArrows() }}
        >
          <svg
            fill="currentColor"
            stroke-width="0"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 16 16"
            height="3rem"
            width="1.5rem"
          >
            <path
              fill-rule="evenodd"
              d="M3.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L9.293 8 3.646 2.354a.5.5 0 0 1 0-.708z"
            ></path>
            <path
              fill-rule="evenodd"
              d="M7.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L13.293 8 7.646 2.354a.5.5 0 0 1 0-.708z"
            ></path>
          </svg>
        </li>
        <li
          class="pointer-events-none fixed right-1 z-10 h-6 w-6 translate-y-40 animate-pulse md:hidden"
          classList={{ "!opacity-0": !showArrows() }}
        >
          <svg
            fill="currentColor"
            stroke-width="0"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 16 16"
            height="3rem"
            width="1.5rem"
          >
            <path
              fill-rule="evenodd"
              d="M3.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L9.293 8 3.646 2.354a.5.5 0 0 1 0-.708z"
            ></path>
            <path
              fill-rule="evenodd"
              d="M7.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L13.293 8 7.646 2.354a.5.5 0 0 1 0-.708z"
            ></path>
          </svg>
        </li>
      </ul>
    </div>
  );
}

type VersusCardProps = {
  currentRound: number;
  match: Record;
};
function VersusCard(props: VersusCardProps) {
  const pb = newPocketbase();
  const usernames = useContext(TournamentContext).usernames;
  const avatarType =
    useContext(TournamentContext).tournament().expand.tournament.avatar;

  const playerone = usernames[props.match.playeroneid];
  const playertwo = usernames[props.match.playertwoid];

  function getMatchState(match: Record) {
    if (match.currentround !== -1 && !props.match.winnerid) return "ingame";
    if (match.winnerid) return "finished";
    //if(match.currentround === -1) return "notstarted"
    return "notstarted";
  }

  const matchState = () => getMatchState(props.match);

  return (
    <li
      class="flex w-full flex-col gap-4 rounded-md border-2 border-primary-200/10 bg-secondary/10 p-4 pt-8 leading-tight md:grid md:grid-cols-[1fr_auto_1fr] md:gap-0 md:text-lg"
      classList={{
        "!border-accent-400/25": matchState() === "ingame",
        "!border-primary-400/25": matchState() === "finished",
      }}
    >
      <span
        class="relative flex flex-row items-center gap-4"
        classList={{
          "opacity-60": !playerone,
          "bg-gradient-to-r from-primary-400/25 rounded-md":
            props.match.playeroneid === pb.authStore.model?.id,
        }}
      >
        <Show when={props.match.winnerid === props.match.playeroneid}>
          <img
            src={CrownIcon}
            class="absolute -top-8 left-1 h-8 w-8 invert-[90%]"
          />
        </Show>
        <Show when={playerone}>
          <img
            src={`https://api.dicebear.com/7.x/${avatarType}/png?seed=${playerone.avatar}`}
            alt={`player ${playerone.name}'s avatar`}
            class="max-w-[2.5rem] rounded-md shadow-secondary/40 outline outline-2 outline-offset-2 outline-secondary/50 [box-shadow:_0_0_8px_8px_var(--tw-shadow-color)]"
          />
        </Show>
        <span class="max-w-[12ch] overflow-hidden text-ellipsis whitespace-nowrap [overflow-wrap:anywhere]">
          {!playerone ? "No opponent" : playerone.name}
        </span>
      </span>
      <span
        class="flex justify-center"
        classList={{
          "opacity-0 hidden md:flex": !playerone || !playertwo,
        }}
      >
        <img
          src={VersusIcon}
          class="h-8 w-8 invert-[90%]"
          classList={{
            "animate-breath":
              props.match.currentround !== -1 && !props.match.winnerid,
          }}
        />
      </span>
      <span
        class="relative flex flex-row-reverse items-center gap-4"
        classList={{
          "opacity-60": !playertwo,
          "bg-gradient-to-l from-primary-400/25 rounded-md":
            props.match.playertwoid === pb.authStore.model?.id,
        }}
      >
        <Show when={props.match.winnerid === props.match.playertwoid}>
          <img
            src={CrownIcon}
            class="absolute -top-8 right-1 h-8 w-8 invert-[90%]"
          />
        </Show>
        <Show when={playertwo}>
          <img
            src={`https://api.dicebear.com/7.x/${avatarType}/png?seed=${playertwo.avatar}`}
            alt={`player ${playertwo.name}'s avatar`}
            class="max-w-[2.5rem] rounded-md shadow-secondary/40 outline outline-2 outline-offset-2 outline-secondary/50 [box-shadow:_0_0_8px_8px_var(--tw-shadow-color)]"
          />
        </Show>
        <span class="max-w-[12ch] overflow-hidden text-ellipsis whitespace-nowrap [overflow-wrap:anywhere]">
          {!playertwo ? "No opponent" : playertwo.name}
        </span>
      </span>
      <span class="col-span-3 mt-4 flex justify-center text-base">
        <span
          class="relative isolate flex items-center py-1 text-center"
          classList={{
            "px-6":
              matchState() === "notstarted" || matchState() === "finished",
            "px-2": matchState() === "ingame",
          }}
        >
          <span
            class="absolute inset-0 -z-10 block -skew-x-12 rounded-md border bg-gradient-to-tr"
            classList={{
              "from-background/70 to-background/10 border-background/30":
                matchState() === "notstarted",
              "from-accent-500/50 to-accent-500/20 border-accent-500/20":
                matchState() === "ingame",
              "from-primary-400/40 to-primary-400/10 border-primary-400/30":
                matchState() === "finished",
            }}
          />
          <Switch>
            <Match when={matchState() === "finished"}>
              <Show
                when={props.match.winnerid !== "__tie__"}
                fallback={"It's a tie!"}
              >
                The winner is {usernames[props.match.winnerid].name}!
              </Show>
            </Match>
            <Match when={matchState() === "notstarted"}>
              Match not yet started!
            </Match>
            <Match when={matchState() === "ingame"}>
              <span
                class="inline-block items-center pr-2"
                classList={{
                  "font-bold":
                    props.match.playeronerounds > props.match.playertworounds,
                }}
              >
                {props.match.playeronerounds}
              </span>
              <span class="inline-block h-full w-[2px] -skew-x-12 bg-secondary/60" />
              <span class="inline-block px-4 text-center">
                Current round is {props.match.currentround + 1}
              </span>
              <span class="inline-block h-full w-[2px] -skew-x-12 bg-secondary/60" />
              <span
                class="inline-block items-center pl-2"
                classList={{
                  "font-bold":
                    props.match.playertworounds > props.match.playeronerounds,
                }}
              >
                {props.match.playertworounds}
              </span>
            </Match>
          </Switch>
        </span>
      </span>
    </li>
  );
}

function MatchesTree(props: MatchesSectionProps) {
  // TODO: Arrows

  const allMatches = Array(props.sports.length)
    .fill(1)
    .map((_) => []) as unknown as Array<
    [Record | undefined | null, Record | undefined][]
  >;

  for (let i = 0; i < props.matches.length; i++) {
    const first = props.matches[i];
    const second = props.matches[i + 1];

    if (i > 0 && first.branch === props.matches[i - 1]?.branch) continue;

    const arr = [];
    arr[0] = first;
    if (first.branch === second?.branch) arr[1] = second;

    allMatches[first.tournamentround].push(arr as [Record, Record]);
  }
  let previousMatches = 0;
  for (let i = 0; i < allMatches.length; i++) {
    const roundMatches = allMatches[i];

    if (roundMatches.length > 0) {
      previousMatches = roundMatches.length;
    } else {
      const exponent = Math.ceil(Math.log2(previousMatches)) - 1;
      previousMatches = Math.pow(2, exponent);
      for (let j = 0; j < previousMatches; j++) {
        if (previousMatches < 1) allMatches[i].push([null, undefined]);
        else allMatches[i].push([undefined, undefined]);
      }
    }
  }

  return (
    <div class="flex flex-row overflow-hidden rounded-md bg-secondary/10 md:col-span-2 md:flex-col">
      <h4 class="-rotate-180 bg-primary-200/40 p-2 text-center text-lg leading-none [writing-mode:_tb-rl] md:rotate-0 md:[writing-mode:_horizontal-tb]">
        Matches
      </h4>
      <ul class="relative grid gap-16 overflow-x-scroll overscroll-x-contain p-4 [grid-auto-columns:100%] [grid-auto-flow:column] md:overflow-x-auto md:[grid-auto-columns:65%] lg:[grid-auto-columns:45%]">
        <For each={allMatches}>
          {(roundMatches, roundIndex) => (
            <li
              class="relative flex flex-col transition-all"
              classList={{
                "outline outline-2 rounded-sm outline-offset-8 outline-accent-400/60":
                  roundIndex() === props.currentRound &&
                  !props.everyoneFinished,
                "opacity-75": roundIndex() < props.currentRound,
              }}
            >
              <strong class="mb-8 block text-center text-lg md:text-xl">
                Round {roundIndex() + 1}
              </strong>
              <div class="mb-8 opacity-90">
                <SportRow
                  sport={SportRecordToModel(
                    props.sports[roundIndex()] as unknown as SportRecord,
                  )}
                />
              </div>
              <ul class="flex flex-1 flex-col gap-8">
                <For each={roundMatches}>
                  {(match, matchIndex) => (
                    <>
                      <div class="flex flex-1 flex-col justify-center gap-4 rounded-sm outline outline-2 outline-offset-4 outline-primary-200/25">
                        <Show when={!match[0] && !match[1]}>
                          <PlaceholderMatch />
                          <Show when={typeof match[0] === "undefined"}>
                            <PlaceholderMatch />
                          </Show>
                        </Show>
                        <Show when={match[0]}>
                          <VersusCard
                            currentRound={props.currentRound}
                            match={props.matchesRounds[match[0]!.id]}
                          />
                        </Show>
                        <Show when={match[1]}>
                          <VersusCard
                            currentRound={props.currentRound}
                            match={props.matchesRounds[match[1]!.id]}
                          />
                        </Show>
                      </div>
                    </>
                  )}
                </For>
              </ul>
            </li>
          )}
        </For>
      </ul>
    </div>
  );
}

function PlaceholderMatch() {
  return (
    <li class="flex w-full flex-col gap-4 rounded-md border-2 border-primary-200/10 bg-secondary/10 p-4 pt-8 leading-tight md:text-lg">
      <div class="flex flex-col items-center md:flex-row">
        <span class="block h-[2.5rem] w-0" />
        <span class="block opacity-75">Not yet determined</span>
        <span class="block flex-1 text-right opacity-75">
          Not yet determined
        </span>
      </div>
      <span class="block opacity-0">placeholder</span>
    </li>
  );
}
