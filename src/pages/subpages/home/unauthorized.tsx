import { Button, Dialog, Tabs } from "@kobalte/core";
import { A } from "@solidjs/router";
import ROUTES from "../../../static/constants/routes";
import TextFieldInput from "../../../components/textfield";
import { FormSubmitEvent } from "../../../models/types";
import request, { newPocketbase } from "../../../http/request";
import refreshIsAuthorized from "../../../hooks/isauthorized";
import { Setter, Show, createSignal, useContext } from "solid-js";
import { z } from "zod";
import { ToasterContext } from "../../../app";
import SubmitButton from "../../../components/submitbutton";

export default function Unauthorized() {
  const [selectedTab, setSelectedTab] = createSignal("login");
  function successfulRegister() {
    setSelectedTab("login");
  }

  return (
    <>
      <Tabs.Root
        aria-label="Main navigation"
        value={selectedTab()}
        onChange={setSelectedTab}
      >
        <Tabs.List class="flex">
          <Tabs.Trigger
            class="group relative flex-1 p-2 text-lg uppercase opacity-75 ui-selected:font-bold ui-selected:opacity-100 md:text-xl"
            value="login"
          >
            <span class="absolute bottom-0 left-0 block h-[1px] w-full scale-x-0 bg-accent-100 transition-transform duration-300 group-hover:scale-x-[0.25] ui-group-selected:scale-x-100" />
            Login
          </Tabs.Trigger>
          <Tabs.Trigger
            class="group relative flex-1 p-2 text-lg uppercase opacity-75 ui-selected:font-bold ui-selected:opacity-100 md:text-xl"
            value="register"
          >
            <span class="absolute bottom-0 left-0 block h-[1px] w-full scale-x-0 bg-accent-100 transition-transform duration-300 group-hover:scale-x-[0.25] ui-group-selected:scale-x-100" />
            Register
          </Tabs.Trigger>
        </Tabs.List>
        <Tabs.Content value="login">
          <Login />
        </Tabs.Content>
        <Tabs.Content value="register">
          <Register redirect={successfulRegister} />
        </Tabs.Content>
      </Tabs.Root>
      <ul class="-mx-6 flex translate-y-6 justify-between break-words border border-primary-50/50 px-4 text-xs opacity-70 md:-mx-10 md:translate-y-10 md:text-sm lg:-mx-12 lg:translate-y-12">
        <li>Made by Norbert Tanács</li>
        <li>
          <A
            href={ROUTES.dependencies}
            class="flex items-center gap-[2px] hover:text-primary-100 hover:underline"
          >
            Dependencies
            <svg
              fill="currentColor"
              stroke-width="0"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 16 16"
              height="1em"
              width="1em"
            >
              <path d="M4.715 6.542 3.343 7.914a3 3 0 1 0 4.243 4.243l1.828-1.829A3 3 0 0 0 8.586 5.5L8 6.086a1.002 1.002 0 0 0-.154.199 2 2 0 0 1 .861 3.337L6.88 11.45a2 2 0 1 1-2.83-2.83l.793-.792a4.018 4.018 0 0 1-.128-1.287z"></path>
              <path d="M6.586 4.672A3 3 0 0 0 7.414 9.5l.775-.776a2 2 0 0 1-.896-3.346L9.12 3.55a2 2 0 1 1 2.83 2.83l-.793.792c.112.42.155.855.128 1.287l1.372-1.372a3 3 0 1 0-4.243-4.243L6.586 4.672z"></path>
            </svg>
          </A>
        </li>
      </ul>
    </>
  );
}

type RegisterTypes = {
  redirect: () => void;
};
function Register(props: RegisterTypes) {
  const [isLoading, setIsLoading] = createSignal(false);
  const [password, setPassword] = createSignal("");
  const [passwordAgain, setPasswordAgain] = createSignal("");
  const passwordsValid = () =>
    password() !== "" &&
    password() === passwordAgain() &&
    password().length >= 12;
  const toast = useContext(ToasterContext);

  function handleSubmit(e: FormSubmitEvent) {
    e.preventDefault();
    if (isLoading()) return;
    const formData = new FormData(e.currentTarget);

    const pb = newPocketbase();
    const submit = async () => {
      setIsLoading(true);
      await new Promise((r) => setTimeout(r, 500));
      const data = {
        username: formData.get("username"),
        email: formData.get("email"),
        emailVisibility: false,
        password: formData.get("password"),
        passwordConfirm: formData.get("passwordagain"),
        selectedavatar: null,
        registered: true,
      };

      await pb.collection("users").create(data);
    };

    submit()
      .then(async () => {
        await new Promise((r) => setTimeout(r, 2500));
        toast.default({
          title: "Register successful!",
          //description: "Validation email was sent!",
        });
        props.redirect();
        // TODO: ask backend to send verification email
      })
      .catch((reason) => {
        const response = reason.data;
        let description;
        if (response.data.passwordConfirm)
          description = "Passwords do not match!";
        if (!description && response.data.password)
          description = response.data.password.message;
        if (!description && response.data.username)
          description = response.data.username.message;
        toast.error({
          title: response.message,
          description: description,
        });
      })
      .finally(() => {
        setIsLoading(false);
      });
  }

  return (
    <>
      <form onSubmit={handleSubmit} class="mt-10 flex flex-col gap-8">
        <TextFieldInput
          name="username"
          label="Username"
          required
          description="You do not need to log in if someone else creates a tournament already!"
          schema={z
            .string()
            .min(3, { message: "Min length is 3!" })
            .max(32, { message: "Max length is 32!" })
            .regex(new RegExp("^[a-zA-Z0-9]+$"), {
              message: "Letters and numbers only!",
            })}
        />
        <TextFieldInput
          name="email"
          label="Email"
          required
          schema={z.string().email({ message: "This is not a valid email!" })}
        />
        <TextFieldInput
          name="password"
          type="password"
          label="Password"
          onInput={(event) => {
            setPassword(event.currentTarget.value);
          }}
          required
          schema={z
            .string()
            .min(12, { message: "Min length is 12!" })
            .max(99, { message: "Max length is 99!" })}
        />
        <TextFieldInput
          name="passwordagain"
          type="password"
          label="Password Again"
          onInput={(event) => {
            setPasswordAgain(event.currentTarget.value);
          }}
          required
        />
        <SubmitButton
          disabled={!passwordsValid()}
          loading={isLoading()}
          label="Register!"
        />
      </form>
    </>
  );
}

function Login() {
  const [isLoading, setIsLoading] = createSignal(false);
  const [showResetPassword, setShowResetPassword] = createSignal(false);
  const toast = useContext(ToasterContext);

  function handleSubmit(e: FormSubmitEvent) {
    e.preventDefault();
    if (isLoading()) return;
    const formData = new FormData(e.currentTarget);
    const usernamemail = formData.get("usernamemail");
    const password = formData.get("password");

    if (typeof usernamemail !== "string" || typeof password !== "string")
      return;
    request(async (pb) => {
      setIsLoading(true);
      await new Promise((r) => setTimeout(r, 250));
      await pb.collection("users").authWithPassword(usernamemail, password);
    })
      .then(() => {
        refreshIsAuthorized();
        toast.default({
          title: "Successful login",
        });
      })
      .catch((reason) => {
        const response = reason.data;
        toast.error({
          title: response.message,
        });
      })
      .finally(() => {
        setIsLoading(false);
      });
  }

  return (
    <>
      <form onSubmit={handleSubmit} class="mt-10 flex flex-col gap-8">
        <TextFieldInput
          name="usernamemail"
          label="Username / Email"
          required
          description="You do not need to log in if someone else creates a tournament already!"
        />
        <TextFieldInput
          name="password"
          type="password"
          label="Password"
          required
        />
        <SubmitButton loading={isLoading()} label="Login" />
        <Show when={false}>
          <Button.Root
            onClick={() => setShowResetPassword(true)}
            class="mx-auto -mt-6 max-w-max opacity-75 transition-opacity hover:opacity-100"
          >
            Forgotten password
          </Button.Root>
        </Show>
      </form>
      <PasswordDialog
        show={showResetPassword()}
        setShow={setShowResetPassword}
      />
    </>
  );
}

type PasswordDialogProps = {
  show: boolean;
  setShow: Setter<boolean>;
};
function PasswordDialog(props: PasswordDialogProps) {
  const [emailSent, setEmailSent] = createSignal(false);

  function handleSubmit(e: FormSubmitEvent) {
    console.log("hi");
    setEmailSent(true);
  }

  return (
    <Dialog.Root open={props.show}>
      <Dialog.Portal>
        <Dialog.Overlay class="fixed inset-0 z-50 grid place-items-center overflow-auto p-4 backdrop-blur-[1px] ui-expanded:animate-modaloverlayenter ui-closed:animate-modaloverlayexit">
          <Dialog.Content class="relative w-full max-w-[500px] rounded-sm border border-primary-200 bg-background p-2 shadow-xl shadow-black md:p-4">
            <div class="relative">
              <Dialog.Title class="text-xl md:text-2xl">
                Password reset
              </Dialog.Title>
              <Dialog.CloseButton
                class="absolute right-0 top-0 -translate-x-1 text-[1.5em] leading-none transition-transform duration-100 hover:scale-125"
                onClick={() => props.setShow(false)}
              >
                {"\u26cc"}
              </Dialog.CloseButton>
            </div>
            <Show
              when={emailSent()}
              fallback={
                <form
                  class="mt-12 flex flex-col gap-4 px-2"
                  onSubmit={handleSubmit}
                >
                  <TextFieldInput
                    name="usernamemail"
                    label="Username / Email to reset the password"
                    required
                  />
                  <SubmitButton loading={false} label="Reset" />
                </form>
              }
            >
              <p class="mt-8 px-2">
                Email was sent! It should be in your inbox in a couple of
                minutes!
              </p>
            </Show>
          </Dialog.Content>
        </Dialog.Overlay>
      </Dialog.Portal>
    </Dialog.Root>
  );
}
