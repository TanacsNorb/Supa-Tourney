import { A } from "@solidjs/router";
import { ParentProps } from "solid-js";
import ROUTES from "../../../static/constants/routes";
import { Button } from "@kobalte/core";
import { newPocketbase } from "../../../http/request";
import refreshIsAuthorized from "../../../hooks/isauthorized";

export default function AuthorizedHome() {
  function logout() {
    const pb = newPocketbase();
    pb.authStore.clear();
    new Promise((r) => setTimeout(r, 10)).then(() => refreshIsAuthorized());
  }

  return (
    <>
      <div class="mb-[max(4vh,_1.25rem)] flex flex-col gap-[max(8vh,_2rem)]">
        <MainLinkButton href={ROUTES.joinGame}>Join a game</MainLinkButton>
        <MainLinkButton href={ROUTES.createTournament}>
          Create a tournament
        </MainLinkButton>
      </div>
      <ul class="relative -mx-6 flex translate-y-6 justify-between break-words border border-primary-50/50 px-4 text-xs opacity-70 md:-mx-10 md:translate-y-10 md:text-sm lg:-mx-12 lg:translate-y-12">
        <li>Made by Norbert Tanács</li>
        <li class="absolute -top-full left-[50%] translate-x-[-50%]">
          <Button.Root
            onClick={logout}
            class="transition-colors duration-100 hover:text-warning-400 focus-visible:text-warning-400"
          >
            Logout
          </Button.Root>
        </li>
        <li>
          <A
            href={ROUTES.dependencies}
            class="flex items-center gap-[2px] hover:text-primary-100 hover:underline"
          >
            Dependencies
            <svg
              fill="currentColor"
              stroke-width="0"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 16 16"
              height="1em"
              width="1em"
            >
              <path d="M4.715 6.542 3.343 7.914a3 3 0 1 0 4.243 4.243l1.828-1.829A3 3 0 0 0 8.586 5.5L8 6.086a1.002 1.002 0 0 0-.154.199 2 2 0 0 1 .861 3.337L6.88 11.45a2 2 0 1 1-2.83-2.83l.793-.792a4.018 4.018 0 0 1-.128-1.287z"></path>
              <path d="M6.586 4.672A3 3 0 0 0 7.414 9.5l.775-.776a2 2 0 0 1-.896-3.346L9.12 3.55a2 2 0 1 1 2.83 2.83l-.793.792c.112.42.155.855.128 1.287l1.372-1.372a3 3 0 1 0-4.243-4.243L6.586 4.672z"></path>
            </svg>
          </A>
        </li>
      </ul>
    </>
  );
}

type MainLinkButtonProps = {
  href: string;
};
function MainLinkButton(props: ParentProps<MainLinkButtonProps>) {
  return (
    <A
      class="relative border-b-2 border-primary-100 px-[4ch] py-2 text-center text-lg !leading-none transition-colors hover:border-accent-500 hover:text-primary-700 focus-visible:outline-dashed focus-visible:outline-4 focus-visible:outline-offset-2 focus-visible:outline-accent-500 md:py-4 md:text-xl"
      classList={{
        "after:content-['<--'] after:!absolute after:right-1 after:top-[calc(50%_-_0.5em)] after:pointer-events-none after:opacity-0 hover:after:opacity-100 after:transition-opacity after:text-accent-500":
          true,
        "before:content-['-->'] before:!absolute before:left-1 before:top-[calc(50%_-_0.5em)] before:pointer-events-none before:opacity-0 hover:before:opacity-100 before:transition-opacity before:text-accent-500":
          true,
      }}
      href={props.href}
    >
      {props.children}
    </A>
  );
}
