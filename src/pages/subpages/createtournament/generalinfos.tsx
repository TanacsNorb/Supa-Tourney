import { createSignal, useContext } from "solid-js";
import { AnimatedForm, StepperFormContext } from "../../createtournament";
import { ComponentTypes } from "./tournamentsetup";
import ButtonGroup from "../../../components/buttongroup";
import Checkbox from "../../../components/checkbox";
import { FormSubmitEvent } from "../../../models/types";

const ICONTYPES = {
  adventurer: "adventurer-neutral",
  bottts: "bottts-neutral",
  funemoji: "fun-emoji",
  thumbs: "thumbs",
} as const;
type IconTypesKeys = keyof typeof ICONTYPES;
type IconTypesValues = (typeof ICONTYPES)[IconTypesKeys];

export default function GeneralInfos(props: ComponentTypes) {
  const formReaction = useContext(StepperFormContext).reaction;
  const [imageLoaded, setImageLoaded] = createSignal(false);
  const [iconType, setIconType] = createSignal<IconTypesValues>(
    ICONTYPES.bottts,
  );

  function handleSubmit(e: FormSubmitEvent) {
    e.preventDefault();
    const form = e.currentTarget;
    const formData = new FormData(form);
    formReaction(formData, props.formId);
  }

  const dicebearButtons = [
    {
      label: "Adventurer",
      value: ICONTYPES.adventurer,
    },
    {
      label: "Bottts",
      value: ICONTYPES.bottts,
    },
    {
      label: "Fun emoji",
      value: ICONTYPES.funemoji,
    },
    {
      label: "Thumbs",
      value: ICONTYPES.thumbs,
    },
  ];

  return (
    <>
      <AnimatedForm id={props.formId} handleSubmit={handleSubmit}>
        <h2 class="-mt-4 mb-4 border-b border-b-primary-400 pb-1 text-center text-xl font-bold leading-none text-colortext md:mb-10 md:text-3xl">
          Final Information
        </h2>
        <ButtonGroup
          label="Players avatar theme"
          name="avatar"
          description="Everyone will get a different avatar based on their name or their choosing"
          buttons={dicebearButtons}
          onChange={(value) => setIconType(value as IconTypesValues)}
          defaultValue={ICONTYPES.bottts}
          required
        />
        <img
          src={`https://api.dicebear.com/7.x/${iconType()}/png?seed=default`}
          alt="players avatar"
          onLoad={() => setImageLoaded(true)}
          class="mx-auto max-w-[5rem] md:max-w-[8rem]"
          classList={{
            "rounded-md shadow-secondary/40 outline outline-2 outline-offset-4 outline-secondary/50 [box-shadow:_0_0_8px_8px_var(--tw-shadow-color)]":
              imageLoaded(),
          }}
        />
        <Checkbox name="notification" label="Notify players when someone won" />
      </AnimatedForm>
    </>
  );
}
