import {
  For,
  JSX,
  Match,
  Show,
  Switch,
  createRenderEffect,
  createSignal,
  useContext,
} from "solid-js";
import { FormSubmitEvent } from "../../../models/types";
import { AnimatedForm, StepperFormContext } from "../../createtournament";
import BASESCORINGS from "../../../static/constants/defaultroundscorings";
import {
  AmountSchema,
  LoserAmountSchema,
  MultiplierSchema,
  RoundScoring,
  RoundScoringNameSchema,
  RoundScoringSchema,
  RoundScoringTypes,
} from "../../../models/RoundScoring";
import { Button, Checkbox, Collapsible, Dialog } from "@kobalte/core";
import { ToasterContext } from "../../../app";
import ButtonGroup from "../../../components/buttongroup";
import TextFieldInput from "../../../components/textfield";
import ColorInput from "../../../components/colorinput";
import easyHash from "../../../utils/easyHash";
import STORAGE from "../../../static/constants/storage";
import { z } from "zod";
import {
  isDarkTextBetterContrast,
  rgbaToRgbHex,
} from "../../../utils/luminance";
import TAILWINDCUSTOM from "../../../static/constants/tailwindcustom";
import ToConfirm from "../../../components/confirmdialog";
import ToMultipleConfirm from "../../../components/multiconfirmdialog";
import { ActionButton } from "./sportssetup";
import useToast from "../../../hooks/useToast";
import { ComponentTypes } from "./tournamentsetup";

export default function RoundScoringsSetup(props: ComponentTypes) {
  const toast = useToast();
  const customRoundScorings =
    useContext(StepperFormContext).customRoundScorings;
  const selectedSavedRoundScorings =
    useContext(StepperFormContext).selectedSavedRoundScorings;
  const formReaction = useContext(StepperFormContext).reaction;
  const formValues = useContext(StepperFormContext).values.get(props.formId)!;

  function handleSubmit(e: FormSubmitEvent) {
    e.preventDefault();
    const form = e.currentTarget;
    const formData = new FormData(form);
    const map = new Map(formData);
    if (map.size === 0) {
      toast.error({
        description: "Select at least one round scoring!",
        title: "Rounds",
      });
      return;
    }

    formReaction(formData, props.formId);
  }

  return (
    <>
      <AnimatedForm id={props.formId} handleSubmit={handleSubmit}>
        <section>
          <h2 class="-mt-4 mb-4 border-b border-b-primary-400 pb-1 text-center text-xl font-bold leading-none text-colortext md:mb-10 md:text-3xl">
            Default round scorings
          </h2>
          <div class="grid grid-cols-1 gap-4 md:grid-cols-2 md:gap-8">
            <For each={Object.values(BASESCORINGS)}>
              {(scoring) => (
                <BaseRoundScoringCard
                  scoring={scoring}
                  defaultValue={Boolean(formValues.get(scoring.hash))}
                />
              )}
            </For>
          </div>
        </section>
        <section>
          <h2 class="mb-4 border-b border-b-primary-400 pb-1 text-center text-xl font-bold leading-none text-colortext md:mb-10 md:text-3xl">
            Custom round scorings
          </h2>
          <div class="flex justify-center gap-4 md:gap-8">
            <NewRoundScoring />
            <ManageSavedRoundScorings />
          </div>
          <ul class="mt-4 grid grid-cols-1 gap-4 md:mt-10 md:grid-cols-2 md:gap-8">
            <For each={customRoundScorings}>
              {(roundScoring) => <CustomRoundScoring scoring={roundScoring} />}
            </For>
            <For each={selectedSavedRoundScorings}>
              {(roundScoring) => (
                <CustomRoundScoring scoring={roundScoring} isSaved={true} />
              )}
            </For>
          </ul>
        </section>
      </AnimatedForm>
    </>
  );
}

type BaseRoundScoringCardProps = {
  scoring: RoundScoring;
  defaultValue: boolean;
};
function BaseRoundScoringCard(props: BaseRoundScoringCardProps) {
  const [checked, setChecked] = createSignal(props.defaultValue);

  return (
    <Checkbox.Root
      checked={checked()}
      onChange={setChecked}
      name={props.scoring.hash}
      value="true"
    >
      <Checkbox.Input class="peer" />
      <Checkbox.Label
        class="block cursor-pointer overflow-hidden rounded-b-md border border-primary-600/40 outline outline-2 outline-offset-4 outline-primary-400/0 transition-[outline-color,border-color,box-shadow,outline-offset] duration-100 hover:outline-primary-300/80 peer-focus-visible:outline-primary-300/80"
        classList={{
          "!border-primary-800 !outline-offset-0 hover:!outline-primary-400":
            checked(),
        }}
      >
        <div>
          <h3
            class="relative overflow-hidden bg-primary-600/40 px-[1em] py-1 text-center text-lg font-bold leading-none"
            classList={{ "!bg-primary-600/60": checked() }}
          >
            <svg
              fill="none"
              stroke-width="2"
              xmlns="http://www.w3.org/2000/svg"
              width="1.5em"
              height="1.5em"
              viewBox="0 0 24 24"
              stroke="currentColor"
              stroke-linecap="round"
              stroke-linejoin="round"
              class="absolute left-0 top-0 -rotate-45 scale-125 opacity-75"
            >
              <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
              <path d="M3 5m0 2a2 2 0 0 1 2 -2h14a2 2 0 0 1 2 2v10a2 2 0 0 1 -2 2h-14a2 2 0 0 1 -2 -2z"></path>
              <path d="M12 5v2"></path>
              <path d="M12 10v1"></path>
              <path d="M12 14v1"></path>
              <path d="M12 18v1"></path>
              <path d="M7 3v2"></path>
              <path d="M17 3v2"></path>
              <path d="M15 10.5v3a1.5 1.5 0 0 0 3 0v-3a1.5 1.5 0 0 0 -3 0z"></path>
              <path d="M6 9h1.5a1.5 1.5 0 0 1 0 3h-.5h.5a1.5 1.5 0 0 1 0 3h-1.5"></path>
            </svg>
            {props.scoring.name}
          </h3>
          <div class="px-2 pt-2">
            <h4 class="mb-1 block bg-primary-400/25 text-center">
              Winner earns
            </h4>
            <ul>
              <Switch>
                <Match when={props.scoring.winner.type === "FIX"}>
                  <li class="flex flex-col py-1">
                    <span class="block bg-primary-400/25 text-center">
                      Fix amount
                    </span>
                    <span class="block text-center">
                      {/* @ts-ignore */}
                      {props.scoring.winner.amount}
                    </span>
                  </li>
                </Match>
                <Match when={props.scoring.winner.type === "ROUNDSWON"}>
                  <li class="grid grid-cols-2 py-1">
                    <span class="mr-1 block bg-primary-400/25 text-center">
                      Amount
                    </span>
                    <span class="ml-1 block bg-primary-400/25 text-center">
                      Multiplier
                    </span>
                    <span class="block text-center">Rounds won</span>
                    <span class="block text-center">
                      {/* @ts-ignore */}
                      {props.scoring.winner.multiplier ?? "1"}
                    </span>
                  </li>
                </Match>
                <Match when={props.scoring.winner.type === "SUMOFDIFFERENCES"}>
                  <li class="flex py-1">
                    <div class="flex-1">
                      <span class="mr-1 block bg-primary-400/25 text-center">
                        Amount
                      </span>
                      <span class="block text-center">
                        Sum of rounds difference
                      </span>
                    </div>
                    <div>
                      <span class="ml-1 block bg-primary-400/25 px-1 text-center">
                        Multiplier
                      </span>
                      <span class="block text-center">
                        {/* @ts-ignore */}
                        {props.scoring.winner.multiplier ?? "1"}
                      </span>
                    </div>
                  </li>
                </Match>
              </Switch>
            </ul>
            <h4 class="mb-1 block bg-primary-400/25 text-center">
              Loser earns
            </h4>
            <Switch>
              <Match when={props.scoring.loser.type === "FIX"}>
                <li class="flex flex-col py-1">
                  <span class="block bg-primary-400/25 text-center">
                    Fix amount
                  </span>
                  <span class="block text-center">
                    {/* @ts-ignore */}
                    {props.scoring.loser.amount}
                  </span>
                </li>
              </Match>
              <Match when={props.scoring.loser.type === "ROUNDSWON"}>
                <li class="grid grid-cols-2 py-1">
                  <span class="mr-1 block bg-primary-400/25 text-center">
                    Amount
                  </span>
                  <span class="ml-1 block bg-primary-400/25 text-center">
                    Multiplier
                  </span>
                  <span class="block text-center">Rounds won</span>
                  <span class="block text-center">
                    {/* @ts-ignore */}
                    {props.scoring.loser.multiplier ?? "1"}
                  </span>
                </li>
              </Match>
            </Switch>
          </div>
        </div>
      </Checkbox.Label>
    </Checkbox.Root>
  );
}

function NewRoundScoring() {
  const toast = useContext(ToasterContext);
  const customRoundScorings =
    useContext(StepperFormContext).customRoundScorings;
  const [modalOpen, setModalOpen] = createSignal(false);
  const [winnerScoringType, setWinnerScoringType] = createSignal("");
  const [loserScoringType, setLoserScoringType] = createSignal("");

  function handleSubmit(e: FormSubmitEvent) {
    e.preventDefault();
    const form = e.currentTarget;
    const formMap = new Map(new FormData(form).entries()) as Map<
      string,
      string | object
    >;

    const formKeys = Array.from(formMap.keys());
    const winnerKeys = formKeys.filter((str) => str.startsWith("winner"));
    const loserKeys = formKeys.filter((str) => str.startsWith("loser"));

    formMap.set("winner", {});
    for (const key of winnerKeys) {
      const value = formMap.get(key);
      const strip = key.split("winner")[1];
      const obj = formMap.get("winner") as any;
      obj[strip] = value;

      formMap.set("winner", obj);
      formMap.delete(key);
    }

    formMap.set("loser", {});
    for (const key of loserKeys) {
      const value = formMap.get(key);
      const strip = key.split("loser")[1];
      const obj = formMap.get("loser") as any;
      obj[strip] = value;

      formMap.set("loser", obj);
      formMap.delete(key);
    }

    const roundScoring = Object.fromEntries(formMap.entries()) as any;
    roundScoring.hash = easyHash(JSON.stringify(roundScoring));

    const res = RoundScoringSchema.safeParse(roundScoring);
    if (!res.success) {
      toast.error({ title: "Sorry! Try refilling the form!" });
      return;
    }

    if (
      customRoundScorings.findIndex((crs) => crs.hash === res.data.hash) !== -1
    ) {
      toast.error({ title: "This round scoring is already selected!" });
      return;
    }

    const saveRoundScorings = getSavedRoundScorings();
    if (
      saveRoundScorings &&
      saveRoundScorings.findIndex((crs) => crs.hash === res.data.hash) !== -1
    ) {
      toast.error({ title: "This round scoring is already saved!" });
      return;
    }

    customRoundScorings.push(roundScoring);
    setModalOpen(false);
    setWinnerScoringType("");
    setLoserScoringType("");
  }

  const LoserScoringsButtons = [
    {
      label: "Fix amount",
      value: RoundScoringTypes.enum.FIX,
    },
    {
      label: "Rounds won",
      value: RoundScoringTypes.enum.ROUNDSWON,
    },
  ];
  const WinnerScoringsButtons = [
    {
      label: "Fix amount",
      value: RoundScoringTypes.enum.FIX,
    },
    {
      label: "Sum of rounds difference",
      value: RoundScoringTypes.enum.SUMOFDIFFERENCES,
    },
    {
      label: "Rounds won",
      value: RoundScoringTypes.enum.ROUNDSWON,
    },
  ];
  return (
    <>
      <Button.Root
        onClick={() => setModalOpen(true)}
        class="rounded-sm border border-primary-400 bg-primary-400/25 px-[0.75rem] py-[0.375rem] text-colortext transition-transform duration-100 hover:-translate-y-1 active:!translate-y-0 active:bg-primary-400/50"
      >
        Create custom round scoring
      </Button.Root>

      <Dialog.Root open={modalOpen()}>
        <Dialog.Portal>
          <Dialog.Overlay
            onClick={() => setModalOpen(false)}
            class="fixed inset-0 z-50 grid place-items-center overflow-auto p-2 backdrop-blur-sm ui-expanded:animate-modaloverlayenter ui-closed:animate-modaloverlayexit"
          >
            <Dialog.Content
              onClick={(e: MouseEvent) => e.stopPropagation()}
              class="relative w-full max-w-[715px] rounded-sm border border-primary-200 bg-background p-2 shadow-xl shadow-black md:p-4"
            >
              <Dialog.Title class="flex-1 px-4 text-center text-2xl font-bold leading-none md:text-3xl">
                Create a{" "}
                <b class="bg-gradient-to-tr from-primary-500 to-accent-300 bg-clip-text text-colortext/0">
                  new
                </b>{" "}
                round scoring!
              </Dialog.Title>
              <Dialog.CloseButton
                onClick={() => setModalOpen(false)}
                class="absolute right-2 top-0 flex items-center text-3xl leading-none outline-none transition-[transform,color] duration-100 hover:scale-125 hover:text-primary-200 focus-visible:scale-125 focus-visible:text-primary-200"
              >
                {"\u26cc"}
              </Dialog.CloseButton>
              <form
                class="flex flex-col gap-6 py-8 md:gap-10 md:py-8"
                onSubmit={handleSubmit}
              >
                <TextFieldInput
                  name="name"
                  label="Name"
                  schema={RoundScoringNameSchema}
                  required
                />
                <h3 class="-mb-3 text-center text-lg font-bold md:-mb-4 md:text-xl">
                  Round Winner Earns
                </h3>
                <ButtonGroup
                  label=""
                  name="winnertype"
                  buttons={WinnerScoringsButtons}
                  onChange={(value) => setWinnerScoringType(value)}
                  required
                />
                <Switch>
                  <Match
                    when={winnerScoringType() === RoundScoringTypes.enum.FIX}
                  >
                    <TextFieldInput
                      name="winneramount"
                      label="Amount"
                      schema={AmountSchema}
                      required
                    />
                  </Match>
                  <Match
                    when={
                      winnerScoringType() ===
                        RoundScoringTypes.enum.SUMOFDIFFERENCES ||
                      winnerScoringType() === RoundScoringTypes.enum.ROUNDSWON
                    }
                  >
                    <TextFieldInput
                      name="winnermultiplier"
                      label="Multiplier"
                      schema={MultiplierSchema}
                      required
                    />
                  </Match>
                </Switch>
                <h3 class="-mb-3 text-center text-lg font-bold md:-mb-4 md:text-xl">
                  Round Loser Earns
                </h3>
                <ButtonGroup
                  label=""
                  name="losertype"
                  buttons={LoserScoringsButtons}
                  onChange={(value) => setLoserScoringType(value)}
                  required
                />
                <Switch>
                  <Match
                    when={loserScoringType() === RoundScoringTypes.enum.FIX}
                  >
                    <TextFieldInput
                      name="loseramount"
                      label="Amount"
                      schema={LoserAmountSchema}
                      required
                    />
                  </Match>
                  <Match
                    when={
                      loserScoringType() === RoundScoringTypes.enum.ROUNDSWON
                    }
                  >
                    <TextFieldInput
                      name="losermultiplier"
                      label="Multiplier"
                      schema={MultiplierSchema}
                      required
                    />
                  </Match>
                </Switch>
                <ColorInput />
                <Button.Root
                  type="submit"
                  class="group relative isolate mt-6 w-full overflow-hidden rounded-sm border-2 border-primary-600 p-1 md:mt-10"
                >
                  <span class="pointer-events-none absolute -left-[110%] top-1/2 -z-10 block aspect-square w-full -translate-y-1/2 -rotate-45 bg-primary-500/25 transition-[transform,_background-color] group-hover:translate-x-full group-active:bg-primary-500/50" />
                  <span class="mx-auto block w-max rounded-md bg-background px-2 py-1">
                    Create Round Scoring!
                  </span>
                </Button.Root>
              </form>
            </Dialog.Content>
          </Dialog.Overlay>
        </Dialog.Portal>
      </Dialog.Root>
    </>
  );
}

function getSavedRoundScorings() {
  const customRoundScorings = window.localStorage.getItem(
    STORAGE.customRoundScorings,
  );
  if (!customRoundScorings) return false;
  try {
    const parsed = JSON.parse(customRoundScorings);
    const isSportsArraySchema = z.array(RoundScoringSchema);

    const result = isSportsArraySchema.safeParse(parsed);
    if (!result.success) return false;
    return parsed as RoundScoring[];
  } catch {
    return false;
  }
}

function setStorageRoundScorings(roundScorings: RoundScoring[]) {
  try {
    const stringified = JSON.stringify(roundScorings);
    window.localStorage.setItem(STORAGE.customRoundScorings, stringified);

    return true;
  } catch {
    console.error("could not parse");
    return false;
  }
}

type CustomRoundScoringProps = {
  scoring: RoundScoring;
  isSaved?: boolean;
};
function CustomRoundScoring(props: CustomRoundScoringProps) {
  const customRoundScorings =
    useContext(StepperFormContext).customRoundScorings;
  const selectedSavedRoundScorings =
    useContext(StepperFormContext).selectedSavedRoundScorings;
  const pTextColor = isDarkTextBetterContrast(props.scoring.customColor!)
    ? "#111"
    : "#eee";
  const pColor = props.scoring.customColor;

  return (
    <Checkbox.Root checked={true} name={props.scoring.hash} value="true">
      <Checkbox.Input class="peer" />
      <Checkbox.Label
        class="block cursor-default overflow-hidden rounded-b-md border border-custom outline outline-2 outline-offset-4 outline-primary-400/0 transition-[outline-color] duration-100 peer-focus-visible:outline-primary-300/80"
        style={{ [TAILWINDCUSTOM.BORDER]: pColor }}
      >
        <div>
          <h3
            class="relative overflow-hidden bg-custom px-[1em] py-1 text-center text-lg font-bold leading-none text-custom"
            style={{
              [TAILWINDCUSTOM.BG]: pColor,
              [TAILWINDCUSTOM.TEXT]: pTextColor,
            }}
          >
            <svg
              fill="none"
              stroke-width="2"
              xmlns="http://www.w3.org/2000/svg"
              width="1.5em"
              height="1.5em"
              viewBox="0 0 24 24"
              stroke="currentColor"
              stroke-linecap="round"
              stroke-linejoin="round"
              class="absolute left-0 top-0 -rotate-45 scale-125 opacity-75"
            >
              <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
              <path d="M3 5m0 2a2 2 0 0 1 2 -2h14a2 2 0 0 1 2 2v10a2 2 0 0 1 -2 2h-14a2 2 0 0 1 -2 -2z"></path>
              <path d="M12 5v2"></path>
              <path d="M12 10v1"></path>
              <path d="M12 14v1"></path>
              <path d="M12 18v1"></path>
              <path d="M7 3v2"></path>
              <path d="M17 3v2"></path>
              <path d="M15 10.5v3a1.5 1.5 0 0 0 3 0v-3a1.5 1.5 0 0 0 -3 0z"></path>
              <path d="M6 9h1.5a1.5 1.5 0 0 1 0 3h-.5h.5a1.5 1.5 0 0 1 0 3h-1.5"></path>
            </svg>
            {props.scoring.name}
          </h3>
          <RoundScoringInfos scoring={props.scoring} />
          <Button.Root
            class="mx-auto mt-4 block rounded-t-md border border-b-0 border-custom px-2 text-center transition-colors hover:text-warning-300"
            style={{
              [TAILWINDCUSTOM.BORDER]: pColor,
            }}
            onClick={() => {
              if (Boolean(props.isSaved)) {
                selectedSavedRoundScorings.splice(
                  0,
                  selectedSavedRoundScorings.length,
                  ...selectedSavedRoundScorings.filter(
                    (croundscoring) =>
                      croundscoring.hash !== props.scoring.hash,
                  ),
                );
              } else
                ToConfirm({
                  callback(confirmed) {
                    if (confirmed) {
                      customRoundScorings.splice(
                        0,
                        customRoundScorings.length,
                        ...customRoundScorings.filter(
                          (croundscoring) =>
                            croundscoring.hash !== props.scoring.hash,
                        ),
                      );
                    }
                  },
                  title: `Are you sure you want to delete round scoring: ${props.scoring.name}?`,
                });
            }}
          >
            <Show when={Boolean(props.isSaved)} fallback={"Delete"}>
              Remove select
            </Show>
          </Button.Root>
        </div>
      </Checkbox.Label>
    </Checkbox.Root>
  );
}

export function RoundScoringInfos(
  props: Omit<CustomRoundScoringProps, "isSaved">,
) {
  const sColor = props.scoring.customColor + "90";
  const sTextColor = isDarkTextBetterContrast(rgbaToRgbHex(sColor, "#160630"))
    ? "#111"
    : "#eee";

  return (
    <div class="px-2 pt-2">
      <h4
        class="mb-1 block bg-custom text-center text-custom"
        style={{
          [TAILWINDCUSTOM.BG]: sColor,
          [TAILWINDCUSTOM.TEXT]: sTextColor,
        }}
      >
        Winner earns
      </h4>
      <ul>
        <Switch>
          <Match when={props.scoring.winner.type === "FIX"}>
            <li class="flex flex-col py-1">
              <span
                class="block bg-custom text-center text-custom"
                style={{
                  [TAILWINDCUSTOM.BG]: sColor,
                  [TAILWINDCUSTOM.TEXT]: sTextColor,
                }}
              >
                Fix amount
              </span>
              <span class="block text-center">
                {/* @ts-ignore */}
                {props.scoring.winner.amount}
              </span>
            </li>
          </Match>
          <Match when={props.scoring.winner.type === "ROUNDSWON"}>
            <li class="grid grid-cols-2 py-1">
              <span
                class="mr-1 block bg-custom text-center text-custom"
                style={{
                  [TAILWINDCUSTOM.BG]: sColor,
                  [TAILWINDCUSTOM.TEXT]: sTextColor,
                }}
              >
                Amount
              </span>
              <span
                class="ml-1 block bg-custom text-center text-custom"
                style={{
                  [TAILWINDCUSTOM.BG]: sColor,
                  [TAILWINDCUSTOM.TEXT]: sTextColor,
                }}
              >
                Multiplier
              </span>
              <span class="block text-center">Rounds won</span>
              <span class="block text-center">
                {/* @ts-ignore */}
                {props.scoring.winner.multiplier ?? "1"}
              </span>
            </li>
          </Match>
          <Match when={props.scoring.winner.type === "SUMOFDIFFERENCES"}>
            <li class="flex py-1">
              <div class="flex-1">
                <span
                  class="mr-1 block bg-custom text-center text-custom"
                  style={{
                    [TAILWINDCUSTOM.BG]: sColor,
                    [TAILWINDCUSTOM.TEXT]: sTextColor,
                  }}
                >
                  Amount
                </span>
                <span class="block text-center">Sum of rounds difference</span>
              </div>
              <div>
                <span
                  class="ml-1 block bg-custom px-1 text-center text-custom"
                  style={{
                    [TAILWINDCUSTOM.BG]: sColor,
                    [TAILWINDCUSTOM.TEXT]: sTextColor,
                  }}
                >
                  Multiplier
                </span>
                <span class="block text-center">
                  {/* @ts-ignore */}
                  {props.scoring.winner.multiplier ?? "1"}
                </span>
              </div>
            </li>
          </Match>
        </Switch>
      </ul>
      <h4
        class="mb-1 block bg-custom text-center text-custom"
        style={{
          [TAILWINDCUSTOM.BG]: sColor,
          [TAILWINDCUSTOM.TEXT]: sTextColor,
        }}
      >
        Loser earns
      </h4>
      <Switch>
        <Match when={props.scoring.loser.type === "FIX"}>
          <li class="flex flex-col py-1">
            <span
              class="block bg-custom text-center text-custom"
              style={{
                [TAILWINDCUSTOM.BG]: sColor,
                [TAILWINDCUSTOM.TEXT]: sTextColor,
              }}
            >
              Fix amount
            </span>
            <span class="block text-center">
              {/* @ts-ignore */}
              {props.scoring.loser.amount}
            </span>
          </li>
        </Match>
        <Match when={props.scoring.loser.type === "ROUNDSWON"}>
          <li class="grid grid-cols-2 py-1">
            <span
              class="mr-1 block bg-custom text-center text-custom"
              style={{
                [TAILWINDCUSTOM.BG]: sColor,
                [TAILWINDCUSTOM.TEXT]: sTextColor,
              }}
            >
              Amount
            </span>
            <span
              class="ml-1 block bg-custom text-center text-custom"
              style={{
                [TAILWINDCUSTOM.BG]: sColor,
                [TAILWINDCUSTOM.TEXT]: sTextColor,
              }}
            >
              Multiplier
            </span>
            <span class="block text-center">Rounds won</span>
            <span class="block text-center">
              {/* @ts-ignore */}
              {props.scoring.loser.multiplier ?? "1"}
            </span>
          </li>
        </Match>
      </Switch>
    </div>
  );
}

function ManageSavedRoundScorings() {
  const toast = useContext(ToasterContext);
  const customRoundScorings =
    useContext(StepperFormContext).customRoundScorings;
  const selectedSavedRoundScorings =
    useContext(StepperFormContext).selectedSavedRoundScorings;
  const [modalOpen, setModalOpen] = createSignal(false);
  const [storageSavedRoundScorings, setStorageSavedRoundScorings] =
    createSignal<RoundScoring[]>([], {
      equals: false,
    });

  createRenderEffect(() => {
    if (modalOpen()) {
      const savedRoundScorings = getSavedRoundScorings();
      if (savedRoundScorings) setStorageSavedRoundScorings(savedRoundScorings);
    }
  });

  function handleSavedScoringSelect(scoring: RoundScoring) {
    if (
      selectedSavedRoundScorings.findIndex((sc) => sc.hash === scoring.hash) !==
      -1
    ) {
      const index = selectedSavedRoundScorings.findIndex(
        (sc) => sc.hash === scoring.hash,
      );
      selectedSavedRoundScorings.splice(index, 1);
    } else {
      selectedSavedRoundScorings.push(scoring);
    }
  }

  function handleScoringSaves(scoring: RoundScoring) {
    let success = false;
    let temp: RoundScoring[] = [];

    if (
      storageSavedRoundScorings().findIndex(
        (sc) => sc.hash === scoring.hash,
      ) !== -1
    ) {
      temp = storageSavedRoundScorings().filter(
        (sp) => sp.hash !== scoring.hash,
      );

      const index = selectedSavedRoundScorings.findIndex(
        (sc) => sc.hash === scoring.hash,
      );
      selectedSavedRoundScorings.splice(index, 1);
      customRoundScorings.push(scoring);
    } else {
      temp = storageSavedRoundScorings();
      temp.push(scoring);

      const index = customRoundScorings.findIndex(
        (sc) => sc.hash === scoring.hash,
      );
      customRoundScorings.splice(index, 1);
      selectedSavedRoundScorings.push(scoring);
    }

    success = setStorageRoundScorings(temp);
    if (!success) {
      toast.error({ title: "Error during un-/saving sport!" });
      return;
    }
    setStorageSavedRoundScorings(temp);
  }

  const SAVEDDELETION = Object.freeze({
    all: "all",
    saveonly: "saveonly",
    cancel: "cancel",
  });
  function handleSavedDeletion(scoring: RoundScoring) {
    ToMultipleConfirm({
      title: `Are you sure you want to delete: ${scoring.name}`,
      main: {
        label: "Yes, delete it!",
        value: SAVEDDELETION.all,
      },
      additionalButtons: [
        {
          label: "Cancel",
          value: SAVEDDELETION.cancel,
        },
        {
          label: "Only from save",
          value: SAVEDDELETION.saveonly,
        },
      ],
      callback(confirm) {
        if (confirm === SAVEDDELETION.saveonly) {
          handleScoringSaves(scoring);
        }
        if (confirm === SAVEDDELETION.all) {
          const index = selectedSavedRoundScorings.findIndex(
            (sc) => sc.hash === scoring.hash,
          );
          if (index !== -1) selectedSavedRoundScorings.splice(index, 1);

          const filtered = storageSavedRoundScorings().filter(
            (sc) => sc.hash !== scoring.hash,
          );

          setStorageRoundScorings(filtered);
          setStorageSavedRoundScorings(filtered);
        }
      },
    });
  }

  return (
    <>
      <Button.Root
        onClick={() => setModalOpen(true)}
        class="rounded-sm border border-primary-400 bg-primary-400/25 px-[0.75rem] py-[0.375rem] text-colortext transition-transform duration-100 hover:-translate-y-1 active:!translate-y-0 active:bg-primary-400/50"
      >
        Manage saved round scorings
      </Button.Root>

      <Dialog.Root open={modalOpen()}>
        <Dialog.Portal>
          <Dialog.Overlay
            onClick={() => setModalOpen(false)}
            class="fixed inset-0 z-50 grid place-items-center overflow-auto p-2 backdrop-blur-sm ui-expanded:animate-modaloverlayenter ui-closed:animate-modaloverlayexit"
          >
            <Dialog.Content
              onClick={(e: MouseEvent) => e.stopPropagation()}
              class="relative w-full max-w-[1350px] rounded-sm border border-primary-200 bg-background p-2 shadow-xl shadow-black md:p-4"
            >
              <Dialog.Title class="flex-1 px-4 text-center text-2xl font-bold leading-none md:text-3xl">
                Manage your{" "}
                <b class="bg-gradient-to-tr from-primary-500 to-accent-300 bg-clip-text text-colortext/0">
                  saved
                </b>{" "}
                round scorings
              </Dialog.Title>
              <Dialog.CloseButton
                onClick={() => setModalOpen(false)}
                class="absolute right-2 top-0 flex items-center text-3xl leading-none outline-none transition-[transform,color] duration-100 hover:scale-125 hover:text-primary-200 focus-visible:scale-125 focus-visible:text-primary-200"
              >
                {"\u26cc"}
              </Dialog.CloseButton>
              <div class="flex flex-col gap-4 pb-2 pt-4 md:flex-row md:gap-8 md:pb-4 md:pt-8">
                <div class="md:max-w-[33%] md:flex-[1_1_0]">
                  <h3 class="text-center text-lg font-bold !leading-none md:text-xl">
                    Currently selected round scorings
                  </h3>
                  <h4
                    aria-hidden="true"
                    class="invisible mb-4 text-center text-xs !leading-none md:mb-8 md:text-sm"
                  >
                    placeholder
                  </h4>
                  <div class="mb-4 grid gap-4 empty:hidden md:mb-8 md:gap-8">
                    <For each={customRoundScorings}>
                      {(cScoring) => (
                        <CustomSavedRoundScoring
                          scoring={cScoring}
                          handleScoringSaves={handleScoringSaves}
                          saved={
                            storageSavedRoundScorings().findIndex(
                              (sc) => sc.hash === cScoring.hash,
                            ) !== -1
                          }
                          icon={
                            <svg
                              fill="currentColor"
                              stroke-width="0"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 24 24"
                              height="1.5em"
                              width="1.5em"
                              class="transition-transform group-hover:scale-110"
                            >
                              <path d="M5 21h14a2 2 0 0 0 2-2V8a1 1 0 0 0-.29-.71l-4-4A1 1 0 0 0 16 3H5a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2zm10-2H9v-5h6zM13 7h-2V5h2zM5 5h2v4h8V5h.59L19 8.41V19h-2v-5a2 2 0 0 0-2-2H9a2 2 0 0 0-2 2v5H5z"></path>
                            </svg>
                          }
                        />
                      )}
                    </For>
                  </div>
                  <For each={selectedSavedRoundScorings}>
                    {(sRoundScoring) => (
                      <PhantomRoundScoring scoring={sRoundScoring} />
                    )}
                  </For>
                </div>
                <hr class="block h-auto w-[1px] self-stretch border-0 bg-gradient-to-t from-primary-200/25 via-primary-200/75 to-primary-200/25" />
                <div class="md:flex-[2_1_0]">
                  <h3 class="text-center text-lg font-bold !leading-none md:text-xl">
                    Saved round scorings
                  </h3>
                  <h4 class="mb-4 text-center text-xs !leading-none md:mb-8 md:text-sm">
                    These round scorings are only stored locally! Make sure your
                    browser does not delete cookies and files for this site!
                  </h4>
                  <div class="grid gap-4 md:grid-cols-2">
                    <For each={storageSavedRoundScorings()}>
                      {(sScoring) => (
                        <CustomSavedRoundScoring
                          scoring={sScoring}
                          saved={
                            selectedSavedRoundScorings.findIndex(
                              (sc) => sc.hash === sScoring.hash,
                            ) !== -1
                          }
                          handleScoringSaves={handleSavedScoringSelect}
                          icon={
                            <svg
                              fill="currentColor"
                              stroke-width="0"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 16 16"
                              height="1.5em"
                              width="1.5em"
                              class="transition-transform group-hover:scale-125"
                            >
                              <path
                                fill-rule="evenodd"
                                d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"
                              ></path>
                            </svg>
                          }
                          secondaryAction={{
                            icon: (
                              <svg
                                fill="currentColor"
                                stroke-width="0"
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 16 16"
                                height="1.5em"
                                width="1.5em"
                                class="scale-75 transition-transform group-hover:scale-90"
                              >
                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"></path>
                                <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"></path>
                              </svg>
                            ),
                            callback(scoring) {
                              handleSavedDeletion(scoring);
                            },
                          }}
                        />
                      )}
                    </For>
                  </div>
                </div>
              </div>
            </Dialog.Content>
          </Dialog.Overlay>
        </Dialog.Portal>
      </Dialog.Root>
    </>
  );
}

function PhantomRoundScoring(props: Omit<CustomRoundScoringProps, "isSaved">) {
  const pTextColor = isDarkTextBetterContrast(props.scoring.customColor!)
    ? "#111"
    : "#eee";
  const pColor = props.scoring.customColor;

  return (
    <div
      class="mb-4 block cursor-default overflow-hidden rounded-t-md border border-custom transition-opacity last-of-type:mb-0 hover:opacity-80 ui-expanded:opacity-80 md:mb-8"
      style={{
        [TAILWINDCUSTOM.BORDER]: pColor,
      }}
    >
      <div
        class="flex w-full bg-custom py-1 text-custom"
        style={{
          [TAILWINDCUSTOM.BG]: pColor,
          [TAILWINDCUSTOM.TEXT]: pTextColor,
        }}
      >
        <h3 class="relative flex-1 px-[1em] text-center text-lg font-bold !leading-none">
          <svg
            fill="none"
            stroke-width="2"
            xmlns="http://www.w3.org/2000/svg"
            width="1.5em"
            height="1.5em"
            viewBox="0 0 24 24"
            stroke="currentColor"
            stroke-linecap="round"
            stroke-linejoin="round"
            class="absolute left-0 top-0 -rotate-45 scale-125 opacity-75"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M3 5m0 2a2 2 0 0 1 2 -2h14a2 2 0 0 1 2 2v10a2 2 0 0 1 -2 2h-14a2 2 0 0 1 -2 -2z"></path>
            <path d="M12 5v2"></path>
            <path d="M12 10v1"></path>
            <path d="M12 14v1"></path>
            <path d="M12 18v1"></path>
            <path d="M7 3v2"></path>
            <path d="M17 3v2"></path>
            <path d="M15 10.5v3a1.5 1.5 0 0 0 3 0v-3a1.5 1.5 0 0 0 -3 0z"></path>
            <path d="M6 9h1.5a1.5 1.5 0 0 1 0 3h-.5h.5a1.5 1.5 0 0 1 0 3h-1.5"></path>
          </svg>
          {props.scoring.name}
        </h3>
      </div>
    </div>
  );
}

type CustomSavedRoundScoringProps = CustomRoundScoringProps & {
  scoring: RoundScoring;
  handleScoringSaves: (scoring: RoundScoring) => void;
  saved: boolean;
  icon: JSX.Element;
  secondaryAction?: {
    callback: (scoring: RoundScoring) => void;
    icon: JSX.Element;
  };
};
function CustomSavedRoundScoring(props: CustomSavedRoundScoringProps) {
  const [open, setOpen] = createSignal(false);
  const pTextColor = isDarkTextBetterContrast(props.scoring.customColor!)
    ? "#111"
    : "#eee";
  const pColor = props.scoring.customColor;

  return (
    <Collapsible.Root
      class="block cursor-default self-baseline overflow-hidden rounded-t-md border border-custom transition-opacity hover:opacity-100"
      style={{ [TAILWINDCUSTOM.BORDER]: pColor }}
      classList={{ "opacity-90": !open() }}
      onOpenChange={(open) => setOpen(open)}
    >
      <div
        class="flex w-full bg-custom text-custom"
        style={{
          [TAILWINDCUSTOM.BG]: pColor,
          [TAILWINDCUSTOM.TEXT]: pTextColor,
        }}
      >
        <Collapsible.Trigger class="flex flex-grow items-center">
          <svg
            fill="currentColor"
            stroke-width="0"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 16 16"
            height="1em"
            width="1em"
            class="ml-1 transition-transform duration-[250ms]"
            classList={{ "-rotate-90": open() }}
          >
            <path d="m3.86 8.753 5.482 4.796c.646.566 1.658.106 1.658-.753V3.204a1 1 0 0 0-1.659-.753l-5.48 4.796a1 1 0 0 0 0 1.506z"></path>
          </svg>
          <h3 class="flex-1 break-all py-1 text-center text-lg font-bold leading-none">
            {props.scoring.name}
          </h3>
          <Show when={props.secondaryAction}>
            <ActionButton
              open={open}
              primaryColor={pColor!}
              primaryTextColor={pTextColor}
              icon={props.secondaryAction?.icon}
              toggled={false}
              callback={() => props.secondaryAction?.callback(props.scoring)}
            />
          </Show>
          <ActionButton
            open={open}
            primaryColor={pColor!}
            primaryTextColor={pTextColor}
            toggled={props.saved}
            callback={() => props.handleScoringSaves(props.scoring)}
            icon={props.icon}
          />
        </Collapsible.Trigger>
      </div>
      <Collapsible.Content class="ui-expanded:animate-collapsibleOpen ui-closed:animate-collapsibleClose">
        <RoundScoringInfos scoring={props.scoring} />
      </Collapsible.Content>
    </Collapsible.Root>
  );
}
