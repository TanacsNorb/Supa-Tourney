import {
  Accessor,
  For,
  JSX,
  Setter,
  Show,
  createEffect,
  createRenderEffect,
  createSignal,
  createUniqueId,
  useContext,
} from "solid-js";
import {
  AnimatedForm,
  StepperFormContext,
  validateForm,
} from "../../createtournament";
import {
  Button,
  Checkbox,
  Collapsible,
  Dialog,
  ToggleButton,
} from "@kobalte/core";
import {
  DefaultSport,
  ScoringRule,
  Sport,
  SportSchemaName,
  SportSchemaRounds,
  SportSchemaScoreGoal,
  SportSchemaScoreStart,
  SportSchemaScoresPerTurn,
  UniqueSport,
  UniqueSportSchema,
} from "../../../models/Sport";
import BASESPORTS from "../../../static/constants/defaultsports";
import TextFieldInput from "../../../components/textfield";
import ButtonGroup from "../../../components/buttongroup";
import { z } from "zod";
import {
  isDarkTextBetterContrast,
  rgbaToRgbHex,
} from "../../../utils/luminance";
import useToast from "../../../hooks/useToast";
import easyHash from "../../../utils/easyHash";
import { ToasterContext } from "../../../app";
import ToConfirm from "../../../components/confirmdialog";
import STORAGE from "../../../static/constants/storage";
import TAILWINDCUSTOM from "../../../static/constants/tailwindcustom";
import ToMultipleConfirm from "../../../components/multiconfirmdialog";
import { FormSubmitEvent } from "../../../models/types";
import ColorInput from "../../../components/colorinput";
import { ComponentTypes } from "./tournamentsetup";
import canHover from "../../../utils/canhover";

export default function SportsSetupSubpage(props: ComponentTypes) {
  const customSports = useContext(StepperFormContext).customSports;
  const selectedSavedSports =
    useContext(StepperFormContext).selectedSavedSports;
  const formReaction = useContext(StepperFormContext).reaction;
  const formValues = useContext(StepperFormContext).values.get(props.formId)!;
  const toast = useToast();

  function handleSubmit(e: FormSubmitEvent) {
    e.preventDefault();
    const form = e.currentTarget;
    const formData = new FormData(form);
    const map = new Map(formData);
    if (map.size === 0) {
      toast.error({
        description: "Select at least one sport!",
        title: "Sports",
      });
      return;
    }

    formReaction(formData, props.formId);
  }

  return (
    <>
      <AnimatedForm id={props.formId} handleSubmit={handleSubmit}>
        <section>
          <h2 class="-mt-4 mb-4 border-b border-b-primary-400 pb-1 text-center text-xl font-bold leading-none text-colortext md:mb-10 md:text-3xl">
            Default sports
          </h2>
          <div class="grid grid-cols-1 gap-4 md:grid-cols-2 md:gap-8">
            <DefaultSportCheckbox
              name={DefaultSport.enum.Darts}
              sport={BASESPORTS.darts}
              defaultValue={Boolean(formValues.get(DefaultSport.enum.Darts))}
            />
            <DefaultSportCheckbox
              name={DefaultSport.enum.Beerpong}
              sport={BASESPORTS.beerpong}
              defaultValue={Boolean(formValues.get(DefaultSport.enum.Beerpong))}
            />
            <DefaultSportCheckbox
              name={DefaultSport.enum.Tablefootball}
              sport={BASESPORTS.tablefootball}
              defaultValue={Boolean(
                formValues.get(DefaultSport.enum.Tablefootball),
              )}
            />
            <DefaultSportCheckbox
              name={DefaultSport.enum.Snooker}
              sport={BASESPORTS.snooker}
              defaultValue={Boolean(formValues.get(DefaultSport.enum.Snooker))}
            />
          </div>
        </section>
        <section>
          <h2 class="mb-4 border-b border-b-primary-400 pb-1 text-center text-xl font-bold leading-none text-colortext md:mb-10 md:text-3xl">
            Custom sports
          </h2>
          <div class="flex justify-center gap-4 md:gap-8">
            <NewSport />
            <ManageCustomSports />
          </div>
          <ul class="mt-4 grid grid-cols-1 gap-4 md:mt-10 md:grid-cols-2 md:gap-8">
            <For each={customSports}>
              {(sport) => <CustomSportCheckbox sport={sport} />}
            </For>
            <For each={selectedSavedSports}>
              {(sport) => <CustomSportCheckbox sport={sport} isSaved={true} />}
            </For>
          </ul>
        </section>
      </AnimatedForm>
    </>
  );
}

function NewSport() {
  const customSports = useContext(StepperFormContext).customSports;
  const toast = useContext(ToasterContext);
  const formId = createUniqueId();
  const [modalOpen, setModalOpen] = createSignal(false);
  const [hasEndingScore, setHasEndingScore] = createSignal(false);
  const [scoringRule, setScoringRule] = createSignal("");
  const [commonInputs, setCommonInputs] = createSignal<number[]>([], {
    equals: false,
  });

  createEffect(() => {
    if (!modalOpen()) {
      setCommonInputs([]);
      setScoringRule("");
      setHasEndingScore(false);
    }
  });

  const RoundsPlayedButtons = [
    {
      label: "Fixed",
      value: "true",
    },
    {
      label: "Best of",
      value: "false",
    },
  ];
  const ScoringRuleButtons = [
    {
      label: ScoringRule.enum.Once,
      value: ScoringRule.enum.Once,
    },
    {
      label: ScoringRule.enum.Multiple,
      value: ScoringRule.enum.Multiple,
    },
    {
      label: ScoringRule.enum.Simultaneous,
      value: ScoringRule.enum.Simultaneous,
    },
    {
      label: ScoringRule.enum.Undetermined,
      value: ScoringRule.enum.Undetermined,
    },
  ];

  function handleSubmit(e: SubmitEvent & { currentTarget: HTMLFormElement }) {
    e.preventDefault();
    const form = e.currentTarget;
    const valid = validateForm(form);
    if (!valid) {
      toast.error({ title: "New sport", description: "Form is invalid!" });
    }

    const formData = new FormData(form);
    const sportObj = Object.fromEntries(formData.entries()) as any;
    sportObj.hash = easyHash(JSON.stringify(sportObj));
    sportObj.commonInputs = commonInputs();

    const res = UniqueSportSchema.safeParse(sportObj);
    if (!res.success) {
      toast.error({ title: "Sorry! Try refilling the form!" });
      return;
    }
    if (customSports.findIndex((sp) => sp.hash === res.data.hash) !== -1) {
      toast.error({ title: "This sport is already selected!" });
      return;
    }
    const savedCustomSports = getCustomSports();
    if (
      savedCustomSports &&
      savedCustomSports.findIndex((sp) => sp.hash === res.data.hash) !== -1
    ) {
      toast.error({ title: "This sport is already saved!" });
      return;
    }
    customSports.push(res.data);
    setModalOpen(false);
  }

  return (
    <>
      <Button.Root
        onClick={() => setModalOpen(true)}
        class="rounded-sm border border-primary-400 bg-primary-400/25 px-[0.75rem] py-[0.375rem] text-colortext transition-transform duration-100 hover:-translate-y-1 active:!translate-y-0 active:bg-primary-400/50"
      >
        Create custom sport
      </Button.Root>

      <Dialog.Root open={modalOpen()}>
        <Dialog.Portal>
          <Dialog.Overlay
            onClick={() => setModalOpen(false)}
            class="fixed inset-0 z-50 grid place-items-center overflow-auto p-2 backdrop-blur-sm ui-expanded:animate-modaloverlayenter ui-closed:animate-modaloverlayexit"
          >
            <Dialog.Content
              onClick={(e: MouseEvent) => e.stopPropagation()}
              class="relative w-full max-w-[715px] rounded-sm border border-primary-200 bg-background p-2 shadow-xl shadow-black md:p-4"
            >
              <Dialog.Title class="flex-1 px-4 text-center text-2xl font-bold leading-none md:text-3xl">
                Create a{" "}
                <b class="bg-gradient-to-tr from-primary-500 to-accent-300 bg-clip-text text-colortext/0">
                  new
                </b>{" "}
                sport
              </Dialog.Title>
              <Dialog.CloseButton
                onClick={() => setModalOpen(false)}
                class="absolute right-2 top-0 flex items-center text-3xl leading-none outline-none transition-[transform,color] duration-100 hover:scale-125 hover:text-primary-200 focus-visible:scale-125 focus-visible:text-primary-200"
              >
                {"\u26cc"}
              </Dialog.CloseButton>
              <form
                id={formId}
                class="flex flex-col gap-6 py-8 md:gap-10 md:py-8"
                onSubmit={handleSubmit}
              >
                <TextFieldInput
                  name="name"
                  label="Sport name"
                  schema={SportSchemaName}
                  required
                />
                <div class="flex gap-6 md:gap-10">
                  <TextFieldInput
                    name="rounds"
                    label="Rounds count"
                    schema={SportSchemaRounds}
                    required
                    inputMode="numeric"
                    description="Ex. a leg in darts"
                  />
                  <TextFieldInput
                    name="scoreStart"
                    label="Starting score"
                    schema={SportSchemaScoreStart}
                    required
                    inputMode="numeric"
                  />
                </div>
                <div class="flex flex-row items-center gap-2 sm:pl-4 md:gap-8 md:pl-8">
                  <div class="flex -translate-y-1 flex-col items-center">
                    <p class="mb-1 text-xs md:text-sm">Has ending score?</p>
                    <ToggleButton.Root
                      pressed={hasEndingScore()}
                      onChange={setHasEndingScore}
                      class="group block rounded-sm border bg-secondary/20 p-2 outline-none active:bg-primary-400/25"
                    >
                      {(state) => (
                        <Show
                          when={state.pressed()}
                          fallback={
                            <svg
                              fill="currentColor"
                              stroke-width="40"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 16 16"
                              height="1.25em"
                              width="1.25em"
                              class="transition-transform duration-100 group-hover:scale-125 group-hover:text-primary-200 group-focus-visible:scale-125 group-focus-visible:text-primary-200"
                            >
                              <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"></path>
                            </svg>
                          }
                        >
                          <svg
                            fill="currentColor"
                            stroke-width="0"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 16 16"
                            height="1.25em"
                            width="1.25em"
                            class="transition-transform duration-100 group-hover:scale-125 group-hover:text-primary-200 group-focus-visible:scale-125 group-focus-visible:text-primary-200"
                          >
                            <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z"></path>
                          </svg>
                        </Show>
                      )}
                    </ToggleButton.Root>
                  </div>
                  <div class="flex-1">
                    <TextFieldInput
                      name="scoreGoal"
                      label="Ending score"
                      schema={SportSchemaScoreGoal}
                      required
                      disabled={!hasEndingScore()}
                      size={1}
                      inputMode="numeric"
                      description="The goal to win a round"
                    />
                  </div>
                </div>
                <ButtonGroup
                  label="How are rounds played?"
                  name="fixedRounds"
                  buttons={RoundsPlayedButtons}
                  required
                />
                <ButtonGroup
                  label="How many times do players score during their turn?"
                  name="scoringRule"
                  buttons={ScoringRuleButtons}
                  onChange={(value) => setScoringRule(value)}
                  required
                />
                <TextFieldInput
                  name="scoresPerTurn"
                  label="How many times*?"
                  schema={SportSchemaScoresPerTurn}
                  required
                  disabled={scoringRule() !== ScoringRule.enum.Multiple}
                  inputMode="numeric"
                  description="*Only active if 'Multiple' is selected"
                />
                <ColorInput />
              </form>
              <CommonInputsForm
                commonInputs={commonInputs}
                setCommonInputs={setCommonInputs}
              />
              <Button.Root
                form={formId}
                type="submit"
                class="group relative isolate mt-6 w-full overflow-hidden rounded-sm border-2 border-primary-600 p-1 md:mt-10"
              >
                <span class="pointer-events-none absolute -left-[110%] top-1/2 -z-10 block aspect-square w-full -translate-y-1/2 -rotate-45 bg-primary-500/25 transition-[transform,_background-color] group-hover:translate-x-full group-active:bg-primary-500/50" />
                <span class="mx-auto block w-max rounded-md bg-background px-2 py-1">
                  Create Sport!
                </span>
              </Button.Root>
            </Dialog.Content>
          </Dialog.Overlay>
        </Dialog.Portal>
      </Dialog.Root>
    </>
  );
}

function isInputElement(element: Element): element is HTMLInputElement {
  return element && element.nodeName === "INPUT";
}
type CommonInputsFormProps = {
  commonInputs: Accessor<number[]>;
  setCommonInputs: Setter<number[]>;
};
function CommonInputsForm({
  commonInputs,
  setCommonInputs,
}: CommonInputsFormProps) {
  function handleSubmit(e: SubmitEvent & { currentTarget: HTMLFormElement }) {
    e.preventDefault();
    const form = e.currentTarget;

    const valid = validateForm(form);
    if (!valid) return;

    const element = form.elements[0];
    if (!isInputElement(element)) return;

    const value = parseInt(element.value);
    if (isNaN(value)) return;

    setCommonInputs((arr) => {
      arr.push(value);
      return arr;
    });
    form.reset();
  }

  function handleRemove(e: MouseEvent & { currentTarget: HTMLButtonElement }) {
    const index = parseInt(e.currentTarget.dataset["index"] ?? "notanumber");
    if (isNaN(index)) return;

    setCommonInputs((arr) => {
      arr.splice(index, 1);
      return arr;
    });
  }

  return (
    <div class="rounded-sm border border-secondary/50 p-4">
      <h3 class="mb-4 text-center text-base leading-none md:text-lg">
        Common scores to quickly access during game!
      </h3>
      <form onSubmit={handleSubmit}>
        <div class="flex items-end gap-4 md:gap-8">
          <div class="flex-1">
            <TextFieldInput
              schema={z.coerce.number({
                invalid_type_error: "Input must be a number!",
                required_error: "Required",
              })}
              label="Value"
              description="Negative points put the player towards starting score, so for example if starting score is 10, ending score is 0, player has 5 points, earning -1 score puts the player at 6."
              inputMode="numeric"
              size={1}
            />
          </div>
          <Button.Root
            class="group mt-[1.375em] self-start rounded-sm border bg-secondary/10 px-8 py-2 outline-none active:bg-primary-400/25"
            type="submit"
          >
            <svg
              fill="currentColor"
              stroke-width="0"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 16 16"
              height="1em"
              width="1em"
              class="scale-125 transition-transform duration-100 group-hover:scale-150 group-hover:text-primary-200 group-focus-visible:scale-150 group-focus-visible:text-primary-200"
            >
              <path
                fill-rule="evenodd"
                d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"
              ></path>
            </svg>
          </Button.Root>
        </div>
        <ul class="mt-4 flex gap-2 empty:p-4 md:gap-4">
          <For each={commonInputs()}>
            {(item, index) => (
              <li>
                <Button.Root
                  data-index={index()}
                  onClick={handleRemove}
                  class="group relative min-w-[5ch] overflow-hidden rounded-sm border border-primary-400 p-1 py-[0.5em] shadow-inner shadow-primary-400 outline-none transition-transform duration-100 active:scale-90"
                >
                  <span
                    class="absolute -top-[1.5em] left-[calc(50%_-_1ch_/_2)] scale-125"
                    classList={{
                      "transition-transform duration-150 group-hover:translate-y-[1.5em] group-focus-visible:translate-y-[1.5em]":
                        canHover(),
                      "translate-y-[1.5em]": !canHover(),
                    }}
                  >
                    {"\u26cc"}
                  </span>
                  <span
                    class="block"
                    classList={{
                      "transition-transform duration-150 group-hover:translate-y-[0.75em] group-focus-visible:translate-y-[0.75em]":
                        canHover(),
                      "translate-y-[0.75em]": !canHover(),
                    }}
                  >
                    {item}
                  </span>
                </Button.Root>
              </li>
            )}
          </For>
        </ul>
      </form>
    </div>
  );
}

type DefaultSportCheckboxProps = {
  name: string;
  sport: Sport;
  defaultValue: boolean;
};
function DefaultSportCheckbox(props: DefaultSportCheckboxProps) {
  const [checked, setChecked] = createSignal(props.defaultValue);

  return (
    <Checkbox.Root
      checked={checked()}
      onChange={setChecked}
      name={props.name}
      value="true"
    >
      <Checkbox.Input class="peer" />
      <Checkbox.Label
        class="block cursor-pointer overflow-hidden rounded-t-md border border-primary-100/90 outline outline-2 outline-offset-4 outline-primary-400/0 transition-[outline-color,border-color,box-shadow,outline-offset] duration-100 hover:outline-primary-300/80 peer-focus-visible:outline-primary-300/80"
        classList={{
          "!border-primary-500 !outline-offset-0 hover:!outline-primary-400":
            checked(),
        }}
      >
        <div>
          <h3 class="relative overflow-hidden bg-primary-100/90 px-[1em] py-1 text-center text-lg font-bold leading-none text-inversetext">
            <svg
              fill="currentColor"
              stroke-width="0"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              height="1.5em"
              width="1.5em"
              class="absolute left-0 top-0 rotate-90 scale-125 opacity-75"
            >
              <path d="M4.929 19.081c1.895 1.895 4.405 2.938 7.071 2.938s5.177-1.043 7.071-2.938c3.899-3.899 3.899-10.243 0-14.143C17.177 3.044 14.665 2 12 2S6.823 3.044 4.929 4.938c-3.899 3.899-3.899 10.244 0 14.143zm12.728-1.414a7.969 7.969 0 0 1-3.813 2.129c-.009-1.602.586-3.146 1.691-4.251 1.163-1.163 2.732-1.828 4.277-1.851a7.945 7.945 0 0 1-2.155 3.973zm2.325-5.965c-2.124-.021-4.284.853-5.861 2.429-1.532 1.532-2.327 3.68-2.263 5.881a7.946 7.946 0 0 1-5.516-2.345 7.97 7.97 0 0 1-2.332-5.512c.077.002.154.014.231.014 2.115 0 4.16-.804 5.637-2.28 1.58-1.58 2.457-3.739 2.43-5.873a7.948 7.948 0 0 1 5.349 2.337 7.96 7.96 0 0 1 2.325 5.349zM6.343 6.353a7.968 7.968 0 0 1 3.973-2.169c-.018 1.555-.685 3.124-1.851 4.291-1.104 1.103-2.642 1.696-4.238 1.691a7.929 7.929 0 0 1 2.116-3.813z"></path>
            </svg>
            {props.sport.name}
          </h3>
          <ul class="px-2 pt-2">
            <li class="flex flex-col py-1">
              <span class="block bg-primary-100/25 text-center">Rounds</span>
              <span class="block text-center">{props.sport.rounds}</span>
            </li>
            <li class="grid grid-cols-2 py-1">
              <span class="mr-1 block bg-primary-100/25 text-center">
                Start score
              </span>
              <span class="ml-1 block bg-primary-100/25 text-center">
                Ending score
              </span>
              <span class="block text-center">{props.sport.scoreStart}</span>
              <span class="block text-center">
                {props.sport.scoreGoal ?? "undetermined"}
              </span>
            </li>
            <li class="flex flex-col py-1">
              <span class="block bg-primary-100/25 text-center">
                Fixed rounds?
              </span>
              <span class="block text-center">
                {props.sport.fixedRounds ? "Yes" : "No"}
              </span>
            </li>
            <li class="grid grid-cols-2 py-1">
              <span
                class="mr-1 block bg-primary-100/25 text-center"
                classList={{ "col-span-2": !props.sport.scoresPerTurn }}
              >
                Scoring rule
              </span>
              <span
                class="ml-1 block bg-primary-100/25 text-center"
                classList={{ hidden: !props.sport.scoresPerTurn }}
              >
                How many times?
              </span>
              <span
                class="block text-center"
                classList={{ "col-span-2": !props.sport.scoresPerTurn }}
              >
                {props.sport.scoringRule}
              </span>
              <span
                class="block text-center"
                classList={{ hidden: !props.sport.scoresPerTurn }}
              >
                {props.sport.scoresPerTurn}
              </span>
            </li>
          </ul>
        </div>
      </Checkbox.Label>
    </Checkbox.Root>
  );
}

type CustomSportCheckboxProps = {
  sport: UniqueSport;
  isSaved?: boolean;
};
function CustomSportCheckbox(props: CustomSportCheckboxProps) {
  const customSports = useContext(StepperFormContext).customSports;
  const selectedSavedSports =
    useContext(StepperFormContext).selectedSavedSports;
  const pTextColor = isDarkTextBetterContrast(props.sport.customColor!)
    ? "#111"
    : "#eee";
  const pColor = props.sport.customColor;

  return (
    <Checkbox.Root checked={true} name={props.sport.hash} value="true" readOnly>
      <Checkbox.Input class="peer" />
      <Checkbox.Label
        class="block cursor-default overflow-hidden rounded-t-md border border-custom"
        style={{ [TAILWINDCUSTOM.BORDER]: pColor }}
      >
        <div>
          <h3
            class="relative overflow-hidden break-words bg-custom px-[1em] py-1 text-center text-lg font-bold leading-none text-custom"
            style={{
              [TAILWINDCUSTOM.BG]: pColor,
              [TAILWINDCUSTOM.TEXT]: pTextColor,
            }}
          >
            <svg
              fill="currentColor"
              stroke-width="0"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              height="1.5em"
              width="1.5em"
              class="absolute left-0 top-0 rotate-90 scale-125 opacity-75"
            >
              <path d="M4.929 19.081c1.895 1.895 4.405 2.938 7.071 2.938s5.177-1.043 7.071-2.938c3.899-3.899 3.899-10.243 0-14.143C17.177 3.044 14.665 2 12 2S6.823 3.044 4.929 4.938c-3.899 3.899-3.899 10.244 0 14.143zm12.728-1.414a7.969 7.969 0 0 1-3.813 2.129c-.009-1.602.586-3.146 1.691-4.251 1.163-1.163 2.732-1.828 4.277-1.851a7.945 7.945 0 0 1-2.155 3.973zm2.325-5.965c-2.124-.021-4.284.853-5.861 2.429-1.532 1.532-2.327 3.68-2.263 5.881a7.946 7.946 0 0 1-5.516-2.345 7.97 7.97 0 0 1-2.332-5.512c.077.002.154.014.231.014 2.115 0 4.16-.804 5.637-2.28 1.58-1.58 2.457-3.739 2.43-5.873a7.948 7.948 0 0 1 5.349 2.337 7.96 7.96 0 0 1 2.325 5.349zM6.343 6.353a7.968 7.968 0 0 1 3.973-2.169c-.018 1.555-.685 3.124-1.851 4.291-1.104 1.103-2.642 1.696-4.238 1.691a7.929 7.929 0 0 1 2.116-3.813z"></path>
            </svg>
            {props.sport.name}
          </h3>
          <SportCardInfos sport={props.sport} />
          <Button.Root
            class="mx-auto mt-4 block rounded-t-md border border-b-0 border-custom px-2 text-center transition-colors hover:text-warning-300"
            style={{
              [TAILWINDCUSTOM.BORDER]: pColor,
            }}
            onClick={() => {
              if (Boolean(props.isSaved)) {
                selectedSavedSports.splice(
                  0,
                  selectedSavedSports.length,
                  ...selectedSavedSports.filter(
                    (csport) => csport.hash !== props.sport.hash,
                  ),
                );
              } else
                ToConfirm({
                  callback(confirmed) {
                    if (confirmed) {
                      customSports.splice(
                        0,
                        customSports.length,
                        ...customSports.filter(
                          (csport) => csport.hash !== props.sport.hash,
                        ),
                      );
                    }
                  },
                  title: `Are you sure you want to delete sport: ${props.sport.name}?`,
                });
            }}
          >
            <Show when={Boolean(props.isSaved)} fallback={"Delete"}>
              Remove select
            </Show>
          </Button.Root>
        </div>
      </Checkbox.Label>
    </Checkbox.Root>
  );
}

function getCustomSports() {
  const customSports = window.localStorage.getItem(STORAGE.customSports);
  if (!customSports) return false;
  try {
    const parsed = JSON.parse(customSports);
    const isSportsArraySchema = z.array(UniqueSportSchema);

    const result = isSportsArraySchema.safeParse(parsed);
    if (!result.success) return false;
    return parsed as UniqueSport[];
  } catch {
    return false;
  }
}

function setStorageCustomSports(sports: UniqueSport[]) {
  try {
    const stringified = JSON.stringify(sports);
    window.localStorage.setItem(STORAGE.customSports, stringified);

    return true;
  } catch {
    console.error("could not parse");
    return false;
  }
}

function ManageCustomSports() {
  const toast = useContext(ToasterContext);
  const customSports = useContext(StepperFormContext).customSports;
  const selectedSavedSports =
    useContext(StepperFormContext).selectedSavedSports;
  const [modalOpen, setModalOpen] = createSignal(false);
  const [storageSavedSports, setStorageSavedSports] = createSignal<
    UniqueSport[]
  >([], {
    equals: false,
  });

  createRenderEffect(() => {
    if (modalOpen()) {
      const savedCustomSports = getCustomSports();
      if (savedCustomSports) setStorageSavedSports(savedCustomSports);
    }
  });

  function handleSportSaves(sport: UniqueSport) {
    let success = false;
    let temp: UniqueSport[] = [];

    if (storageSavedSports().findIndex((sp) => sp.hash === sport.hash) !== -1) {
      temp = storageSavedSports().filter((sp) => sp.hash !== sport.hash);

      const index = selectedSavedSports.findIndex(
        (sp) => sp.hash === sport.hash,
      );
      selectedSavedSports.splice(index, 1);
      customSports.push(sport);
    } else {
      temp = storageSavedSports();
      temp.push(sport);

      const index = customSports.findIndex((sp) => sp.hash === sport.hash);
      customSports.splice(index, 1);
      selectedSavedSports.push(sport);
    }

    success = setStorageCustomSports(temp);
    if (!success) {
      toast.error({ title: "Error during un-/saving sport!" });
      return;
    }
    setStorageSavedSports(temp);
  }

  function handleSavedSportSelect(sport: UniqueSport) {
    if (selectedSavedSports.findIndex((sp) => sp.hash === sport.hash) !== -1) {
      const index = selectedSavedSports.findIndex(
        (sp) => sp.hash === sport.hash,
      );
      selectedSavedSports.splice(index, 1);
    } else {
      selectedSavedSports.push(sport);
    }
  }

  const SAVEDDELETION = Object.freeze({
    all: "all",
    saveonly: "saveonly",
    cancel: "cancel",
  });
  function handleSavedDeletion(sport: UniqueSport) {
    ToMultipleConfirm({
      title: `Are you sure you want to delete: ${sport.name}`,
      main: {
        label: "Yes, delete it!",
        value: SAVEDDELETION.all,
      },
      additionalButtons: [
        {
          label: "Cancel",
          value: SAVEDDELETION.cancel,
        },
        {
          label: "Only from save",
          value: SAVEDDELETION.saveonly,
        },
      ],
      callback(confirm) {
        if (confirm === SAVEDDELETION.saveonly) {
          handleSportSaves(sport);
        }
        if (confirm === SAVEDDELETION.all) {
          const index = selectedSavedSports.findIndex(
            (sp) => sp.hash === sport.hash,
          );
          if (index !== -1) selectedSavedSports.splice(index, 1);

          const filtered = storageSavedSports().filter(
            (sp) => sp.hash !== sport.hash,
          );
          setStorageCustomSports(filtered);
          setStorageSavedSports(filtered);
        }
      },
    });
  }

  return (
    <>
      <Button.Root
        onClick={() => setModalOpen(true)}
        class="rounded-sm border border-primary-400 bg-primary-400/25 px-[0.75rem] py-[0.375rem] text-colortext transition-transform duration-100 hover:-translate-y-1 active:!translate-y-0 active:bg-primary-400/50"
      >
        Manage custom sports
      </Button.Root>

      <Dialog.Root open={modalOpen()}>
        <Dialog.Portal>
          <Dialog.Overlay
            onClick={() => setModalOpen(false)}
            class="fixed inset-0 z-50 grid place-items-center overflow-auto p-2 backdrop-blur-sm ui-expanded:animate-modaloverlayenter ui-closed:animate-modaloverlayexit"
          >
            <Dialog.Content
              onClick={(e: MouseEvent) => e.stopPropagation()}
              class="relative w-full max-w-[1350px] rounded-sm border border-primary-200 bg-background p-2 shadow-xl shadow-black md:p-4"
            >
              <Dialog.Title class="flex-1 px-4 text-center text-2xl font-bold leading-none md:text-3xl">
                Manage your{" "}
                <b class="bg-gradient-to-tr from-primary-500 to-accent-300 bg-clip-text text-colortext/0">
                  saved
                </b>{" "}
                sports
              </Dialog.Title>
              <Dialog.CloseButton
                onClick={() => setModalOpen(false)}
                class="absolute right-2 top-0 flex items-center text-3xl leading-none outline-none transition-[transform,color] duration-100 hover:scale-125 hover:text-primary-200 focus-visible:scale-125 focus-visible:text-primary-200"
              >
                {"\u26cc"}
              </Dialog.CloseButton>
              <div class="flex flex-col gap-4 pb-2 pt-4 md:flex-row md:gap-8 md:pb-4 md:pt-8">
                <div class="md:max-w-[33%] md:flex-[1_1_0]">
                  <h3 class="text-center text-lg font-bold !leading-none md:text-xl">
                    Currently selected sports
                  </h3>
                  <h4
                    aria-hidden="true"
                    class="invisible mb-4 text-center text-xs !leading-none md:mb-8 md:text-sm"
                  >
                    placeholder
                  </h4>
                  <div class="mb-4 grid gap-4 empty:hidden md:mb-8 md:gap-8">
                    <For each={customSports}>
                      {(cSport) => (
                        <CustomSportCard
                          sport={cSport}
                          handleSportSaves={handleSportSaves}
                          saved={
                            storageSavedSports().findIndex(
                              (sp) => sp.hash === cSport.hash,
                            ) !== -1
                          }
                          icon={
                            <svg
                              fill="currentColor"
                              stroke-width="0"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 24 24"
                              height="1.5em"
                              width="1.5em"
                              class="transition-transform group-hover:scale-110"
                            >
                              <path d="M5 21h14a2 2 0 0 0 2-2V8a1 1 0 0 0-.29-.71l-4-4A1 1 0 0 0 16 3H5a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2zm10-2H9v-5h6zM13 7h-2V5h2zM5 5h2v4h8V5h.59L19 8.41V19h-2v-5a2 2 0 0 0-2-2H9a2 2 0 0 0-2 2v5H5z"></path>
                            </svg>
                          }
                        />
                      )}
                    </For>
                  </div>
                  <For each={selectedSavedSports}>
                    {(sSport) => <PhantomSport sport={sSport} />}
                  </For>
                </div>
                <hr class="block h-auto w-[1px] self-stretch border-0 bg-gradient-to-t from-primary-200/25 via-primary-200/75 to-primary-200/25" />
                <div class="md:flex-[2_1_0]">
                  <h3 class="text-center text-lg font-bold !leading-none md:text-xl">
                    Saved sports
                  </h3>
                  <h4 class="mb-4 text-center text-xs !leading-none md:mb-8 md:text-sm">
                    These sports are only stored locally! Make sure your browser
                    does not delete cookies and files for this site!
                  </h4>
                  <div class="grid gap-4 md:grid-cols-2">
                    <For each={storageSavedSports()}>
                      {(sSport) => (
                        <CustomSportCard
                          sport={sSport}
                          saved={
                            selectedSavedSports.findIndex(
                              (sp) => sp.hash === sSport.hash,
                            ) !== -1
                          }
                          handleSportSaves={handleSavedSportSelect}
                          icon={
                            <svg
                              fill="currentColor"
                              stroke-width="0"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 16 16"
                              height="1.5em"
                              width="1.5em"
                              class="transition-transform group-hover:scale-125"
                            >
                              <path
                                fill-rule="evenodd"
                                d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"
                              ></path>
                            </svg>
                          }
                          secondaryAction={{
                            icon: (
                              <svg
                                fill="currentColor"
                                stroke-width="0"
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 16 16"
                                height="1.5em"
                                width="1.5em"
                                class="scale-75 transition-transform group-hover:scale-90"
                              >
                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"></path>
                                <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"></path>
                              </svg>
                            ),
                            callback(sport) {
                              handleSavedDeletion(sport);
                            },
                          }}
                        />
                      )}
                    </For>
                  </div>
                </div>
              </div>
            </Dialog.Content>
          </Dialog.Overlay>
        </Dialog.Portal>
      </Dialog.Root>
    </>
  );
}

type CustomSportCardProps = {
  sport: UniqueSport;
  handleSportSaves: (sport: UniqueSport) => void;
  saved: boolean;
  icon: JSX.Element;
  secondaryAction?: {
    callback: (sport: UniqueSport) => void;
    icon: JSX.Element;
  };
};
function CustomSportCard(props: CustomSportCardProps) {
  const [open, setOpen] = createSignal(false);
  const pTextColor = isDarkTextBetterContrast(props.sport.customColor!)
    ? "#111"
    : "#eee";
  const pColor = props.sport.customColor;

  return (
    <Collapsible.Root
      class="block cursor-default self-baseline overflow-hidden rounded-t-md border border-custom transition-opacity hover:opacity-100"
      style={{ [TAILWINDCUSTOM.BORDER]: pColor }}
      classList={{ "opacity-90": !open() }}
      onOpenChange={(open) => setOpen(open)}
    >
      <div
        class="flex w-full bg-custom text-custom"
        style={{
          [TAILWINDCUSTOM.BG]: pColor,
          [TAILWINDCUSTOM.TEXT]: pTextColor,
        }}
      >
        <Collapsible.Trigger class="flex flex-grow items-center">
          <svg
            fill="currentColor"
            stroke-width="0"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 16 16"
            height="1em"
            width="1em"
            class="ml-1 transition-transform duration-[250ms]"
            classList={{ "-rotate-90": open() }}
          >
            <path d="m3.86 8.753 5.482 4.796c.646.566 1.658.106 1.658-.753V3.204a1 1 0 0 0-1.659-.753l-5.48 4.796a1 1 0 0 0 0 1.506z"></path>
          </svg>
          <h3 class="flex-1 break-all py-1 text-center text-lg font-bold leading-none">
            {props.sport.name}
          </h3>
          <Show when={props.secondaryAction}>
            <ActionButton
              open={open}
              primaryColor={pColor!}
              primaryTextColor={pTextColor}
              icon={props.secondaryAction?.icon}
              toggled={false}
              callback={() => props.secondaryAction?.callback(props.sport)}
            />
          </Show>
          <ActionButton
            open={open}
            primaryColor={pColor!}
            primaryTextColor={pTextColor}
            toggled={props.saved}
            callback={() => props.handleSportSaves(props.sport)}
            icon={props.icon}
          />
        </Collapsible.Trigger>
      </div>
      <Collapsible.Content class="ui-expanded:animate-collapsibleOpen ui-closed:animate-collapsibleClose">
        <SportCardInfos sport={props.sport} />
      </Collapsible.Content>
    </Collapsible.Root>
  );
}

type ActionButtonProps = {
  open: Accessor<boolean>;
  primaryColor: string;
  primaryTextColor: string;
  callback: () => void;
  toggled: boolean;
  icon: JSX.Element;
};
export function ActionButton(props: ActionButtonProps) {
  return (
    <ToggleButton.Root
      class="group relative border-l-2 px-2"
      classList={{ "border-b": props.open() }}
      style={{
        "border-bottom-color": props.primaryColor,
        "border-left-color": props.primaryTextColor,
      }}
      onChange={() => props.callback()}
      pressed={props.toggled}
      onClick={(event) => event.stopImmediatePropagation()}
    >
      {(state) => (
        <Show when={state.pressed()} fallback={props.icon}>
          <svg
            fill="currentColor"
            stroke-width="40"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 16 16"
            height="1.5em"
            width="1.5em"
            class="transition-transform group-hover:scale-110"
          >
            <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"></path>
          </svg>
        </Show>
      )}
    </ToggleButton.Root>
  );
}

type SportCardInfosProps = {
  sport: UniqueSport;
};
export function SportCardInfos(props: SportCardInfosProps) {
  const sColor = props.sport.customColor + "90";
  const sTextColor = isDarkTextBetterContrast(rgbaToRgbHex(sColor, "#160630"))
    ? "#111"
    : "#eee";

  return (
    <ul class="px-2 pt-2">
      <li class="flex flex-col py-1">
        <span
          class="block text-center"
          style={{
            "background-color": sColor,
            color: sTextColor,
          }}
        >
          Rounds
        </span>
        <span class="block text-center">{props.sport.rounds}</span>
      </li>
      <li class="grid grid-cols-2 py-1">
        <span
          class="mr-1 block text-center"
          style={{
            "background-color": sColor,
            color: sTextColor,
          }}
        >
          Start score
        </span>
        <span
          class="ml-1 block text-center"
          style={{
            "background-color": sColor,
            color: sTextColor,
          }}
        >
          Ending score
        </span>
        <span class="block text-center">{props.sport.scoreStart}</span>
        <span class="block text-center">
          {props.sport.scoreGoal ?? "undetermined"}
        </span>
      </li>
      <li class="flex flex-col py-1">
        <span
          class="block text-center"
          style={{
            "background-color": sColor,
            color: sTextColor,
          }}
        >
          Fixed rounds?
        </span>
        <span class="block text-center">
          {props.sport.fixedRounds ? "Yes" : "No"}
        </span>
      </li>
      <li class="grid grid-cols-2 py-1">
        <span
          class="mr-1 block text-center"
          classList={{ "col-span-2": !props.sport.scoresPerTurn }}
          style={{
            "background-color": sColor,
            color: sTextColor,
          }}
        >
          Scoring rule
        </span>
        <span
          class="ml-1 block text-center"
          classList={{ hidden: !props.sport.scoresPerTurn }}
          style={{
            "background-color": sColor,
            color: sTextColor,
          }}
        >
          How many times?
        </span>
        <span
          class="block text-center"
          classList={{ "col-span-2": !props.sport.scoresPerTurn }}
        >
          {props.sport.scoringRule}
        </span>
        <span
          class="block text-center"
          classList={{ hidden: !props.sport.scoresPerTurn }}
        >
          {props.sport.scoresPerTurn}
        </span>
      </li>
      <li class="flex flex-col py-1">
        <span
          class="block text-center"
          style={{
            "background-color": sColor,
            color: sTextColor,
          }}
        >
          Common Inputs
        </span>
        <span class="flex flex-wrap justify-center gap-2">
          <For each={props.sport.commonInputs} fallback="-">
            {(input) => (
              <span class="inline-block min-w-[3ch] border-b text-center">
                {input}
              </span>
            )}
          </For>
        </span>
      </li>
    </ul>
  );
}

function PhantomSport(props: SportCardInfosProps) {
  const pTextColor = isDarkTextBetterContrast(props.sport.customColor!)
    ? "#111"
    : "#eee";
  const pColor = props.sport.customColor;

  return (
    <div
      class="mb-4 block cursor-default overflow-hidden rounded-t-md border border-custom transition-opacity last-of-type:mb-0 hover:opacity-80 ui-expanded:opacity-80 md:mb-8"
      style={{
        [TAILWINDCUSTOM.BORDER]: pColor,
      }}
    >
      <div
        class="flex w-full bg-custom py-1 text-custom"
        style={{
          [TAILWINDCUSTOM.BG]: pColor,
          [TAILWINDCUSTOM.TEXT]: pTextColor,
        }}
      >
        <h3 class="relative flex-1 text-center text-lg font-bold !leading-none">
          <svg
            fill="currentColor"
            stroke-width="0"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            height="1.5em"
            width="1.5em"
            class="absolute -top-1 left-0 rotate-90 opacity-75"
          >
            <path d="M4.929 19.081c1.895 1.895 4.405 2.938 7.071 2.938s5.177-1.043 7.071-2.938c3.899-3.899 3.899-10.243 0-14.143C17.177 3.044 14.665 2 12 2S6.823 3.044 4.929 4.938c-3.899 3.899-3.899 10.244 0 14.143zm12.728-1.414a7.969 7.969 0 0 1-3.813 2.129c-.009-1.602.586-3.146 1.691-4.251 1.163-1.163 2.732-1.828 4.277-1.851a7.945 7.945 0 0 1-2.155 3.973zm2.325-5.965c-2.124-.021-4.284.853-5.861 2.429-1.532 1.532-2.327 3.68-2.263 5.881a7.946 7.946 0 0 1-5.516-2.345 7.97 7.97 0 0 1-2.332-5.512c.077.002.154.014.231.014 2.115 0 4.16-.804 5.637-2.28 1.58-1.58 2.457-3.739 2.43-5.873a7.948 7.948 0 0 1 5.349 2.337 7.96 7.96 0 0 1 2.325 5.349zM6.343 6.353a7.968 7.968 0 0 1 3.973-2.169c-.018 1.555-.685 3.124-1.851 4.291-1.104 1.103-2.642 1.696-4.238 1.691a7.929 7.929 0 0 1 2.116-3.813z"></path>
          </svg>
          {props.sport.name}
        </h3>
      </div>
    </div>
  );
}
