import {
  Accessor,
  For,
  Setter,
  Show,
  createContext,
  createSignal,
  createUniqueId,
  useContext,
} from "solid-js";
import useToast from "../../../hooks/useToast";
import { FormSubmitEvent } from "../../../models/types";
import {
  AnimatedForm,
  StepperFormContext,
  steps,
} from "../../createtournament";
import { useIsSub1000 } from "../../../hooks/useIsPhone";
import {
  DragDropProvider,
  useDragDropContext,
  DragDropSensors,
  createDraggable,
  createDroppable,
  DragEvent,
  transformStyle,
} from "@thisbeyond/solid-dnd";
import { UniqueSport, UniqueSportSchema } from "../../../models/Sport";
import { BaseSportFromName } from "../../../static/constants/defaultsports";
import { BaseRoundScoringFromHash } from "../../../static/constants/defaultroundscorings";
import { RoundScoring, RoundScoringSchema } from "../../../models/RoundScoring";
import { Button, Dialog, Popover } from "@kobalte/core";
import { isDarkTextBetterContrast } from "../../../utils/luminance";
import TAILWINDCUSTOM from "../../../static/constants/tailwindcustom";
import { SportCardInfos } from "./sportssetup";
import { RoundScoringInfos } from "./roundscoringssetup";
import { ComponentTypes } from "./tournamentsetup";
import { Dynamic } from "solid-js/web";

export default function RoundScoringsToSportsSubpage(props: ComponentTypes) {
  const toast = useToast();
  const isSmall = useIsSub1000();

  const tournamentType = useContext(StepperFormContext).tournamentType;
  const assignedRounds = useContext(StepperFormContext).assignedRounds;
  const roundAssigningReaction =
    useContext(StepperFormContext).roundAssigningReaction;
  const stats = useContext(StepperFormContext).values.get(steps[0])!;
  const rounds = () => parseInt(stats.get("roundCount")!);

  const [assignedSports, setAssignedSports] = createSignal<
    (UniqueSport | undefined)[]
  >(assignedRounds.map((pair) => pair[0]).flat(), {
    equals: false,
  });
  const [assignedRoundScorings, setAssignedRoundScorings] = createSignal<
    (RoundScoring | undefined)[]
  >(assignedRounds.map((pair) => pair[1]).flat(), { equals: false });

  const sports = Array.from(useContext(StepperFormContext).customSports).concat(
    useContext(StepperFormContext).selectedSavedSports,
  );
  const scorings = Array.from(
    useContext(StepperFormContext).customRoundScorings,
  ).concat(useContext(StepperFormContext).selectedSavedRoundScorings);

  const selectedDefaultSports = useContext(StepperFormContext).values.get(
    steps[1],
  )!;

  for (const key of Array.from(selectedDefaultSports.keys())) {
    const baseSport = BaseSportFromName(key);
    if (baseSport) sports.push(baseSport);
  }

  const selectedDefaultRoundScorings = useContext(
    StepperFormContext,
  ).values.get(steps[2])!;
  for (const key of Array.from(selectedDefaultRoundScorings.keys())) {
    const baseRoundScoring = BaseRoundScoringFromHash(key);
    if (baseRoundScoring) scorings.push(baseRoundScoring);
  }

  function handleSubmit(e: FormSubmitEvent) {
    e.preventDefault();

    let sportsError: { description: string; title: string } | null = null;
    let roundsError: { description: string; title: string } | null = null;
    if (assignedSports().length < rounds()) {
      sportsError = {
        description: "Not all rounds are filled with sports!",
        title: "Assigning sports",
      };
    }
    for (const sport of assignedSports()) {
      const result = UniqueSportSchema.safeParse(sport);
      if (!result.success && !sportsError) {
        sportsError = {
          description: "Not all rounds are filled with sports!",
          title: "Assigning sports",
        };
      }
    }

    if (tournamentType() === "roundrobin") {
      if (assignedRoundScorings().length < rounds()) {
        roundsError = {
          description: "Not all rounds are filled with scoring!",
          title: "Assigning scorings",
        };
      }
      for (const scoring of assignedRoundScorings()) {
        const result = RoundScoringSchema.safeParse(scoring);
        if (!result.success && !roundsError) {
          roundsError = {
            description: "Not all rounds are filled with scoring!",
            title: "Assigning scorings",
          };
        }
      }
    }

    if (sportsError) toast.error(sportsError);
    if (roundsError) toast.error(roundsError);
    if (sportsError || roundsError) return;

    let data = [];
    for (let i = 0; i < rounds(); i++) {
      data[i] = [assignedSports()[i], assignedRoundScorings()[i]];
    }

    roundAssigningReaction(data as Array<[UniqueSport, RoundScoring]>);
  }

  return (
    <>
      <AnimatedForm id={props.formId} handleSubmit={handleSubmit} fullSize>
        <h2 class="-mt-4 border-b border-b-primary-400 pb-1 text-center text-xl font-bold leading-none text-colortext md:text-3xl">
          Assign sports & round scorings
        </h2>
        <h3
          class="-mt-4 text-center text-lg md:mb-10 md:text-xl lg:-mt-8"
          classList={{
            hidden: isSmall(),
          }}
        >
          Place sports in round order and assign round scorings to each sport
        </h3>
        <Show
          when={isSmall()}
          fallback={
            <DesktopVersion
              sports={sports}
              scorings={scorings}
              assignedSport={assignedSports}
              setAssignedSport={setAssignedSports}
              assignedRoundScoring={assignedRoundScorings}
              setAssignedRoundScoring={setAssignedRoundScorings}
            />
          }
        >
          <MobileVersion
            sports={sports}
            scorings={scorings}
            assignedSport={assignedSports}
            setAssignedSport={setAssignedSports}
            assignedRoundScoring={assignedRoundScorings}
            setAssignedRoundScoring={setAssignedRoundScorings}
          />
        </Show>
      </AnimatedForm>
    </>
  );
}

const DNDTYPES = Object.freeze({
  sport: "sport",
  scoring: "scoring",
});
type DesktopVersionProps = {
  sports: UniqueSport[];
  scorings: RoundScoring[];
  assignedSport: Accessor<(UniqueSport | undefined)[]>;
  setAssignedSport: Setter<(UniqueSport | undefined)[]>;
  assignedRoundScoring: Accessor<(RoundScoring | undefined)[]>;
  setAssignedRoundScoring: Setter<(RoundScoring | undefined)[]>;
};
function DesktopVersion(props: DesktopVersionProps) {
  const stats = useContext(StepperFormContext).values.get(steps[0])!;
  const tournamentType = useContext(StepperFormContext).tournamentType;
  const rounds = () => Array(parseInt(stats.get("roundCount")!));

  const dragRemove = ({ draggable }: DragEvent) => {
    if (Number.isInteger(draggable.data.index)) {
      if (draggable.node.dataset["type"] === DNDTYPES.sport)
        props.setAssignedSport((arr) => {
          arr[draggable.data.index] = undefined;
          return arr;
        });
      else
        props.setAssignedRoundScoring((arr) => {
          arr[draggable.data.index] = undefined;
          return arr;
        });
    }
  };

  const dragAddOrReplace = ({ draggable, droppable }: DragEvent) => {
    if (!Number.isInteger(draggable.data.index)) {
      if (draggable.node.dataset["type"] === DNDTYPES.sport)
        props.setAssignedSport((arr) => {
          arr[droppable!.data.index] = { ...draggable.data.value };
          return arr;
        });
      else
        props.setAssignedRoundScoring((arr) => {
          arr[droppable!.data.index] = { ...draggable.data.value };
          return arr;
        });
    } else {
      if (draggable.node.dataset["type"] === DNDTYPES.sport)
        props.setAssignedSport((arr) => {
          arr[droppable!.data.index] = { ...draggable.data.value };
          arr[draggable.data.index] = undefined;
          return arr;
        });
      else
        props.setAssignedRoundScoring((arr) => {
          arr[droppable!.data.index] = { ...draggable.data.value };
          arr[draggable.data.index] = undefined;
          return arr;
        });
    }
  };

  const onDragEnd = ({ draggable, droppable }: DragEvent) => {
    if (droppable) {
      if (
        draggable.data.type !== droppable.data.type &&
        !Boolean(droppable.node.querySelector("[data-type]"))
      )
        return;
      dragAddOrReplace({ draggable, droppable });
    } else dragRemove({ draggable });
  };

  return (
    <div
      class="grid grid-cols-2 gap-4"
      classList={{
        "!grid-cols-3": tournamentType() === "roundrobin",
      }}
    >
      <DragDropProvider onDragEnd={onDragEnd}>
        <DragDropSensors />
        <section class="border-r border-r-secondary/50 px-2">
          <div class="sticky top-4">
            <h4 class="mb-8 text-center text-2xl">Sports</h4>
            <ul class="flex max-h-[75vh] flex-col gap-4">
              <For each={props.sports}>
                {(sport) => <SportDraggable sport={sport} />}
              </For>
            </ul>
          </div>
        </section>
        <section class="flex flex-col gap-8">
          <h4 class="text-center text-2xl">Rounds</h4>
          <For each={rounds()}>
            {(_, index) => (
              <div class="flex flex-col gap-1">
                <h5 class="pb-2 text-center text-xl">#{index() + 1}</h5>
                <SportDroppable
                  sport={props.assignedSport()[index()]}
                  index={index()}
                />
                <Show when={tournamentType() === "roundrobin"}>
                  <ScoringDroppable
                    scoring={props.assignedRoundScoring()[index()]}
                    index={index()}
                  />
                </Show>
              </div>
            )}
          </For>
        </section>
        <Show when={tournamentType() === "roundrobin"}>
          <section class="border-l border-l-secondary/50 px-2">
            <div class="sticky top-4">
              <h4 class="mb-8 text-center text-2xl">Round Scorings</h4>
              <ul class="flex max-h-[75vh] flex-col gap-4">
                <For each={props.scorings}>
                  {(scoring) => <RoundScoringDraggable scoring={scoring} />}
                </For>
              </ul>
            </div>
          </section>
        </Show>
      </DragDropProvider>
    </div>
  );
}

type SportDraggableProps = {
  sport: UniqueSport;
  index?: number;
};
const SportDraggable = (props: SportDraggableProps) => {
  const id = createUniqueId();
  const draggable = createDraggable(id, {
    type: DNDTYPES.sport,
    value: props.sport,
    index: props.index,
  });
  const pTextColor = props.sport.customColor
    ? isDarkTextBetterContrast(props.sport.customColor)
      ? "#111111"
      : "#eeeeee"
    : "#111111";
  const pColor = props.sport.customColor ?? "#E7CCD1";

  return (
    <div
      ref={draggable.ref}
      class="overflow-hidden rounded-t-md border border-custom"
      style={{
        ...transformStyle(draggable.transform),
        [TAILWINDCUSTOM.BORDER]: pColor,
      }}
      data-type={DNDTYPES.sport}
    >
      <div class="flex">
        <h3
          class="relative flex-1 cursor-move break-words bg-custom py-5 pl-[1em] text-center text-lg font-bold leading-none text-custom"
          style={{
            [TAILWINDCUSTOM.BG]: pColor,
            [TAILWINDCUSTOM.TEXT]: pTextColor,
          }}
          {...draggable.dragActivators}
        >
          <svg
            fill="currentColor"
            stroke-width="0"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            height="2em"
            width="2em"
            class="absolute left-1 top-3 rotate-90 scale-125 opacity-50"
          >
            <path d="M4.929 19.081c1.895 1.895 4.405 2.938 7.071 2.938s5.177-1.043 7.071-2.938c3.899-3.899 3.899-10.243 0-14.143C17.177 3.044 14.665 2 12 2S6.823 3.044 4.929 4.938c-3.899 3.899-3.899 10.244 0 14.143zm12.728-1.414a7.969 7.969 0 0 1-3.813 2.129c-.009-1.602.586-3.146 1.691-4.251 1.163-1.163 2.732-1.828 4.277-1.851a7.945 7.945 0 0 1-2.155 3.973zm2.325-5.965c-2.124-.021-4.284.853-5.861 2.429-1.532 1.532-2.327 3.68-2.263 5.881a7.946 7.946 0 0 1-5.516-2.345 7.97 7.97 0 0 1-2.332-5.512c.077.002.154.014.231.014 2.115 0 4.16-.804 5.637-2.28 1.58-1.58 2.457-3.739 2.43-5.873a7.948 7.948 0 0 1 5.349 2.337 7.96 7.96 0 0 1 2.325 5.349zM6.343 6.353a7.968 7.968 0 0 1 3.973-2.169c-.018 1.555-.685 3.124-1.851 4.291-1.104 1.103-2.642 1.696-4.238 1.691a7.929 7.929 0 0 1 2.116-3.813z"></path>
          </svg>
          {props.sport.name}
        </h3>
        <Popover.Root>
          <Popover.Trigger class="group rounded-tr-md px-2">
            <svg
              fill="currentColor"
              stroke-width="0"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 16 16"
              height="1.25em"
              width="1.25em"
              class="group-focus-visible:scale-125"
            >
              <path d="m9.708 6.075-3.024.379-.108.502.595.108c.387.093.464.232.38.619l-.975 4.577c-.255 1.183.14 1.74 1.067 1.74.72 0 1.554-.332 1.933-.789l.116-.549c-.263.232-.65.325-.905.325-.363 0-.494-.255-.402-.704l1.323-6.208Zm.091-2.755a1.32 1.32 0 1 1-2.64 0 1.32 1.32 0 0 1 2.64 0Z"></path>
            </svg>{" "}
          </Popover.Trigger>
          <Popover.Portal>
            <Popover.Content class="relative rounded-md border border-secondary !bg-background shadow-xl shadow-black/20 ui-expanded:animate-popoverenter ui-closed:animate-popoverexit">
              <Popover.Arrow />
              <div>
                <Popover.Title class="px-6 text-center text-lg">
                  {props.sport.name}
                </Popover.Title>
                <Popover.CloseButton class="absolute right-1 top-0 block text-xl leading-none">
                  {"\u26cc"}
                </Popover.CloseButton>
              </div>
              <Popover.Description>
                <SportCardInfos
                  sport={{
                    ...props.sport,
                    customColor: props.sport.customColor ?? "#E7CCD1",
                  }}
                />
              </Popover.Description>
            </Popover.Content>
          </Popover.Portal>
        </Popover.Root>
      </div>
    </div>
  );
};

type RoundScoringDraggableProps = {
  scoring: RoundScoring;
  index?: number;
};
const RoundScoringDraggable = (props: RoundScoringDraggableProps) => {
  const id = createUniqueId();
  const draggable = createDraggable(id, {
    type: DNDTYPES.scoring,
    value: props.scoring,
    index: props.index,
  });
  const pTextColor = props.scoring.customColor
    ? isDarkTextBetterContrast(props.scoring.customColor)
      ? "#111111"
      : "#eeeeee"
    : "#eeeeee";
  const pColor = props.scoring.customColor ?? "#65132C";

  return (
    <div
      ref={draggable.ref}
      class="overflow-hidden rounded-b-md border border-custom"
      style={{
        ...transformStyle(draggable.transform),
        [TAILWINDCUSTOM.BORDER]: pColor,
      }}
      data-type={DNDTYPES.scoring}
    >
      <div class="flex">
        <h3
          class="relative flex-1 cursor-move break-words bg-custom py-5 pl-[1em] text-center text-lg font-bold leading-none text-custom"
          style={{
            [TAILWINDCUSTOM.BG]: pColor,
            [TAILWINDCUSTOM.TEXT]: pTextColor,
          }}
          {...draggable.dragActivators}
        >
          <svg
            fill="none"
            stroke-width="2"
            xmlns="http://www.w3.org/2000/svg"
            width="2em"
            height="2em"
            viewBox="0 0 24 24"
            stroke="currentColor"
            stroke-linecap="round"
            stroke-linejoin="round"
            class="absolute left-1 top-3 -rotate-45 scale-125 opacity-50"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M3 5m0 2a2 2 0 0 1 2 -2h14a2 2 0 0 1 2 2v10a2 2 0 0 1 -2 2h-14a2 2 0 0 1 -2 -2z"></path>
            <path d="M12 5v2"></path>
            <path d="M12 10v1"></path>
            <path d="M12 14v1"></path>
            <path d="M12 18v1"></path>
            <path d="M7 3v2"></path>
            <path d="M17 3v2"></path>
            <path d="M15 10.5v3a1.5 1.5 0 0 0 3 0v-3a1.5 1.5 0 0 0 -3 0z"></path>
            <path d="M6 9h1.5a1.5 1.5 0 0 1 0 3h-.5h.5a1.5 1.5 0 0 1 0 3h-1.5"></path>
          </svg>
          {props.scoring.name}
        </h3>
        <Popover.Root>
          <Popover.Trigger class="group rounded-tr-md px-2">
            <svg
              fill="currentColor"
              stroke-width="0"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 16 16"
              height="1.25em"
              width="1.25em"
              class="group-focus-visible:scale-125"
            >
              <path d="m9.708 6.075-3.024.379-.108.502.595.108c.387.093.464.232.38.619l-.975 4.577c-.255 1.183.14 1.74 1.067 1.74.72 0 1.554-.332 1.933-.789l.116-.549c-.263.232-.65.325-.905.325-.363 0-.494-.255-.402-.704l1.323-6.208Zm.091-2.755a1.32 1.32 0 1 1-2.64 0 1.32 1.32 0 0 1 2.64 0Z"></path>
            </svg>{" "}
          </Popover.Trigger>
          <Popover.Portal>
            <Popover.Content class="relative rounded-md border border-secondary !bg-background shadow-xl shadow-black/20 ui-expanded:animate-popoverenter ui-closed:animate-popoverexit">
              <Popover.Arrow />
              <div>
                <Popover.Title class="px-6 text-center text-lg">
                  {props.scoring.name}
                </Popover.Title>
                <Popover.CloseButton class="absolute right-1 top-0 block text-xl leading-none">
                  {"\u26cc"}
                </Popover.CloseButton>
              </div>
              <Popover.Description>
                <RoundScoringInfos
                  scoring={{
                    ...props.scoring,
                    customColor: props.scoring.customColor ?? "#65132C",
                  }}
                />
              </Popover.Description>
            </Popover.Content>
          </Popover.Portal>
        </Popover.Root>
      </div>
    </div>
  );
};

type SportDroppableProps = {
  sport: UniqueSport | undefined;
  index: number;
};
function SportDroppable(props: SportDroppableProps) {
  let ref: HTMLDivElement;
  const id = createUniqueId();
  const droppable = createDroppable(id, {
    type: DNDTYPES.sport,
    index: props.index,
  });

  const [state] = useDragDropContext()!;

  const isActiveAccept = () =>
    droppable.isActiveDroppable &&
    state.active.draggable?.data.type === DNDTYPES.sport &&
    !Boolean(ref.querySelector("[data-type]"));

  const isActiveReject = () =>
    droppable.isActiveDroppable &&
    state.active.draggable?.data.type !== DNDTYPES.sport;

  return (
    <div
      ref={ref!}
      use:droppable
      class="relative flex min-h-[6rem] flex-col justify-center rounded-t-md border border-dashed border-secondary p-2"
      classList={{
        "bg-primary-100/25": isActiveAccept(),
        "bg-error-400/25": isActiveReject(),
        "!border-solid": Boolean(props.sport),
      }}
    >
      <Show
        when={props.sport}
        fallback={
          <span class="pointer-events-none absolute inset-0 grid place-items-center text-2xl font-bold uppercase opacity-40">
            Drag {DNDTYPES.sport} here
          </span>
        }
      >
        <SportDraggable sport={props.sport!} index={props.index} />
      </Show>
    </div>
  );
}

type ScoringDroppableProps = {
  scoring: RoundScoring | undefined;
  index: number;
};
function ScoringDroppable(props: ScoringDroppableProps) {
  let ref: HTMLDivElement;
  const id = createUniqueId();
  const droppable = createDroppable(id, {
    type: DNDTYPES.scoring,
    index: props.index,
  });

  const [state] = useDragDropContext()!;

  const isActiveAccept = () =>
    droppable.isActiveDroppable &&
    state.active.draggable?.data.type === DNDTYPES.scoring &&
    !Boolean(ref.querySelector("[data-type]"));

  const isActiveReject = () =>
    droppable.isActiveDroppable &&
    state.active.draggable?.data.type !== DNDTYPES.scoring;

  return (
    <div
      ref={ref!}
      use:droppable
      class="relative flex min-h-[6rem] flex-col justify-center rounded-b-md border border-dashed border-secondary p-2"
      classList={{
        "bg-primary-100/25": isActiveAccept(),
        "bg-error-400/25": isActiveReject(),
        "!border-solid": Boolean(props.scoring),
      }}
    >
      <Show
        when={props.scoring}
        fallback={
          <span class="pointer-events-none absolute inset-0 grid place-items-center text-2xl font-bold uppercase opacity-40">
            Drag {DNDTYPES.scoring} here
          </span>
        }
      >
        <RoundScoringDraggable scoring={props.scoring!} index={props.index} />
      </Show>
    </div>
  );
}

type SelectModalProps = {
  show: boolean;
  type: "scoring" | "sport";
  index: number;
};
const AssigningContext = createContext<{
  assignedSport: Accessor<(UniqueSport | undefined)[]>;
  setAssignedSport: Setter<(UniqueSport | undefined)[]>;
  assignedRoundScoring: Accessor<(RoundScoring | undefined)[]>;
  setAssignedRoundScoring: Setter<(RoundScoring | undefined)[]>;
}>();
function MobileVersion(props: DesktopVersionProps) {
  const stats = useContext(StepperFormContext).values.get(steps[0])!;
  const tournamentType = useContext(StepperFormContext).tournamentType;
  const rounds = () => Array(parseInt(stats.get("roundCount")!));
  const [modal, setModal] = createSignal<SelectModalProps>(
    { show: false, type: "sport", index: -1 },
    { equals: false },
  );

  return (
    <div class="flex flex-col gap-8">
      <For each={rounds()}>
        {(_, index) => (
          <div class="flex flex-col gap-1">
            <h5 class="pb-2 text-center text-xl">#{index() + 1}</h5>
            <div
              class="flex flex-col rounded-t-md border border-dashed border-secondary"
              classList={{
                "!border-solid": Boolean(props.assignedSport()[index()]),
              }}
            >
              <Show when={props.assignedSport()[index()]}>
                <Button.Root
                  class="bg-primary-200/25 py-2"
                  onClick={() => {
                    setModal({ show: true, type: "sport", index: index() });
                  }}
                >
                  Change sport
                </Button.Root>
              </Show>
              <div class="p-2">
                <Show
                  when={props.assignedSport()[index()]}
                  fallback={
                    <Button.Root
                      onClick={() => {
                        setModal({ show: true, type: "sport", index: index() });
                      }}
                      class="block w-full animate-pulse rounded-md bg-primary-400/60 p-2 text-center text-xl uppercase"
                    >
                      Select Sport
                    </Button.Root>
                  }
                >
                  <MobileSportButton
                    sport={props.assignedSport()[index()]!}
                    unselected={false}
                    index={index()}
                  />
                </Show>
              </div>
            </div>
            <Show when={tournamentType() === "roundrobin"}>
              <div
                class="flex flex-col-reverse rounded-b-md border border-dashed border-secondary"
                classList={{
                  "!border-solid": Boolean(
                    props.assignedRoundScoring()[index()],
                  ),
                }}
              >
                <Show when={props.assignedRoundScoring()[index()]!}>
                  <Button.Root
                    class="bg-primary-200/25 py-2"
                    onClick={() => {
                      setModal({ show: true, type: "scoring", index: index() });
                    }}
                  >
                    Change scoring
                  </Button.Root>
                </Show>
                <div class="p-2">
                  <Show
                    when={props.assignedRoundScoring()[index()]!}
                    fallback={
                      <Button.Root
                        class="block w-full animate-pulse rounded-md bg-primary-400/60 p-2 text-center text-xl uppercase"
                        onClick={() => {
                          setModal({
                            show: true,
                            type: "scoring",
                            index: index(),
                          });
                        }}
                      >
                        Select scoring
                      </Button.Root>
                    }
                  >
                    <MobileScoringButton
                      scoring={props.assignedRoundScoring()[index()]!}
                      unselected={false}
                      index={index()}
                    />
                  </Show>
                </div>
              </div>
            </Show>
          </div>
        )}
      </For>
      {/* TODO: move it up */}
      <AssigningContext.Provider
        value={{
          assignedSport: props.assignedSport,
          setAssignedSport: props.setAssignedSport,
          assignedRoundScoring: props.assignedRoundScoring,
          setAssignedRoundScoring: props.setAssignedRoundScoring,
        }}
      >
        <SelectModal
          showProps={modal()}
          close={() =>
            setModal((prev) => {
              prev.show = false;
              return prev;
            })
          }
          sports={props.sports}
          scorings={props.scorings}
        />
      </AssigningContext.Provider>
    </div>
  );
}

type SelectModalComponentProps = {
  showProps: SelectModalProps;
  close: () => void;
  sports: UniqueSport[];
  scorings: RoundScoring[];
};
function SelectModal(props: SelectModalComponentProps) {
  return (
    <Dialog.Root open={props.showProps.show}>
      <Dialog.Portal>
        <Dialog.Overlay
          onClick={() => props.close()}
          class="fixed inset-0 z-50 grid place-items-center overflow-auto p-2 backdrop-blur-sm ui-expanded:animate-modaloverlayenter ui-closed:animate-modaloverlayexit"
        >
          <Dialog.Content
            onClick={(e: MouseEvent) => e.stopPropagation()}
            class="relative w-full max-w-[1350px] rounded-sm border border-primary-200 bg-background p-2 shadow-xl shadow-black md:p-4"
          >
            <Dialog.Title class="flex-1 px-4 text-center text-2xl font-bold leading-none md:text-3xl">
              Select a{" "}
              <b class="bg-gradient-to-tr from-primary-500 to-accent-300 bg-clip-text text-colortext/0">
                {props.showProps.type}
              </b>
            </Dialog.Title>
            <Dialog.CloseButton
              onClick={() => props.close()}
              class="absolute right-2 top-0 flex items-center text-3xl leading-none outline-none transition-[transform,color] duration-100 hover:scale-125 hover:text-primary-200 focus-visible:scale-125 focus-visible:text-primary-200"
            >
              {"\u26cc"}
            </Dialog.CloseButton>
            <ul class="flex flex-col gap-4 pb-2 pt-4">
              <Show
                when={props.showProps.type === "sport"}
                fallback={
                  <For each={props.scorings}>
                    {(scoring) => (
                      <MobileScoringButton
                        scoring={scoring}
                        unselected={true}
                        index={props.showProps.index}
                        close={props.close}
                      />
                    )}
                  </For>
                }
              >
                <For each={props.sports}>
                  {(sport) => (
                    <MobileSportButton
                      sport={sport}
                      unselected={true}
                      index={props.showProps.index}
                      close={props.close}
                    />
                  )}
                </For>
              </Show>
            </ul>
          </Dialog.Content>
        </Dialog.Overlay>
      </Dialog.Portal>
    </Dialog.Root>
  );
}

type MobileScoringButtonProps = {
  scoring: RoundScoring;
  unselected: boolean;
  index?: number;
  close?: () => void;
};
function MobileScoringButton(props: MobileScoringButtonProps) {
  const setAssignedRoundScoring =
    useContext(AssigningContext)?.setAssignedRoundScoring;
  const pTextColor = props.scoring.customColor
    ? isDarkTextBetterContrast(props.scoring.customColor)
      ? "#111111"
      : "#eeeeee"
    : "#111111";
  const pColor = props.scoring.customColor ?? "#E7CCD1";

  return (
    <li
      class="overflow-hidden rounded-t-md border border-custom"
      style={{
        [TAILWINDCUSTOM.BORDER]: pColor,
      }}
    >
      <Dynamic
        component={props.unselected ? Button.Root : "div"}
        class="flex w-full"
        onClick={() => {
          if (!props.unselected || !setAssignedRoundScoring) return;
          setAssignedRoundScoring((arr) => {
            arr[props.index!] = props.scoring;
            return arr;
          });
          if (props.close) props.close();
        }}
      >
        <h3
          class="relative flex-1 break-words bg-custom py-5 pl-[1em] text-center text-lg font-bold leading-none text-custom"
          style={{
            [TAILWINDCUSTOM.BG]: pColor,
            [TAILWINDCUSTOM.TEXT]: pTextColor,
          }}
        >
          <svg
            fill="currentColor"
            stroke-width="0"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            height="2em"
            width="2em"
            class="absolute left-1 top-3 rotate-90 scale-125 opacity-50"
          >
            <path d="M4.929 19.081c1.895 1.895 4.405 2.938 7.071 2.938s5.177-1.043 7.071-2.938c3.899-3.899 3.899-10.243 0-14.143C17.177 3.044 14.665 2 12 2S6.823 3.044 4.929 4.938c-3.899 3.899-3.899 10.244 0 14.143zm12.728-1.414a7.969 7.969 0 0 1-3.813 2.129c-.009-1.602.586-3.146 1.691-4.251 1.163-1.163 2.732-1.828 4.277-1.851a7.945 7.945 0 0 1-2.155 3.973zm2.325-5.965c-2.124-.021-4.284.853-5.861 2.429-1.532 1.532-2.327 3.68-2.263 5.881a7.946 7.946 0 0 1-5.516-2.345 7.97 7.97 0 0 1-2.332-5.512c.077.002.154.014.231.014 2.115 0 4.16-.804 5.637-2.28 1.58-1.58 2.457-3.739 2.43-5.873a7.948 7.948 0 0 1 5.349 2.337 7.96 7.96 0 0 1 2.325 5.349zM6.343 6.353a7.968 7.968 0 0 1 3.973-2.169c-.018 1.555-.685 3.124-1.851 4.291-1.104 1.103-2.642 1.696-4.238 1.691a7.929 7.929 0 0 1 2.116-3.813z"></path>
          </svg>
          {props.scoring.name}
        </h3>
        <Show when={!props.unselected}>
          <Popover.Root>
            <Popover.Trigger class="group rounded-tr-md px-2">
              <svg
                fill="currentColor"
                stroke-width="0"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 16 16"
                height="1.25em"
                width="1.25em"
                class="group-focus-visible:scale-125"
              >
                <path d="m9.708 6.075-3.024.379-.108.502.595.108c.387.093.464.232.38.619l-.975 4.577c-.255 1.183.14 1.74 1.067 1.74.72 0 1.554-.332 1.933-.789l.116-.549c-.263.232-.65.325-.905.325-.363 0-.494-.255-.402-.704l1.323-6.208Zm.091-2.755a1.32 1.32 0 1 1-2.64 0 1.32 1.32 0 0 1 2.64 0Z"></path>
              </svg>{" "}
            </Popover.Trigger>
            <Popover.Portal>
              <Popover.Content class="relative rounded-md border border-secondary !bg-background shadow-xl shadow-black/20 ui-expanded:animate-popoverenter ui-closed:animate-popoverexit">
                <Popover.Arrow />
                <div>
                  <Popover.Title class="px-6 text-center text-lg">
                    {props.scoring.name}
                  </Popover.Title>
                  <Popover.CloseButton class="absolute right-1 top-0 block text-xl leading-none">
                    {"\u26cc"}
                  </Popover.CloseButton>
                </div>
                <Popover.Description>
                  <RoundScoringInfos
                    scoring={{
                      ...props.scoring,
                      customColor: props.scoring.customColor ?? "#65132C",
                    }}
                  />
                </Popover.Description>
              </Popover.Content>
            </Popover.Portal>
          </Popover.Root>
        </Show>
      </Dynamic>
    </li>
  );
}

type MobileSportButtonProps = {
  sport: UniqueSport;
  unselected: boolean;
  index?: number;
  close?: () => void;
};
function MobileSportButton(props: MobileSportButtonProps) {
  const setAssignedSport = useContext(AssigningContext)?.setAssignedSport;
  const pTextColor = props.sport.customColor
    ? isDarkTextBetterContrast(props.sport.customColor)
      ? "#111111"
      : "#eeeeee"
    : "#111111";
  const pColor = props.sport.customColor ?? "#E7CCD1";

  return (
    <li
      class="overflow-hidden rounded-t-md border border-custom"
      style={{
        [TAILWINDCUSTOM.BORDER]: pColor,
      }}
    >
      <Dynamic
        component={props.unselected ? Button.Root : "div"}
        class="flex w-full"
        onClick={() => {
          if (!props.unselected || !setAssignedSport) return;
          setAssignedSport((arr) => {
            arr[props.index!] = props.sport;
            return arr;
          });
          if (props.close) props.close();
        }}
      >
        <h3
          class="relative flex-1 break-words bg-custom py-5 pl-[1em] text-center text-lg font-bold leading-none text-custom"
          style={{
            [TAILWINDCUSTOM.BG]: pColor,
            [TAILWINDCUSTOM.TEXT]: pTextColor,
          }}
        >
          <svg
            fill="currentColor"
            stroke-width="0"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            height="2em"
            width="2em"
            class="absolute left-1 top-3 rotate-90 scale-125 opacity-50"
          >
            <path d="M4.929 19.081c1.895 1.895 4.405 2.938 7.071 2.938s5.177-1.043 7.071-2.938c3.899-3.899 3.899-10.243 0-14.143C17.177 3.044 14.665 2 12 2S6.823 3.044 4.929 4.938c-3.899 3.899-3.899 10.244 0 14.143zm12.728-1.414a7.969 7.969 0 0 1-3.813 2.129c-.009-1.602.586-3.146 1.691-4.251 1.163-1.163 2.732-1.828 4.277-1.851a7.945 7.945 0 0 1-2.155 3.973zm2.325-5.965c-2.124-.021-4.284.853-5.861 2.429-1.532 1.532-2.327 3.68-2.263 5.881a7.946 7.946 0 0 1-5.516-2.345 7.97 7.97 0 0 1-2.332-5.512c.077.002.154.014.231.014 2.115 0 4.16-.804 5.637-2.28 1.58-1.58 2.457-3.739 2.43-5.873a7.948 7.948 0 0 1 5.349 2.337 7.96 7.96 0 0 1 2.325 5.349zM6.343 6.353a7.968 7.968 0 0 1 3.973-2.169c-.018 1.555-.685 3.124-1.851 4.291-1.104 1.103-2.642 1.696-4.238 1.691a7.929 7.929 0 0 1 2.116-3.813z"></path>
          </svg>
          {props.sport.name}
        </h3>
        <Show when={!props.unselected}>
          <Popover.Root>
            <Popover.Trigger class="group rounded-tr-md px-2">
              <svg
                fill="currentColor"
                stroke-width="0"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 16 16"
                height="1.25em"
                width="1.25em"
                class="group-focus-visible:scale-125"
              >
                <path d="m9.708 6.075-3.024.379-.108.502.595.108c.387.093.464.232.38.619l-.975 4.577c-.255 1.183.14 1.74 1.067 1.74.72 0 1.554-.332 1.933-.789l.116-.549c-.263.232-.65.325-.905.325-.363 0-.494-.255-.402-.704l1.323-6.208Zm.091-2.755a1.32 1.32 0 1 1-2.64 0 1.32 1.32 0 0 1 2.64 0Z"></path>
              </svg>{" "}
            </Popover.Trigger>
            <Popover.Portal>
              <Popover.Content class="relative rounded-md border border-secondary !bg-background shadow-xl shadow-black/20 ui-expanded:animate-popoverenter ui-closed:animate-popoverexit">
                <Popover.Arrow />
                <div>
                  <Popover.Title class="px-6 text-center text-lg">
                    {props.sport.name}
                  </Popover.Title>
                  <Popover.CloseButton class="absolute right-1 top-0 block text-xl leading-none">
                    {"\u26cc"}
                  </Popover.CloseButton>
                </div>
                <Popover.Description>
                  <SportCardInfos
                    sport={{
                      ...props.sport,
                      customColor: props.sport.customColor ?? "#E7CCD1",
                    }}
                  />
                </Popover.Description>
              </Popover.Content>
            </Popover.Portal>
          </Popover.Root>
        </Show>
      </Dynamic>
    </li>
  );
}
