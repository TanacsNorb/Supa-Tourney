import {
  createEffect,
  createSignal,
  getOwner,
  runWithOwner,
  useContext,
} from "solid-js";
import {
  AnimatedForm,
  StepperFormContext,
  validateForm,
} from "../../createtournament";
import useToast from "../../../hooks/useToast";
import TextFieldInput from "../../../components/textfield";
import { z } from "zod";
import ButtonGroup from "../../../components/buttongroup";
import { FormSubmitEvent } from "../../../models/types";
import {
  TournamentTypeButtons,
  TournamentTypes,
} from "../../../static/constants/tournamenttypes";

export type ComponentTypes = {
  formId: string;
};
export default function TournamentSetupSubpage(props: ComponentTypes) {
  const formReaction = useContext(StepperFormContext).reaction;
  const setTournamentType = useContext(StepperFormContext).setTournamentType;
  const formValues = useContext(StepperFormContext).values.get(props.formId)!;
  const [selectedType, setSelectedType] = createSignal<TournamentTypes | null>(
    null,
  );
  const toast = useToast();

  createEffect(() => {
    if (selectedType()) setTournamentType(selectedType()!);
  });

  function handleSubmit(e: FormSubmitEvent) {
    e.preventDefault();
    const form = e.currentTarget;
    const formData = new FormData(form);
    const formIsValid = validateForm(form);
    if (!formIsValid) {
      toast.error({
        description: "Form is not valid!",
        title: "Tournament setup",
      });
      return;
    }

    formReaction(formData, props.formId);
  }

  return (
    <AnimatedForm handleSubmit={handleSubmit} id={props.formId}>
      <h2 class="-mt-4 mb-4 border-b border-b-primary-400 pb-1 text-center text-xl font-bold leading-none text-colortext md:mb-10 md:text-3xl">
        Tournament setup
      </h2>
      <TextFieldInput
        label="Tournament Name"
        name="tournamentName"
        value={formValues.get("tournamentName")}
        schema={z
          .string()
          .min(3, { message: "Minimum length is 3!" })
          .max(64, { message: "Max length is 64!" })
          .regex(new RegExp("^[a-zA-Z0-9 _]+$"), {
            message: "Letters and numbers only!",
          })}
        required
      />
      <ButtonGroup
        label="Tournament Type"
        name="tournamentType"
        defaultValue={formValues.get("tournamentType")}
        buttons={TournamentTypeButtons}
        onChange={(value) => setSelectedType(value as TournamentTypes)}
        required
      />
      <RelatedSetupInputs
        related={selectedType() === null ? null : selectedType() === "koth"}
        defaultValue={{
          players: formValues.get("maxPlayers"),
          rounds: formValues.get("roundCount"),
        }}
      />
    </AnimatedForm>
  );
}

type Limits = {
  players: {
    min: number;
    max: number;
  };
  round: {
    min: number;
    max: number;
  };
};
const ROUNDROBINNUMBERS: Limits = {
  players: {
    min: 2,
    max: 32,
  },
  round: {
    min: 1,
    max: 12,
  },
};

const KOFTHNUMBERS: Limits = {
  players: {
    min: 2,
    max: 32,
  },
  round: {
    min: 1,
    max: 5,
  },
};
type RelatedSetupInputProps = {
  related: boolean | null;
  defaultValue?: {
    players?: string;
    rounds?: string;
  };
};
function RelatedSetupInputs(props: RelatedSetupInputProps) {
  const owner = getOwner();
  let firstRender = true;
  const [maxPlayersRef, setMaxPlayersRef] =
    createSignal<HTMLInputElement | null>(null);
  const [roundCountRef, setRoundCountRef] =
    createSignal<HTMLInputElement | null>(null);
  const [limits, setLimits] = createSignal<Limits>({
    players: {
      min: 0,
      max: 0,
    },
    round: { min: 0, max: 0 },
  });

  function calculateRounds(playersCountFromInput: string) {
    const playersCount = parseInt(playersCountFromInput);
    if (isNaN(playersCount)) return "-";
    let round = 1;
    while (true) {
      if (2 ** round >= playersCount) {
        return round.toString();
      }
      round++;
    }
  }

  function calculatePlayers(roundsCountFromInput: string) {
    const rounds = parseInt(roundsCountFromInput);
    if (isNaN(rounds)) return "-";
    return (2 ** rounds).toString();
  }

  createEffect(() => {
    if (props.related === null) return;
    if (props.related) setLimits(KOFTHNUMBERS);
    else setLimits(ROUNDROBINNUMBERS);

    // prevent reset if there is default value
    if (props.defaultValue && firstRender) {
      firstRender = false;
      return;
    }

    const playerEl = maxPlayersRef();
    if (playerEl && playerEl.value !== "") playerEl.value = "";
    const countEl = roundCountRef();
    if (countEl && countEl.value !== "") countEl.value = "";
  });

  function handleRoundCountOnInput(
    e: InputEvent & { currentTarget: HTMLInputElement },
  ) {
    runWithOwner(owner, () => {
      const el = maxPlayersRef();
      if (props.related && el)
        el.value = calculatePlayers(e.currentTarget.value);
    });
  }

  function handleMaxPlayersOnInput(
    e: InputEvent & { currentTarget: HTMLInputElement },
  ) {
    runWithOwner(owner, () => {
      const el = roundCountRef();
      if (props.related && el)
        el.value = calculateRounds(e.currentTarget.value);
    });
  }

  return (
    <div class="flex flex-col gap-6 sm:flex-row lg:gap-12">
      <TextFieldInput
        label={`Max number of Players (${limits().players.min}-${
          limits().players.max
        })`}
        inputref={setMaxPlayersRef}
        onInput={handleMaxPlayersOnInput}
        name="maxPlayers"
        value={props.defaultValue?.players}
        inputmode="numeric"
        required
        disabled={props.related === null}
        schema={z.coerce
          .number({
            invalid_type_error: "Input must be a number!",
            required_error: "Required",
          })
          .int({ message: "Must be a whole number! " })
          .gte(limits().players.min, {
            message: `Minimum is ${limits().players.min}!`,
          })
          .lte(limits().players.max, {
            message: `Maximum is ${limits().players.max}!`,
          })}
      />
      <TextFieldInput
        label={`Number of rounds (${limits().round.min}-${limits().round.max})`}
        inputref={setRoundCountRef}
        onInput={handleRoundCountOnInput}
        name="roundCount"
        value={props.defaultValue?.rounds}
        description={
          props.related
            ? "When King of the Hill is chosen, the max number of players and round count fields are related!"
            : ""
        }
        inputmode="numeric"
        required
        disabled={props.related === null}
        schema={z.coerce
          .number({
            invalid_type_error: "Input must be a number!",
            required_error: "Required",
          })
          .int({ message: "Must be a whole number! " })
          .gte(limits().round.min, {
            message: `Minimum is ${limits().round.min}!`,
          })
          .lte(limits().round.max, {
            message: `Maximum is ${limits().round.max}!`,
          })}
      />
    </div>
  );
}
