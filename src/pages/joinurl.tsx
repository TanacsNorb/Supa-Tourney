import { useNavigate, useSearchParams } from "@solidjs/router";
import AnimatedPage from "../components/animatedpage";
import BackButton from "../components/backbutton";
import { onMount } from "solid-js";
import Spinner from "../components/spinner";
import request from "../http/request";
import PBENDPOINTS from "../http/endpoints";
import ROUTES from "../static/constants/routes";

export default function JoinUrl() {
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const tournament = searchParams.tournament;
  const password = searchParams.password;

  onMount(() => {
    if (!Boolean(tournament) || !Boolean(password))
      navigate("/", { replace: true });

    request(async (pb) => {
      await new Promise((r) => setTimeout(r, 200));
      const response = await pb.send(PBENDPOINTS.joinwithurl, {
        method: "POST",
        body: { tournament, password },
      });

      if(response.joined){
        navigate(ROUTES.lobby.navigate(tournament))
        return;
      }

      window.localStorage.setItem("tmp_pocketbase", response.token);
      window.localStorage.setItem("tmp_avatar", response.avatar);
      navigate(ROUTES.setupGuest, { replace: true });
    }).catch((_) => navigate("/", { replace: true }));
  });

  return (
    <AnimatedPage>
      <BackButton />
      <div class="mt-4 grid place-items-center gap-4 text-xl md:mt-8">
        <p>Joining tournament...</p>
        <Spinner size="big" show />
      </div>
    </AnimatedPage>
  );
}
