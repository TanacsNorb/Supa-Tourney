import { Tabs } from "@kobalte/core";
import AnimatedPage from "../components/animatedpage";
import {
  Show,
  createContext,
  createEffect,
  createResource,
  createSignal,
  onCleanup,
  onMount,
} from "solid-js";
import userIsAuthenticated from "../http/authenticatedroute";
import { newPocketbase } from "../http/request";
import { useNavigate } from "@solidjs/router";
import Spinner from "../components/spinner";
import { Record } from "pocketbase";
import PBENDPOINTS from "../http/endpoints";
import { getOpponents } from "../http/gamepage";
import MatchPage from "./subpages/game/matchpage";
import ScoreboardPage, { MatchesRounds } from "./subpages/game/scoreboardpage";
import ROUTES from "../static/constants/routes";
import useToast from "../hooks/useToast";

export const TournamentContext = createContext({
  usernames: {} as UsernamesType,
  tournament: () => ({}) as TournamentData,
  currentRound: -1,
});

type TournamentData = Record & {
  expand: {
    tournament: Record;
  };
};
export type UsernamesType = {
  [key: string]: { name: string; avatar: string | null };
};
export default function Game() {
  userIsAuthenticated();
  const navigate = useNavigate();
  const pb = newPocketbase();
  const toaster = useToast();
  const [tournament, setTournament] = createSignal<null | TournamentData>(null);
  const [leaderboard, setLeaderboard] = createSignal<null | Record[]>(null);
  const [tournamentStatus, setTournamentStatus] = createSignal<string | null>(
    null,
  );
  const [opponents, opponentsResource] = createResource(getOpponents);
  const [roundScorings, setRoundScorings] = createSignal<Record[] | null>(null);
  const [sports, setSports] = createSignal<Record[] | null>(null);
  const [matches, setMatches] = createSignal<Record[] | null>(null);
  const [usernames, setUsernames] = createSignal<UsernamesType | null>(null);
  const [selectedTab, setSelectedTab] = createSignal("scoreboard");
  const [matchesRounds, setMatchesRounds] = createSignal<MatchesRounds>(
    {},
    { equals: false },
  );
  const [realtimeMatch, setRealtimeMatch] = createSignal({} as Record, {
    equals: false,
  });
  let currentRound = -1;

  function determineTournamentStatus() {
    if (!tournament()) return null;
    if (tournament()!.currentround === -1) return "Tournament is starting...";
    if (tournament()!.everyonefinished) {
      if (
        tournament()!.currentround + 1 ===
        tournament()!.expand.tournament.rounds
      )
        return "Last matches have been finished...";
      return "Starting new round...";
    }
    return "Matches are in progress...";
  }

  function updateLeaderboard() {
    pb.collection("playerscores")
      .getFullList({
        sort: "-score",
      })
      .then((result) => setLeaderboard(result));
  }

  function attachMatchListeners(parameterMatches: Record[] | null = null) {
    // WARN: when the whole tournament ends?
    const allMatches = parameterMatches ?? matches();
    if (!allMatches || currentRound < 0) return;
    const currentRoundMatches = allMatches.filter(
      (match) => match.tournamentround === currentRound,
    );

    if (currentRound !== 0) {
      pb.collection(PBENDPOINTS.tournamentMatches).unsubscribe();
    }

    for (const match of currentRoundMatches) {
      if (match.playeroneid === "X" || match.playertwoid === "X") {
        pb.collection(PBENDPOINTS.tournamentMatches)
          .getOne(match.id)
          .then((noOpponentMatch) => {
            setMatchesRounds((prev) => {
              prev[noOpponentMatch.id] = noOpponentMatch;
              return prev;
            });
          });
      } else {
        const you = pb.authStore.model?.id;
        if (match.playeroneid === you || match.playertwoid === you) {
          pb.collection(PBENDPOINTS.tournamentMatches)
            .getOne(match.id)
            .then((yourMatch) => {
              setRealtimeMatch(yourMatch);
            });
        }

        pb.collection(PBENDPOINTS.tournamentMatches).subscribe(
          match.id,
          function (e) {
            if (e.action === "update") {
              setMatchesRounds((prev) => {
                prev[e.record.id] = e.record;
                return prev;
              });

              if (e.record.winnerid) {
                const playedIt =
                  e.record.playeroneid === pb.authStore.model?.id ||
                  e.record.playertwoid === pb.authStore.model?.id;

                if (tournament()?.expand.tournament.notification && !playedIt) {
                  const usernamesArray = usernames();
                  if (usernamesArray) {
                    const loserId =
                      e.record.playeroneid === e.record.winnerid
                        ? e.record.playertwoid
                        : e.record.playeroneid;

                    toaster.default({
                      title: `${usernamesArray[e.record.winnerid]?.name} won`,
                      description: `versus ${usernamesArray[loserId]?.name}`,
                    });
                  }
                }
                opponentsResource.refetch();
              }

              const userid = pb.authStore.model?.id;
              if (
                e.record.playeroneid === userid ||
                e.record.playertwoid === userid
              ) {
                setRealtimeMatch(e.record);
              }
            }
          },
        );
      }
    }
  }

  onMount(() => {
    const tournamentid = pb.authStore.model?.tournament;
    if (!tournamentid) navigate("/", { replace: true });

    pb.collection(PBENDPOINTS.tournamentsInGame)
      .getFirstListItem(`tournament='${tournamentid}'`, {
        expand: "tournament",
      })
      .then((result) => {
        setTournament(result as TournamentData);
        currentRound = result.currentround;

        pb.collection(PBENDPOINTS.tournamentsInGame).subscribe(
          result.id,
          function (e) {
            if (e.action === "update" && e.record.tournament === tournamentid) {
              if (e.record.currentround > currentRound) {
                currentRound = e.record.currentround;
                attachMatchListeners();

                const yourScore = leaderboard()!.find(
                  (playerScore) =>
                    playerScore.playerid === pb.authStore.model?.id,
                )?.score;

                if (
                  tournament()?.expand.tournament.type !== "koth" ||
                  (tournament()?.expand.tournament.type === "koth" &&
                    yourScore !== -1)
                )
                  setSelectedTab("match");
              }
              if (e.record.everyonefinished) {
                pb.collection(PBENDPOINTS.tournamentMatches)
                  .getFullList({
                    sort: "tournamentround,-branch",
                  })
                  .then((matchesResult) => {
                    setMatches(matchesResult);
                    setMatchesRounds((prev) => {
                      for (const match of matchesResult) {
                        prev[match.id] = match;
                      }
                      return prev;
                    });
                  });
              }
              setTournament({
                ...e.record,
                expand: tournament()?.expand,
              } as TournamentData);
            }
          },
        );

        pb.collection(PBENDPOINTS.tournamentMatches)
          .getFullList({
            sort: "tournamentround,-branch",
          })
          .then((matchesResult) => {
            setMatches(matchesResult);
            setMatchesRounds((prev) => {
              for (const match of matchesResult) {
                prev[match.id] = match;
              }
              return prev;
            });

            for (const match of matchesResult) {
              const userid = pb.authStore.model?.id;
              if (
                match.playeroneid === userid ||
                match.playertwoid === userid
              ) {
                setRealtimeMatch(match);
                break;
              }
            }

            if (result.currentround !== -1) attachMatchListeners(matchesResult);
          });
      });
    pb.collection(PBENDPOINTS.tournaments).subscribe(
      tournamentid,
      function (e) {
        if (e.record.status === "ended")
          navigate(ROUTES.tournamentEnded, {
            state: {
              leaderboard: leaderboard(),
              usernames: usernames(),
              avatarType: tournament()!.expand.tournament.avatar,
              tournamentType: tournament()!.expand.tournament.type,
            },
          });
      },
    );

    pb.collection(PBENDPOINTS.roundScorings)
      .getFullList({
        sort: "roundnumber",
      })
      .then((result) => setRoundScorings(result));
    pb.collection(PBENDPOINTS.sports)
      .getFullList({
        sort: "roundnumber",
      })
      .then((result) => setSports(result));
    pb.collection(PBENDPOINTS.lobbyUsers)
      .getFullList({
        filter: `tournament="${tournamentid}"`,
      })
      .then((result) => {
        const usernames = {} as UsernamesType;
        result.forEach(
          (player) =>
            (usernames[player.id] = {
              name: player.username,
              avatar: player.selectedavatar,
            }),
        );
        setUsernames(usernames);
      });
  });

  createEffect(() => {
    if (tournament()) {
      updateLeaderboard();
      setTournamentStatus(determineTournamentStatus());
    }
  });

  onCleanup(() => {
    pb.collection(PBENDPOINTS.tournamentMatches).unsubscribe();
    pb.collection(PBENDPOINTS.tournamentsInGame).unsubscribe();
  });

  // try it with suspense?
  return (
    <AnimatedPage>
      <section class="mx-auto max-w-[1250px] overflow-x-clip px-2 pb-4">
        <Show
          when={
            tournament() &&
            !opponents.loading &&
            roundScorings() &&
            sports() &&
            matches() &&
            realtimeMatch().id &&
            usernames()
          }
          fallback={<Spinner show size="giant" />}
        >
          <Tabs.Root value={selectedTab()} onChange={setSelectedTab}>
            <Tabs.List class="sticky top-0 z-10 flex rounded-b-md border-2 border-t-0 border-solid border-primary-400 bg-background text-lg md:text-xl">
              <Tabs.Trigger
                value="scoreboard"
                class="flex-1 rounded-bl-md border-r border-r-primary-400 p-2 outline outline-2 -outline-offset-4 outline-primary-400/0 transition-[background-color,outline-color] hover:bg-primary-400/10 focus-visible:outline-primary-400 ui-selected:bg-[#4E2040] fine:p-1"
              >
                Scoreboard
              </Tabs.Trigger>
              <Tabs.Trigger
                value="match"
                class="flex-1 rounded-br-md border-l border-l-primary-400 p-2 outline outline-2 -outline-offset-4 outline-primary-400/0 transition-[background-color,outline-color] hover:bg-primary-400/10 focus-visible:outline-primary-400 ui-selected:bg-[#4E2040] fine:p-1"
              >
                Match
              </Tabs.Trigger>
            </Tabs.List>
            <TournamentContext.Provider
              value={{
                usernames: usernames()!,
                // @ts-ignore
                tournament: tournament,
                currentRound: currentRound,
              }}
            >
              <Tabs.Content value="scoreboard">
                <ScoreboardPage
                  leaderboard={leaderboard()}
                  opponents={opponents()}
                  currentRound={tournament()!.currentround}
                  roundScorings={roundScorings()!}
                  sports={sports()!}
                  matches={matches()!}
                  matchesRounds={matchesRounds()}
                  tournamentStatus={tournamentStatus()}
                  everyoneFinished={tournament()!.everyonefinished}
                />
              </Tabs.Content>
              <Tabs.Content value="match">
                <MatchPage
                  currentRound={tournament()!.currentround}
                  roundScorings={roundScorings()!}
                  sports={sports()!}
                  opponents={opponents()}
                  matches={matches()!}
                  realtimeMatch={realtimeMatch}
                  yourScore={leaderboard()!.find(
                    (playerScore) =>
                      playerScore.playerid === pb.authStore.model?.id,
                  )}
                  tournamentType={tournament()!.expand.tournament.type}
                />
              </Tabs.Content>
            </TournamentContext.Provider>
          </Tabs.Root>
        </Show>
      </section>
    </AnimatedPage>
  );
}
