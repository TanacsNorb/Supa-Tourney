import { Match, Switch } from "solid-js";
import AnimatedPage from "../components/animatedpage";
import { isAuthorized } from "../hooks/isauthorized";
import AuthorizedHome from "./subpages/home/authorizedhome";
import Unauthorized from "./subpages/home/unauthorized";
import Logo from "../static/tourneylogo.png";

export default function Home() {
  return (
    <AnimatedPage>
      <section class="mx-2 my-12 max-w-[750px] overflow-hidden rounded-2xl border-2 border-primary-400 bg-secondary/5 p-6 shadow-xl shadow-primary-600/70 md:mx-auto md:mb-12 md:mt-[15vh] md:p-10 lg:p-12">
        <h1 class="-mt-6 mb-[max(8vh,_2rem)] rounded-b-2xl border-2 border-t-0 border-primary-400 bg-primary-400/20 py-2 text-center text-[clamp(1.5rem,_4vw,_4.5rem)] font-bold leading-none md:-mt-10 md:py-4 lg:-mt-12">
          <span>Tourney </span>
          <img class="inline-block aspect-square h-[1em]" src={Logo} />
        </h1>
        <Switch>
          <Match when={isAuthorized()}>
            <AuthorizedHome />
          </Match>
          <Match when={!isAuthorized()}>
            <Unauthorized />
          </Match>
        </Switch>
      </section>
    </AnimatedPage>
  );
}
