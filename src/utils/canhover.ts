export default function canHover(){
  return window.matchMedia("(hover: hover)").matches;
}
