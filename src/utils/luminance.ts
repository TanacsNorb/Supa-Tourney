// Reference
// https://stackoverflow.com/questions/9733288/how-to-programmatically-calculate-the-contrast-ratio-between-two-colors
const RED = 0.2126;
const GREEN = 0.7152;
const BLUE = 0.0722;

const GAMMA = 2.4;

function luminance(r: number, g: number, b: number) {
  const a = [r, g, b].map((v) => {
    v /= 255;
    return v <= 0.03928 ? v / 12.92 : Math.pow((v + 0.055) / 1.055, GAMMA);
  });
  return a[0] * RED + a[1] * GREEN + a[2] * BLUE;
}

function hexStringToRGB(hexcolor: string) {
  const hexrgb = hexcolor.slice(1);
  const r = parseInt(`${hexrgb[0]}${hexrgb[1]}`, 16);
  const g = parseInt(`${hexrgb[2]}${hexrgb[3]}`, 16);
  const b = parseInt(`${hexrgb[4]}${hexrgb[5]}`, 16);

  return [r, g, b] as const;
}

function contrast(hex1: string, hex2: string) {
  const lum1 = luminance(...hexStringToRGB(hex1));
  const lum2 = luminance(...hexStringToRGB(hex2));
  const brightest = Math.max(lum1, lum2);
  const darkest = Math.min(lum1, lum2);
  return (brightest + 0.05) / (darkest + 0.05);
}
// End of reference

export function isDarkTextBetterContrast(hex: string): boolean {
  return contrast("#000000", hex) > contrast("#ffffff", hex);
}

export function rgbaToRgbHex(foreground: string, background: string) {
  const hexopacity = foreground[7] + foreground[8];
  const fgOpacity = parseInt(hexopacity, 16) / 255;
  const bgOpacity = 1 - fgOpacity;

  const fr = hexStringToRGB(foreground);
  const br = hexStringToRGB(background);
  const [r, g, b] = fr.map((val, index) =>
    parseInt((val * fgOpacity + br[index] * bgOpacity).toFixed(0)),
  );

  return (
    "#" +
    r.toString(16).padStart(2, "0") +
    g.toString(16).padStart(2, "0") +
    b.toString(16).padStart(2, "0")
  );
}
