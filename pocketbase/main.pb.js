/// <reference path="./types.d.ts" />

cronAdd("finishedtournaments", "@midnight", () => {
  const tournaments = $app
    .dao()
    .findRecordsByFilter("tournaments", "status = 'ended'");
  const minDiff = 1000 * 60 * 60 * 12;
  for (let i = 0; i < tournaments.length; i++) {
    const tournament = tournaments[i];
    const datenow = new DateTime().time().unixMilli();
    if (tournament.updated.time().unixMilli() + minDiff >= datenow) continue;

    $app.dao().deleteRecord(tournament);
  }
});

cronAdd("inactivetournaments", "@midnight", () => {
  const tournaments = $app
    .dao()
    .findRecordsByFilter("tournaments", "status != 'ended'");
  // Each phase of a tournament must be concluded in 3 days
  const minDiff = 1000 * 60 * 60 * 72;
  for (let i = 0; i < tournaments.length; i++) {
    const tournament = tournaments[i];
    const datenow = new DateTime().time().unixMilli();
    if (tournament.updated.time().unixMilli() + minDiff >= datenow) continue;

    $app.dao().deleteRecord(tournament);
  }
});

routerAdd(
  "POST",
  "/jointournament/:tournamentid",
  (c) => {
    const key = require(`${__hooks}/env/env.json`).rsakey;
    const info = $apis.requestInfo(c);
    const tournamentid = c.pathParam("tournamentid");
    const user = info.authRecord;
    const data = info.data;
    const password = data.password;

    const tournament = $app.dao().findRecordById("tournaments", tournamentid);
    if (!tournament) return c.json(404, "Tournament not found!");

    const tournamentPassword = $app.dao().findRecordsByExpr(
      "tournamentpasswords",
      $dbx.exp("tournament = {:tournamentid}", {
        tournamentid: tournament.id,
      }),
    )[0];
    if ($security.decrypt(tournamentPassword.get("password"), key) !== password)
      return c.json(400, "Wrong password!");

    const maxPlayerCount = tournament.getInt("players");
    const joinedusers = $app.dao().findRecordsByExpr(
      "lobbyusers",
      $dbx.exp("tournament = {:tournamentid}", {
        tournamentid: tournamentid,
      }),
    );

    for (let i = 0; i < joinedusers.length; i++) {
      if (joinedusers[i].id === user.id)
        return c.json(200, { message: "Already joined!" });
    }
    if (joinedusers.length >= maxPlayerCount)
      throw new BadRequestError("This tournament is full!");

    const generatedPassword = $security.randomString(15).toLowerCase();
    tournamentPassword.set(
      "password",
      $security.encrypt(generatedPassword, key),
    );
    $app.dao().saveRecord(tournamentPassword);

    user.set("tournament", tournamentid);
    $app.dao().saveRecord(user);

    return c.json(200, {
      message: "Successfully joined!",
    });
  },
  $apis.activityLogger($app),
  $apis.requireRecordAuth(),
);

routerAdd(
  "GET",
  "/tournamentpassword",
  (c) => {
    const info = $apis.requestInfo(c);
    const user = info.authRecord;
    const key = require(`${__hooks}/env/env.json`).rsakey;

    const tournaments = $app.dao().findRecordsByExpr(
      "tournaments",
      $dbx.exp("owner = {:user}", {
        user: user.id,
      }),
    );
    if (tournaments.length === 0)
      return c.json(403, {
        message: "Only the tournament creator can access this!",
      });

    let tournament;
    for (let i = 0; i < tournaments.length; i++) {
      try {
        $app
          .dao()
          .findFirstRecordByFilter(
            "tournamentsingame",
            "tournament = {:tournamentid}",
            { tournamentid: tournaments[i].id },
          );
      } catch (error) {
        tournament = tournaments[i];
        break;
      }
    }

    const tournamentPassword = $app.dao().findRecordsByExpr(
      "tournamentpasswords",
      $dbx.exp("tournament = {:tournamentid}", {
        tournamentid: tournament.id,
      }),
    )[0];

    if (tournamentPassword)
      return c.json(200, {
        password: $security.decrypt(tournamentPassword.get("password"), key),
      });

    const generatedPassword = $security.randomString(15).toLowerCase();
    const collection = $app
      .dao()
      .findCollectionByNameOrId("tournamentpasswords");

    const record = new Record(collection, {
      password: $security.encrypt(generatedPassword, key),
      tournament: tournament.id,
    });

    $app.dao().saveRecord(record);

    return c.json(200, {
      password: generatedPassword,
    });
  },
  $apis.activityLogger($app),
  $apis.requireRecordAuth(),
);

routerAdd(
  "GET",
  "/gettournament/:tournamentid",
  (c) => {
    // TODO: it is not started nor it is full
    const tournamentId = c.pathParam("tournamentid");
    const tournament = $app.dao().findRecordById("tournaments", tournamentId);
    const info = $apis.requestInfo(c);
    const user = info.authRecord;
    if (!tournament) return c.json(404, { found: false });

    const users = $app.dao().findRecordsByExpr(
      "users",
      $dbx.exp("tournament = {:tournamentid}", {
        tournamentid: tournament.id,
      }),
    );

    const temporaryusers = $app.dao().findRecordsByExpr(
      "temporaryusers",
      $dbx.exp("tournament = {:tournamentid}", {
        tournamentid: tournament.id,
      }),
    );

    for (let i = 0; i < users.length; i++) {
      if (users[i].id === user.id) return c.json(200, { found: true });
    }
    if (users.length + temporaryusers.length >= tournament.get("players"))
      return c.json(404, { found: false });
    return c.json(200, { found: true });
  },
  $apis.activityLogger($app),
  $apis.requireRecordAuth(),
);

routerAdd(
  "DELETE",
  "/leavetournament",
  (c) => {
    const info = $apis.requestInfo(c);
    const userInfo = info.authRecord;

    const userCollection = userInfo.collection().name;
    const user = $app.dao().findRecordById(userCollection, userInfo.id);

    const tournament = $app
      .dao()
      .findRecordById("tournaments", user.get("tournament"));
    if (!tournament) return c.json(203);

    if (tournament.get("owner") === user.id) {
      $app.dao().deleteRecord(tournament);
    } else {
      if (userCollection === "temporaryusers") {
        $app.dao().deleteRecord(user);
      } else {
        user.set("tournament", null);
        $app.dao().saveRecord(user);
      }
    }

    return c.json(200);
  },
  $apis.activityLogger($app),
  $apis.requireRecordAuth(),
);

routerAdd("POST", "/joinwithurl", (c) => {
  const key = require(`${__hooks}/env/env.json`).rsakey;
  const info = $apis.requestInfo(c);
  const user = info.authRecord;
  const data = info.data;
  const tournamentid = data.tournament;
  const password = data.password;

  if (!Boolean(tournamentid) || !Boolean(password))
    return c.json(400, "Bad Request!");

  const tournament = $app.dao().findRecordById("tournaments", tournamentid);
  if (!Boolean(tournament)) return c.json(400, "Bad Request!");

  if (user) {
    if (user.get("tournament") === tournamentid)
      return c.json(200, { joined: true });
  }

  const tournamentPassword = $app.dao().findRecordsByExpr(
    "tournamentpasswords",
    $dbx.exp("tournament = {:tournamentid}", {
      tournamentid: tournament.id,
    }),
  )[0];
  if (
    !Boolean(tournamentPassword) ||
    $security.decrypt(tournamentPassword.get("password"), key) !== password
  )
    return c.json(400, "Bad Request!");

  if (user) {
    const userCollection = user.collection();
    if (userCollection && userCollection.name === "users") {
      user.set("tournament", tournamentid);
      $app.dao().saveRecord(user);

      const generatedPassword = $security.randomString(15).toLowerCase();
      tournamentPassword.set(
        "password",
        $security.encrypt(generatedPassword, key),
      );
      $app.dao().saveRecord(tournamentPassword);

      return c.json(200, { joined: true });
    }
  }

  const payload = JSON.stringify({
    encryptedpassword: tournamentPassword.get("password"),
    tournament: tournament.id,
    exp: Date.now() + 1000 * 60 * 5,
  });
  const keyJwtlite = require(`${__hooks}/env/env.json`).guestrsakey;
  const signature = $security.hs256(payload, keyJwtlite);
  const token = payload + signature;
  //somehow this did not match with hs256
  //const token = $security.createJWT(jwtData, keyJwt, 60 * 5);

  return c.json(200, { token, avatar: tournament.get("avatar") });
});

routerAdd("POST", "/setupguest", (c) => {
  const keyJwt = require(`${__hooks}/env/env.json`).guestrsakey;
  const info = $apis.requestInfo(c);
  const data = info.data;
  const jwtlite = data.jwt;
  const username = data.username;
  const avatar = data.avatar;

  if (!jwtlite || !username || !avatar) return c.json(400, "Bad Request!");
  // TODO: username + password validation
  if (typeof username !== "string" || username.indexOf(" ") > -1)
    return c.json(400, "Bad Request!");

  const jwtSplit = jwtlite.split("}");
  const payload = jwtSplit[0] + "}";
  const signature = jwtSplit[1];
  const hs = $security.hs256(payload, keyJwt);

  if (hs !== signature) return c.json(400, "Bad Request!");
  const parsedPayload = JSON.parse(payload);

  if (parsedPayload.exp < Date.now()) return c.json(400, "Bad Request!");

  const tournament = $app
    .dao()
    .findRecordById("tournaments", parsedPayload.tournament);
  if (!tournament) return c.json(400, "Bad Request!");

  const tournamentPassword = $app.dao().findRecordsByExpr(
    "tournamentpasswords",
    $dbx.exp("tournament = {:tournamentid}", {
      tournamentid: tournament.id,
    }),
  )[0];

  if (
    !tournamentPassword ||
    tournamentPassword.get("password") !== parsedPayload.encryptedpassword
  )
    return c.json(400, "Bad Request!");

  const maxPlayerCount = tournament.getInt("players");
  const joinedusers = $app.dao().findRecordsByExpr(
    "lobbyusers",
    $dbx.exp("tournament = {:tournamentid}", {
      tournamentid: tournament.id,
    }),
  );

  if (
    maxPlayerCount <= joinedusers.length ||
    tournament.get("status") !== "lobby"
  )
    return c.json(400, "Bad Request!");

  const genPassword = $security.randomString(16);
  // const temporaryUserData = {
  //   username: username,
  //   password: genPassword,
  //   passwordConfirm: genPassword,
  //   selectedavatar: avatar,
  //   tournament: tournament.id,
  //   registered: false,
  // };
  // const collection = $app.dao().findCollectionByNameOrId("temporaryusers");
  // const newTempUserRecord = new Record(collection, temporaryUserData);
  // $app.dao().saveRecord(newTempUserRecord);

  const key = require(`${__hooks}/env/env.json`).rsakey;
  const generatedPassword = $security.randomString(15).toLowerCase();
  tournamentPassword.set("password", $security.encrypt(generatedPassword, key));
  $app.dao().saveRecord(tournamentPassword);

  return c.json(200, {
    username,
    genPassword,
    avatar,
    tournament: tournament.id,
  });
});

routerAdd(
  "POST",
  "starttournament",
  (c) => {
    const info = $apis.requestInfo(c);
    const user = info.authRecord;
    if (!user) return c.json(401, "Unauthorized!");

    const tournament = $app
      .dao()
      .findRecordById("tournaments", user.get("tournament"));
    if (!tournament) return c.json(404, "Tournament not found!");

    if (tournament.get("owner") !== user.id) return c.json(403);
    if (tournament.get("status") !== "lobby") return c.json(204);

    const tournamentsIngame = $app
      .dao()
      .findCollectionByNameOrId("tournamentsingame");
    const playerScores = $app.dao().findCollectionByNameOrId("playerscores");
    const tournamentMatches = $app
      .dao()
      .findCollectionByNameOrId("tournamentmatches");
    const joinedusers = $app.dao().findRecordsByExpr(
      "lobbyusers",
      $dbx.exp("tournament = {:tournamentid}", {
        tournamentid: tournament.id,
      }),
    );

    const tournamentType = tournament.get("type");
    if (joinedusers.length <= 1)
      return c.json(400, "Can not start it with one player only!");
    if (
      tournamentType === "koth" &&
      joinedusers.length < tournament.getInt("players")
    )
      return c.json(400, "Can not start it without a full lobby!");

    if (tournamentType === "koth") {
      require(`${__hooks}/helpers.js`).startKOTHTournament(
        tournament,
        tournamentsIngame,
        tournamentMatches,
        playerScores,
        joinedusers,
      );
    } else if (tournamentType === "roundrobin") {
      require(`${__hooks}/helpers.js`).startRoundRobinTournament(
        tournament,
        tournamentsIngame,
        tournamentMatches,
        playerScores,
        joinedusers,
      );
    }

    tournament.set("status", "ingame");
    $app.dao().saveRecord(tournament);

    return c.json(200, {
      message: "Tournament started!",
    });
  },
  $apis.activityLogger($app),
  $apis.requireRecordAuth(),
);

routerAdd(
  "GET",
  "opponents",
  (c) => {
    const info = $apis.requestInfo(c);
    const user = info.authRecord;
    if (!user) return c.json(401, "Unauthorized!");

    const tournament = $app
      .dao()
      .findRecordById("tournaments", user.get("tournament"));
    if (!tournament) return c.json(404, "Tournament not found!");

    const relatedMatches = $app.dao().findRecordsByFilter(
      "tournamentmatches",
      "(playeroneid = {:playerid} || playertwoid = {:playerid}) && tournament = {:tournamentid}",
      "tournamentround",
      99, // limit
      0, // limit
      { playerid: user.id, tournamentid: tournament.id },
    );

    const opponents = [];
    for (let i = 0; i < relatedMatches.length; i++) {
      const match = relatedMatches[i];
      if (match.get("playeroneid") === user.id)
        opponents.push(match.get("playertwoid"));
      else opponents.push(match.get("playeroneid"));
    }

    return c.json(200, opponents);
  },
  $apis.activityLogger($app),
  $apis.requireRecordAuth(),
);

routerAdd(
  "POST",
  "startnextround",
  (c) => {
    const info = $apis.requestInfo(c);
    const user = info.authRecord;
    if (!user) return c.json(401, "Unauthorized!");

    const tournament = $app
      .dao()
      .findRecordById("tournaments", user.get("tournament"));
    if (!tournament) return c.json(404, "Tournament not found!");
    if (tournament.get("owner") !== user.id) return c.json(403);

    const tournamentInGame = $app
      .dao()
      .findFirstRecordByFilter(
        "tournamentsingame",
        "tournament = {:tournamentid}",
        { tournamentid: tournament.id },
      );

    if (!tournamentInGame) return c.json(404, "Tournament not found!");
    if (
      !tournamentInGame.getBool("everyonefinished") &&
      tournamentInGame.getInt("currentround") !== -1
    )
      return c.json(400);

    let currentTournamentRound = tournamentInGame.getInt("currentround");
    currentTournamentRound++;

    if (currentTournamentRound === tournament.getInt("rounds")) {
      tournament.set("status", "ended");

      $app.dao().saveRecord(tournament);
      return c.json(204);
    }

    let finishedMatches = 0;
    const currentMatches = $app
      .dao()
      .findRecordsByFilter(
        "tournamentmatches",
        "tournament = {:tournamentid} && tournamentround = {:currentRound}",
        "-created",
        99,
        0,
        { tournamentid: tournament.id, currentRound: currentTournamentRound },
      );

    for (let i = 0; i < currentMatches.length; i++) {
      const match = currentMatches[i];
      if (
        match.get("playeroneid") === "X" ||
        match.get("playertwoid") === "X"
      ) {
        finishedMatches++;
        require(`${__hooks}/helpers.js`).handleNoOpponent(
          match,
          tournament.id,
          tournament.get("type"),
        );
      }

      match.set("currentround", 0);
      $app.dao().saveRecord(match);
    }

    tournamentInGame.set("currentround", currentTournamentRound);
    tournamentInGame.set("finishedmatches", finishedMatches);
    tournamentInGame.set("everyonefinished", false);
    $app.dao().saveRecord(tournamentInGame);

    return c.json(204);
  },
  $apis.activityLogger($app),
  $apis.requireRecordAuth(),
);

routerAdd(
  "POST",
  "scoring",
  (c) => {
    const info = $apis.requestInfo(c);
    const user = info.authRecord;
    const data = info.data;
    if (!user) return c.json(401, "Unauthorized!");

    const tournament = $app
      .dao()
      .findRecordById("tournaments", user.get("tournament"));
    if (!tournament) return c.json(404, "Tournament not found!");

    const tournamentInGameRecords = $app
      .dao()
      .findRecordsByFilter(
        "tournamentsingame",
        "tournament = {:tournamentid}",
        "-created",
        1,
        0,
        { tournamentid: tournament.id },
      );

    const tournamentInGame = tournamentInGameRecords[0];
    if (!tournamentInGame) return c.json(404, "Tournament not found!");

    const tournamentRound = tournamentInGame.getInt("currentround");
    if (tournamentInGame.getBool("everyonefinished") && tournamentRound === -1)
      return c.json(400);

    const currentMatch = $app
      .dao()
      .findFirstRecordByFilter(
        "tournamentmatches",
        "(playeroneid = {:playerid} || playertwoid = {:playerid}) && tournament = {:tournamentid} && tournamentround = {:tournamentRound}",
        { playerid: user.id, tournamentid: tournament.id, tournamentRound },
      );

    if (!currentMatch || currentMatch.get("winnerid"))
      return c.json(400, "There is a winner already!");

    const sport = $app
      .dao()
      .findFirstRecordByFilter(
        "sports",
        "tournament = {:tournamentid} && roundnumber = {:tournamentRound}",
        { tournamentid: tournament.id, tournamentRound },
      );

    if (
      sport.get("scoringrule") !== "Simultaneous" &&
      currentMatch.get("currentplayerid") !== user.id
    )
      return c.json(400, "Not your turn!");

    let arr;
    const goal = sport.getInt("scoregoal");
    const start = sport.getInt("scorestart");
    const toAdd = start <= goal;
    const isPlayerOne = currentMatch.get("playeroneid") === user.id;

    if (isPlayerOne) {
      arr = currentMatch.getString("playeronehistory").split(",");
      currentMatch.set("currentplayerid", currentMatch.get("playertwoid"));
    } else {
      arr = currentMatch.getString("playertwohistory").split(",");
      currentMatch.set("currentplayerid", currentMatch.get("playeroneid"));
    }

    if (arr.length >= 5) arr.shift();
    // There should always be at least one element
    const index = arr.length - 1;
    const prevValue = parseInt(arr[index]);
    let score;
    let roundFinished = false;
    if (toAdd) {
      score = prevValue + data.score;
      if (!(goal === 0 && start === 0) && score >= goal) {
        roundFinished = true;
      }
    } else {
      score = prevValue - data.score;
      if (!(goal === 0 && start === 0) && score <= goal) {
        roundFinished = true;
      }
    }

    if (!roundFinished) {
      arr.push(score.toString());

      if (isPlayerOne) {
        currentMatch.set("playeronehistory", arr.join(","));
      } else {
        currentMatch.set("playertwohistory", arr.join(","));
      }
    } else {
      const usersResult = require(`${__hooks}/helpers.js`).handleRoundFinished(
        currentMatch,
        sport,
        user.id,
      );

      if (usersResult.length > 0) {
        const playerScores = $app.dao().findRecordsByFilter(
          "playerscores",
          "tournament = {:tournamentid}",
          "id",
          99, // limit
          0, // limit
          { tournamentid: tournament.id },
        );

        if (tournament.get("type") === "koth") {
          if (usersResult[0] === "__tie__") {
            $app.dao().saveRecord(currentMatch);

            return;
          } else {
            for (let i = 0; i < playerScores.length; i++) {
              const playerScore = playerScores[i];

              if (playerScore.get("playerid") === usersResult[0]) {
                playerScore.set(
                  "score",
                  Math.ceil(playerScore.getInt("score") / 2),
                );
              } else if (playerScore.get("playerid") === usersResult[1]) {
                playerScore.set("score", -1);
              }
              $app.dao().saveRecord(playerScore);
            }
          }
        } else if (tournament.get("type") === "roundrobin") {
          const roundScoring = $app
            .dao()
            .findFirstRecordByFilter(
              "roundscorings",
              "tournament = {:tournamentid} && roundnumber = {:tournamentRound}",
              { tournamentid: tournament.id, tournamentRound },
            );

          require(`${__hooks}/helpers.js`).handleScores(
            roundScoring,
            currentMatch,
            playerScores,
            usersResult,
          );
        }

        let finishedMatches = tournamentInGame.getInt("finishedmatches");
        finishedMatches++;
        tournamentInGame.set("finishedmatches", finishedMatches);
        if (
          tournament.get("type") === "roundrobin" &&
          finishedMatches >= Math.ceil(playerScores.length / 2)
        ) {
          tournamentInGame.set("everyonefinished", true);
        }

        if (tournament.get("type") === "koth") {
          const playersLeft = $app.dao().findRecordsByFilter(
            "playerscores",
            "tournament = {:tournamentid} && score != -1",
            "-score",
            99, // limit
            0, // limit
            { tournamentid: tournament.id },
          );

          if (finishedMatches >= Math.ceil(playersLeft.length / 2)) {
            tournamentInGame.set("everyonefinished", true);
            if (finishedMatches !== 1) {
              require(`${__hooks}/helpers.js`).createKOTHNextMatches(
                playersLeft,
                tournamentInGame.getInt("currentround") + 1,
                tournament.id,
              );
            }
          }
        }

        $app.dao().saveRecord(tournamentInGame);
      }
    }

    $app.dao().saveRecord(currentMatch);
    return c.json(200);
  },
  $apis.activityLogger($app),
  $apis.requireRecordAuth(),
);

routerAdd(
  "POST",
  "undeterminedfinished/:answer",
  (c) => {
    const info = $apis.requestInfo(c);
    const user = info.authRecord;
    const answer = c.pathParam("answer");
    if (!user) return c.json(401, "Unauthorized!");

    const tournament = $app
      .dao()
      .findRecordById("tournaments", user.get("tournament"));
    if (!tournament) return c.json(404, "Tournament not found!");

    const tournamentInGame = $app
      .dao()
      .findFirstRecordByFilter(
        "tournamentsingame",
        "tournament = {:tournamentid}",
        { tournamentid: tournament.id },
      );

    if (!tournamentInGame) return c.json(404, "Tournament not found!");

    const tournamentRound = tournamentInGame.getInt("currentround");
    if (tournamentInGame.getBool("everyonefinished") && tournamentRound === -1)
      return c.json(400);

    const currentMatch = $app
      .dao()
      .findFirstRecordByFilter(
        "tournamentmatches",
        "(playeroneid = {:playerid} || playertwoid = {:playerid}) && tournament = {:tournamentid} && tournamentround = {:tournamentRound}",
        { playerid: user.id, tournamentid: tournament.id, tournamentRound },
      );

    if (!currentMatch || currentMatch.get("winnerid"))
      return c.json(400, "There is a winner already!");

    const sport = $app
      .dao()
      .findFirstRecordByFilter(
        "sports",
        "tournament = {:tournamentid} && roundnumber = {:tournamentRound}",
        { tournamentid: tournament.id, tournamentRound },
      );

    if (sport.get("scoringrule") !== "Undetermined") return c.json(400);

    const initUser = currentMatch.get("matchfinished");
    if (!initUser) {
      currentMatch.set("matchfinished", user.id);
      $app.dao().save(currentMatch);

      return c.json(204);
    }
    if (initUser !== user.id) {
      if (answer === "yes") {
        currentMatch.set("matchfinished", "");

        const usersResult =
          require(`${__hooks}/helpers.js`).handleUndeterminedRoundFinished(
            currentMatch,
            sport,
          );

        if (usersResult.length > 0) {
          const roundScoring = $app
            .dao()
            .findFirstRecordByFilter(
              "roundscorings",
              "tournament = {:tournamentid} && roundnumber = {:tournamentRound}",
              { tournamentid: tournament.id, tournamentRound },
            );

          const playerScores = $app.dao().findRecordsByFilter(
            "playerscores",
            "tournament = {:tournamentid}",
            "id",
            99, // limit
            0, // limit
            { tournamentid: tournament.id },
          );

          require(`${__hooks}/helpers.js`).handleScores(
            roundScoring,
            currentMatch,
            playerScores,
            usersResult,
          );

          let finishedMatches = tournamentInGame.getInt("finishedmatches");
          finishedMatches++;
          tournamentInGame.set("finishedmatches", finishedMatches);
          if (finishedMatches >= Math.ceil(playerScores.length / 2)) {
            tournamentInGame.set("everyonefinished", true);
          }

          $app.dao().saveRecord(tournamentInGame);
        }
      }
      if (answer === "no") {
        currentMatch.set("matchfinished", "");
      }
      $app.dao().save(currentMatch);
      return c.json(204);
    }
    return c.json(204);
  },
  $apis.activityLogger($app),
  $apis.requireRecordAuth(),
);

routerAdd("GET", "testme", (c) => {
  return c.json(204);
  // const tournament = $app
  //   .dao()
  //   .findRecordById("tournaments", "u12eo2bwpxeyy3g");
  // const joinedusers = $app.dao().findRecordsByExpr(
  //   "lobbyusers",
  //   $dbx.exp("tournament = {:tournamentid}", {
  //     tournamentid: tournament.id,
  //   }),
  // );
  // const tournamentMatches = $app
  //   .dao()
  //   .findCollectionByNameOrId("tournamentmatches");
  // const allMatches = require(`${__hooks}/helpers.js`).createRoundRobinMatches(
  //   joinedusers.map((user) => user.id),
  //   tournament.getInt("rounds"),
  // );
  //
  // for (let i = 0; i < allMatches.length; i++) {
  //   let perRoundMatches = allMatches[i];
  //
  //   for (let j = 0; j < perRoundMatches.length; j++) {
  //     let matchPlayers = perRoundMatches[j];
  //     const sport = $app
  //       .dao()
  //       .findFirstRecordByFilter(
  //         "sports",
  //         "tournament = {:tournamentid} && roundnumber = {:round}",
  //         {
  //           tournamentid: tournament.id,
  //           round: i,
  //         },
  //       );
  //     const scoring = $app
  //       .dao()
  //       .findFirstRecordByFilter(
  //         "roundscorings",
  //         "tournament = {:tournamentid} && roundnumber = {:round}",
  //         {
  //           tournamentid: tournament.id,
  //           round: i,
  //         },
  //       );
  //     const record = new Record(tournamentMatches, {
  //       tournamentround: i,
  //       currentround: -1,
  //       winner: null,
  //       currentplayerid: matchPlayers[0],
  //       sport: sport.id,
  //       scoring: scoring.id,
  //       playeroneid: matchPlayers[0],
  //       playeronehistory: "[]",
  //       playertwoid: matchPlayers[1],
  //       playertwohistory: "[]",
  //       tournament: tournament.id,
  //     });
  //     $app.dao().saveRecord(record);
  //   }
  // }
  //
  // return c.json(200, { matches: allMatches });
});
