function shuffle(array) {
  return array
    .map((value) => ({ value, sort: Math.random() }))
    .sort((a, b) => a.sort - b.sort)
    .map((obj) => obj.value);
}

function startKOTHTournament(
  tournament,
  tournamentsIngame,
  tournamentMatches,
  playerScores,
  joinedusers,
) {
  $app.dao().runInTransaction((txDao) => {
    const ingameRecord = new Record(tournamentsIngame, {
      tournament: tournament.id,
      currentround: -1,
      finishedmatches: 0,
    });
    txDao.saveRecord(ingameRecord);

    const playerCount = joinedusers.length;
    const exponent = Math.ceil(Math.log2(playerCount));
    const startingScore = Math.pow(2, exponent);

    for (let i = 0; i < playerCount; i++) {
      const username = joinedusers[i].getString("username");
      const score = startingScore - Math.floor(i / 2);
      const scoreRecord = new Record(playerScores, {
        playerid: joinedusers[i].id,
        score: score,
        tournament: tournament.id,
        //For some reason, it gets surrounded with ""
        username: username.slice(1, username.length - 1),
      });
      txDao.saveRecord(scoreRecord);
    }

    const sport = $app
      .dao()
      .findFirstRecordByFilter(
        "sports",
        "tournament = {:tournamentid} && roundnumber = {:round}",
        {
          tournamentid: tournament.id,
          round: 0,
        },
      );
    for (let i = 0; i < playerCount; i++) {
      if (i % 2 == 1) continue;
      const score = startingScore - Math.floor(i / 4);

      const record = new Record(tournamentMatches, {
        tournamentround: 0,
        branch: score,
        currentround: -1,
        winner: null,
        currentplayerid: joinedusers[i].id,
        sport: sport.id,
        scoring: null,
        playeroneid: joinedusers[i].id,
        playeronehistory: "" + sport.getInt("scorestart"),
        playeronerounds: 0,
        playertwoid: joinedusers[i + 1] ? joinedusers[i + 1].id : "X",
        playertworounds: 0,
        playertwohistory: "" + sport.getInt("scorestart"),
        tournament: tournament.id,
      });
      txDao.saveRecord(record);
    }
  });
}

// players should already be in order by score (branch)
function createKOTHNextMatches(players, nextRound, tournamentid) {
  const sport = $app
    .dao()
    .findFirstRecordByFilter(
      "sports",
      "tournament = {:tournamentid} && roundnumber = {:round}",
      {
        tournamentid,
        round: nextRound,
      },
    );
  const tournamentMatches = $app
    .dao()
    .findCollectionByNameOrId("tournamentmatches");

  $app.dao().runInTransaction((txDao) => {
    for (let i = 0; i < players.length; i++) {
      if (i % 2 === 1) continue;

      const record = new Record(tournamentMatches, {
        tournamentround: nextRound,
        branch: Math.ceil(players[i].getInt("score") / 2),
        currentround: -1,
        winner: null,
        currentplayerid: players[i].get("playerid"),
        sport: sport.id,
        scoring: null,
        playeroneid: players[i].get("playerid"),
        playeronehistory: "" + sport.getInt("scorestart"),
        playeronerounds: 0,
        playertwoid: players[i + 1] ? players[i + 1].get("playerid") : "X",
        playertworounds: 0,
        playertwohistory: "" + sport.getInt("scorestart"),
        tournament: tournamentid,
      });
      txDao.saveRecord(record);
    }
  });
}

function startRoundRobinTournament(
  tournament,
  tournamentsIngame,
  tournamentMatches,
  playerScores,
  joinedusers,
) {

  $app.dao().runInTransaction((txDao) => {
    const ingameRecord = new Record(tournamentsIngame, {
      tournament: tournament.id,
      currentround: -1,
      finishedmatches: 0,
    });
    txDao.saveRecord(ingameRecord);

    const playerCount = joinedusers.length;
    for (let i = 0; i < playerCount; i++) {
      const username = joinedusers[i].getString("username");
      const scoreRecord = new Record(playerScores, {
        playerid: joinedusers[i].id,
        score: 0,
        tournament: tournament.id,
        //For some reason, it gets surrounded with ""
        username: username.slice(1, username.length - 1),
      });
      txDao.saveRecord(scoreRecord);
    }

    const allMatches = createRoundRobinMatches(
      joinedusers.map((user) => user.id),
      tournament.getInt("rounds"),
    );

    for (let i = 0; i < allMatches.length; i++) {
      let perRoundMatches = allMatches[i];
      const sport = $app
        .dao()
        .findFirstRecordByFilter(
          "sports",
          "tournament = {:tournamentid} && roundnumber = {:round}",
          {
            tournamentid: tournament.id,
            round: i,
          },
        );

      const scoring = $app
        .dao()
        .findFirstRecordByFilter(
          "roundscorings",
          "tournament = {:tournamentid} && roundnumber = {:round}",
          {
            tournamentid: tournament.id,
            round: i,
          },
        );

      for (let j = 0; j < perRoundMatches.length; j++) {
        let matchPlayers = perRoundMatches[j];

        const record = new Record(tournamentMatches, {
          tournamentround: i,
          branch: 0,
          currentround: -1,
          winner: null,
          currentplayerid: matchPlayers[0],
          sport: sport.id,
          scoring: scoring.id,
          playeroneid: matchPlayers[0],
          playeronehistory: "" + sport.getInt("scorestart"),
          playeronerounds: 0,
          playertwoid: matchPlayers[1],
          playertworounds: 0,
          playertwohistory: "" + sport.getInt("scorestart"),
          tournament: tournament.id,
        });
        txDao.saveRecord(record);
      }
    }
  });
}

function createRoundRobinMatches(players, rounds) {
  let matches = [];
  let shufflesCount =
    players.length % 2 === 0 ? players.length - 1 : players.length;

  let firstHalf = players.slice(0, Math.ceil(players.length / 2));
  let secondHalf = players.slice(Math.ceil(players.length / 2));
  if (firstHalf.length !== secondHalf.length) secondHalf.push("X");

  for (let i = 0; i < rounds; i++) {
    if (matches.length % shufflesCount === 0) {
      firstHalf = shuffle(firstHalf);
      secondHalf = shuffle(secondHalf);
    }

    let roundMatches = [];
    for (let j = 0; j < firstHalf.length; j++) {
      roundMatches.push([firstHalf[j], secondHalf[j]]);
    }
    matches.push(roundMatches);

    let fromFirst = firstHalf.pop();
    let fromSecond = secondHalf.shift();
    firstHalf.splice(1, 0, fromSecond);
    secondHalf.push(fromFirst);
  }

  return matches;
}

function handleUndeterminedRoundFinished(currentMatch, sport) {
  const roundsPlayedRule = sport.getString("roundsplayed");
  const start = sport.getInt("scorestart");
  let playerOneRounds = currentMatch.getInt("playeronerounds");
  let playerTwoRounds = currentMatch.getInt("playertworounds");
  const playerOneScore = parseInt(
    currentMatch.get("playeronehistory").split(",").reverse()[0],
  );
  const playerTwoScore = parseInt(
    currentMatch.get("playertwohistory").split(",").reverse()[0],
  );
  const hasPlayerOneScored = playerOneScore >= playerTwoScore;

  if (hasPlayerOneScored) {
    playerOneRounds++;
    currentMatch.set("playeronerounds", playerOneRounds);
  } else {
    playerTwoRounds++;
    currentMatch.set("playertworounds", playerTwoRounds);
  }

  currentMatch.set("playeronehistory", "" + start);
  currentMatch.set("playertwohistory", "" + start);
  currentMatch.set("currentround", currentMatch.getInt("currentround") + 1);

  if (roundsPlayedRule === "fixed") {
    return handleFixedRoundsWinner(
      playerOneRounds,
      playerTwoRounds,
      currentMatch,
      sport,
    );
  } else if (roundsPlayedRule === "best of") {
    return handleBestOfWinner(
      playerOneRounds,
      playerTwoRounds,
      currentMatch,
      sport,
      hasPlayerOneScored,
    );
  }
}

function handleRoundFinished(currentMatch, sport, userid) {
  const isPlayerOne = currentMatch.get("playeroneid") === userid;
  const roundsPlayedRule = sport.getString("roundsplayed");
  const start = sport.getInt("scorestart");
  let playerOneRounds = currentMatch.getInt("playeronerounds");
  let playerTwoRounds = currentMatch.getInt("playertworounds");

  if (isPlayerOne) {
    playerOneRounds++;
    currentMatch.set("playeronerounds", playerOneRounds);
  } else {
    playerTwoRounds++;
    currentMatch.set("playertworounds", playerTwoRounds);
  }

  currentMatch.set("playeronehistory", "" + start);
  currentMatch.set("playertwohistory", "" + start);
  currentMatch.set("currentround", currentMatch.getInt("currentround") + 1);

  if (roundsPlayedRule === "fixed") {
    return handleFixedRoundsWinner(
      playerOneRounds,
      playerTwoRounds,
      currentMatch,
      sport,
    );
  } else if (roundsPlayedRule === "best of") {
    return handleBestOfWinner(
      playerOneRounds,
      playerTwoRounds,
      currentMatch,
      sport,
      isPlayerOne,
    );
  }
}

function handleBestOfWinner(
  playerOneRounds,
  playerTwoRounds,
  currentMatch,
  sport,
  isPlayerOne,
) {
  let winner = "__tie__";
  const roundsToWin = sport.getInt("rounds");

  // If rounds is an even number
  if (
    playerOneRounds + playerTwoRounds >= roundsToWin &&
    playerOneRounds === playerTwoRounds
  ) {
    currentMatch.set("winnerid", winner);
    return [winner, null];
  } else if (isPlayerOne) {
    if (Math.ceil(roundsToWin / 2) <= playerOneRounds) {
      winner = currentMatch.get("playeroneid");
      currentMatch.set("winnerid", winner);
      return [winner, currentMatch.get("playertwoid")];
    }
  } else {
    if (Math.ceil(roundsToWin / 2) <= playerTwoRounds) {
      winner = currentMatch.get("playertwoid");
      currentMatch.set("winnerid", winner);
      return [winner, currentMatch.get("playeroneid")];
    }
  }

  return [];
}

function handleFixedRoundsWinner(
  playerOneRounds,
  playerTwoRounds,
  currentMatch,
  sport,
) {
  let winner = "__tie__";
  const roundsToWin = sport.getInt("rounds");

  if (playerOneRounds + playerTwoRounds >= roundsToWin) {
    if (playerOneRounds > playerTwoRounds) {
      winner = currentMatch.get("playeroneid");

      currentMatch.set("winnerid", winner);
      return [winner, currentMatch.get("playertwoid")];
    } else if (playerOneRounds < playerTwoRounds) {
      winner = currentMatch.get("playertwoid");

      currentMatch.set("winnerid", winner);
      return [winner, currentMatch.get("playeroneid")];
    }
  }

  return [];
}

function handleScores(roundScoring, match, playerScores, players) {
  if (players[0] === "__tie__") {
    handleTie(roundScoring, match, playerScores);
    return;
  }

  const winner = playerScores.find(
    (playerScore) => playerScore.get("playerid") === players[0],
  );
  handleWinner(roundScoring, match, winner);

  const loser = playerScores.find(
    (playerScore) => playerScore.get("playerid") === players[1],
  );
  handleLoser(roundScoring, match, loser);
}

function handleTie(roundScoring, match, playerScores) {
  const type = roundScoring.get("winnertype");
  const amount = roundScoring.getInt("winneramount");

  const playerOneScoreRecord = playerScores.find(
    (playerScore) => playerScore.get("playerid") === match.get("playeroneid"),
  );
  const playerOneCurrentScore = playerOneScoreRecord.getInt("score");
  const playerTwoScoreRecord = playerScores.find(
    (playerScore) => playerScore.get("playerid") === match.get("playertwoid"),
  );
  const playerTwoCurrentScore = playerTwoScoreRecord.getInt("score");

  if (type === "FIX") {
    playerOneScoreRecord.set(
      "score",
      playerOneCurrentScore + Math.floor(amount / 2),
    );
    playerTwoScoreRecord.set(
      "score",
      playerTwoCurrentScore + Math.floor(amount / 2),
    );
  } else if (type === "ROUNDSWON") {
    // It should be a tie
    let calculated = Math.floor(match.getInt("playeronerounds") * amount);

    playerOneScoreRecord.set("score", playerOneCurrentScore + calculated);
    playerTwoScoreRecord.set("score", playerTwoCurrentScore + calculated);
  } else if (type === "SUMOFDIFFERENCES") {
    playerOneScoreRecord.set("score", playerOneCurrentScore + 1);
    playerTwoScoreRecord.set("score", playerTwoCurrentScore + 1);
  }

  $app.dao().saveRecord(playerOneScoreRecord);
  $app.dao().saveRecord(playerTwoScoreRecord);
}

function handleWinner(roundScoring, match, winner) {
  const type = roundScoring.get("winnertype");
  const amount = roundScoring.getInt("winneramount");
  const currentScore = winner.getInt("score");

  if (type === "FIX") {
    winner.set("score", currentScore + amount);
  } else if (type === "ROUNDSWON") {
    let calculated;
    if (match.get("playeroneid") === winner.get("playerid")) {
      calculated = Math.floor(
        currentScore + match.getInt("playeronerounds") * amount,
      );
    } else {
      calculated = Math.floor(
        currentScore + match.getInt("playertworounds") * amount,
      );
    }
    winner.set("score", calculated);
  } else if (type === "SUMOFDIFFERENCES") {
    const playerOneRounds = match.getInt("playeronerounds");
    const playerTwoRounds = match.getInt("playertworounds");

    const calculated = Math.floor(
      currentScore + Math.abs(playerOneRounds - playerTwoRounds) * amount,
    );

    winner.set("score", calculated);
  }

  $app.dao().saveRecord(winner);
}

function handleLoser(roundScoring, match, loser) {
  const type = roundScoring.get("losertype");
  const amount = roundScoring.getInt("loseramount");
  const currentScore = loser.getInt("score");

  if (type === "FIX") {
    loser.set("score", currentScore + amount);
  } else if (type === "ROUNDSWON") {
    let calculated;
    if (match.get("playeroneid") === loser.get("playerid")) {
      calculated = Math.floor(
        currentScore + match.getInt("playeronerounds") * amount,
      );
    } else {
      calculated = Math.floor(
        currentScore + match.getInt("playertworounds") * amount,
      );
    }
    loser.set("score", calculated);
  }

  $app.dao().saveRecord(loser);
}

function handleNoOpponent(match, tournamentId, tournamentType) {
  let winnerId = "";
  if (match.get("playeroneid") === "X") {
    winnerId = match.get("playertwoid");
  } else winnerId = match.get("playeroneid");

  match.set("winnerid", winnerId);

  const playerScoreRecord = $app
    .dao()
    .findFirstRecordByFilter(
      "playerscores",
      "tournament = {:tournamentid} && playerid = {:winnerId}",
      { tournamentid: tournamentId, winnerId },
    );

  if (tournamentType === "koth") {
    const currentBranch = playerScoreRecord.getInt("score");
    playerScoreRecord.set("score", Math.ceil(currentBranch / 2));
  } else if (tournamentType === "roundrobin") {
    const scoringRecord = $app
      .dao()
      .findFirstRecordByFilter(
        "roundscorings",
        "tournament = {:tournamentid} && roundnumber = {:round}",
        { tournamentid: tournamentId, round: match.get("tournamentround") },
      );

    const type = scoringRecord.get("winnertype");
    const amount = scoringRecord.getInt("winneramount");
    const currentScore = playerScoreRecord.getInt("score");

    if (type === "FIX") {
      playerScoreRecord.set("score", currentScore + amount);
    } else {
      const sport = $app
        .dao()
        .findFirstRecordByFilter(
          "sports",
          "tournament = {:tournamentid} && roundnumber = {:round}",
          { tournamentid: tournamentId, round: match.get("tournamentround") },
        );

      playerScoreRecord.set(
        "score",
        currentScore +
          Math.floor(Math.ceil(sport.getInt("rounds") * 0.5) * amount),
      );
    }
  }

  $app.dao().saveRecord(playerScoreRecord);
}

exports.createKOTHNextMatches = createKOTHNextMatches;
exports.startKOTHTournament = startKOTHTournament;
exports.startRoundRobinTournament = startRoundRobinTournament;
exports.handleUndeterminedRoundFinished = handleUndeterminedRoundFinished;
exports.handleNoOpponent = handleNoOpponent;
exports.handleScores = handleScores;
exports.handleRoundFinished = handleRoundFinished;
exports.createRoundRobinMatches = createRoundRobinMatches;
